//
//  FileSaver.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class FileSaver {
    // MARK: - Private
    private func path(type: FileTypes, name: String, create: Bool) -> URL {
        var d: String
        let dir: FileManager.SearchPathDirectory = .applicationSupportDirectory
        switch type {
        case .image:
            d = "img"
        case .smallImage:
            d = "small_img"
        case .model:
            d = "models"
        }
        let fm = FileManager.default
        let cachesurl = try! fm.url(for: dir,
                                    in: .userDomainMask,
                                    appropriateFor: nil,
                                    create: true)
        let l: String
        if name.contains("/") { // is it a path
            l = (name as NSString).deletingLastPathComponent
        }
        else { // or just a name?
            l = name
        }
        let dirPath = cachesurl.appendingPathComponent(d).appendingPathComponent(l)
        if create {
            do {
                try fm.createDirectory(at: dirPath,
                                       withIntermediateDirectories: true,
                                       attributes: nil)
            }
            catch {
                print("FileSaver: cannot create directory at path name: \(dirPath)")
            }
        }
        return dirPath.appendingPathComponent((name as NSString).lastPathComponent)
    }

    private func loadImageOf(type: FileTypes, name: String) -> UIImage? {
        if let d = contentsOf(type: type, name: name) {
            return UIImage(data: d)
        }
        return nil
    }

    fileprivate func stampDateTo(filePath: URL) {
        let path = filePath.appendingPathExtension(const.dateFile)
        let date = Date()
        let sDate = "\(date.timeIntervalSinceReferenceDate)"
        try? sDate.write(to: path, atomically: false, encoding: .utf8)
    }

    fileprivate struct const {
        static let dateFile = "date"
    }
}

// MARK: - FileSaverService
extension FileSaver: FileSaverService {
    func exists(type: FileTypes, name: String) -> Bool {
        let p = path(type: type, name: name, create: false)
        return FileManager.default.fileExists(atPath: p.path)
    }

    func urlFrom(type: FileTypes, name: String) -> URL {
        return path(type: type, name: name, create: false)
    }

    func filesFor(type: FileTypes) -> [String] {
        let p = path(type: type, name: "NONE", create: false)
        var dir = p
        dir.deleteLastPathComponent()
        let fm = FileManager.default
        let d = try? fm.contentsOfDirectory(atPath: dir.path)
        return d ?? [];
    }

    func dateFor(path: String) -> Date? {
        do {
            let filePath = URL(fileURLWithPath: path)
            let restored = try Data(contentsOf: filePath.appendingPathExtension(const.dateFile))
            let str = String(bytes: restored, encoding: .utf8)
            if let date = str?.toDouble {
                let fileDate = Date(timeIntervalSinceReferenceDate: date)
                return fileDate
            }
        }
        catch {
            print("cannot read file's date: '\(path)' \(error.localizedDescription)")
        }
        return nil
    }

    @discardableResult
    func save(_ type: FileTypes, name: String, with data: Data) -> String {
        let fileName = path(type: type, name: name, create: true)
        do {
            try data.write(to: fileName)
        }
        catch {
            print("FileSaver: Cannot create file of \(type) with name: \(name)")
        }
        stampDateTo(filePath: fileName)
        return fileName.path
    }

    func contentsOf(type: FileTypes, name: String) -> Data? {
        let filePath = path(type: type, name: name, create: false)
        return try? Data(contentsOf: filePath)
    }

    func load(image: String) -> UIImage? {
        return loadImageOf(type: .image, name: image)
    }

    func loadSmall(image: String) -> UIImage? {
        return loadImageOf(type: .smallImage, name: image)
    }

    func remove(type: FileTypes, name: String) {
        let filePath = path(type: type, name: name, create: false)
        let fm = FileManager.default
        if !fm.fileExists(atPath: filePath.path) { return }
        do {
            try fm.removeItem(at: filePath)
        }
        catch {
            print("FileSaver: Cannot delete a file of \(type) with name: \(name); error: '\(error.localizedDescription)'")
        }
        let dateFile = filePath.appendingPathExtension(const.dateFile)
        if fm.fileExists(atPath: dateFile.path) {
            try? fm.removeItem(at: dateFile)
        }
        let dirPath = filePath.deletingLastPathComponent()
        let file = filePath.lastPathComponent
        let dir = dirPath.lastPathComponent
        if file == dir {
            do {
                try fm.removeItem(at: dirPath)
            }
            catch {
                print("FileSaver: Cannot delete a directory of file of \(type) with name: \(name); error: '\(error.localizedDescription)'")
            }
        }
    }
}
