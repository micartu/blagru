//
//  SplashViewController.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class SplashViewController: BaseViewController {
    var vm: SplashViewModelProtocol!
    @IBOutlet weak var logoView: UIImageView!

    // MARK: Life cycle

    override func viewDidLoad() {
        logoView.image = UIImage(named: "app_logo")
        super.viewDidLoad()
        vm.setupTransition()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        vm.viewIsReady()
    }

    // MARK: Themable
    override func apply(theme: Theme) {
        super.apply(theme: theme)
        logoView.tintColor = theme.tintColor
    }
}

// MARK: - ViewControllerable
extension SplashViewController: ViewControllerable {
    static var storyboardName: String {
        return "Splash"
    }
}
