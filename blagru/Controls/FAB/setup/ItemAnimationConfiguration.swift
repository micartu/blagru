//
//  ItemAnimationConfiguration.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

/// Item animation configuration
///
struct ItemAnimationConfiguration {
    /// Animation settings for opening animation.
    /// Default values are:
    ///   - `duration = 0.3`
    ///   - `dampingRatio = 0.55`
    ///   - `initialVelocity = 0.3`
    ///   - `interItemDelay = 0.1`
    ///
    var opening = AnimationSettings(duration: 0.3, dampingRatio: 0.55, initialVelocity: 0.3, interItemDelay: 0.1)

    /// Animation settings for closing animation.
    /// Default values are:
    ///   - `duration = 0.3`
    ///   - `dampingRatio = 0.6`
    ///   - `initialVelocity = 0.8`
    ///   - `interItemDelay = 0.1`
    ///
    var closing = AnimationSettings(duration: 0.15, dampingRatio: 0.6, initialVelocity: 0.8, interItemDelay: 0.1)

    /// Defines the layout of the acton items when opened.
    /// Default is a layout in a vertical line with 12 points inter item spacing
    ///
    var itemLayout: ItemLayout = .verticalLine()

    /// Configures the items before opening. The change from open to closed state is animated.
    /// Default is a scale by factor `0.4` and `item.alpha = 0`.
    ///
    var closedState: ItemPreparation = .scale()

    /// Configures the items for open state. The change from open to closed state is animated.
    /// Default is `item.transform = .identity` and `item.alpha = 1`.
    ///
    var openState: ItemPreparation = .identity()

    /// how to add items to canvas
    enum buttonsAppendMode {
        case below // below the FAB
        case above // above the FAB
    }

    /// defines how to add items to canvas
    /// Default values is: `below`
    var appendMode: buttonsAppendMode = .below
}

extension ItemAnimationConfiguration {
    /// Returns an item animation configuration with
    ///   - `itemLayout = .verticalLine()`
    ///   - `closedState = .scale()`
    ///
    /// - Parameter interItemSpacing: The distance between two adjacent items. Default is `12`.
    /// - Parameter firstItemSpacing: The distance between the action button and the first action item.
    ///                               When `firstItemSpacing` is `0` or less `interItemSpacing` is used instead.
    ///                               Default is `0`.
    ///
    /// - Returns: An item animation configuration object.
    ///
    static func popUp(withInterItemSpacing interItemSpacing: CGFloat = 12,
                      firstItemSpacing: CGFloat = 0) -> ItemAnimationConfiguration {
        var configuration = ItemAnimationConfiguration()
        configuration.itemLayout = .verticalLine(withInterItemSpacing: interItemSpacing, firstItemSpacing: firstItemSpacing)
        configuration.closedState = .scale()
        return configuration
    }

    /// Returns an item animation configuration with
    ///   - `itemLayout = .verticalLine()`
    ///   - `closedState = .horizontalOffset()`
    ///
    /// - Parameter interItemSpacing: The distance between two adjacent items. Default is `12`.
    /// - Parameter firstItemSpacing: The distance between the action button and the first action item.
    ///                               When `firstItemSpacing` is `0` or less `interItemSpacing` is used instead.
    ///                               Default is `0`.
    ///
    /// - Returns: An item animation configuration object.
    ///
    static func slideIn(withInterItemSpacing interItemSpacing: CGFloat = 12,
                        firstItemSpacing: CGFloat = 0) -> ItemAnimationConfiguration {
        var configuration = ItemAnimationConfiguration()
        configuration.itemLayout = .verticalLine(withInterItemSpacing: interItemSpacing, firstItemSpacing: firstItemSpacing)
        configuration.closedState = .horizontalOffset()
        return configuration
    }

    /// Returns an item animation configuration with
    ///   - `itemLayout = .circular()`
    ///   - `closedState = .scale()`
    ///
    /// - Parameter radius: The distance between the center of an item and the center of the button itself.
    ///
    /// - Returns: An item animation configuration object.
    ///
    static func circularPopUp(withRadius radius: CGFloat = 100) -> ItemAnimationConfiguration {
        var configuration = ItemAnimationConfiguration()
        configuration.itemLayout = .circular(withRadius: radius)
        configuration.closedState = .scale()
        configuration.opening.interItemDelay = 0.05
        configuration.closing.interItemDelay = 0.05
        return configuration
    }

    /// Returns an item animation configuration with
    ///   - `itemLayout = .circular()`
    ///   - `closedState = .circularOffset()`
    ///
    /// - Parameter radius: The distance between the center of an item and the center of the button itself.
    ///
    /// - Returns: An item animation configuration object.
    ///
    static func circularSlideIn(withRadius radius: CGFloat = 100) -> ItemAnimationConfiguration {
        var configuration = ItemAnimationConfiguration()
        configuration.itemLayout = .circular(withRadius: radius)
        configuration.closedState = .circularOffset(distance: radius * 0.75)
        return configuration
    }
}
