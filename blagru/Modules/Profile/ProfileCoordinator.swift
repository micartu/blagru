//
//  ProfileCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class ProfileCoordinator: BaseCoordinator {
    init(userID: String, parent: BaseCoordinator?) {
        self.userID = userID
        super.init(parent: parent)
    }

	override func start() {
		let vc = BaseTableViewController()
		vc.manualBuilding = true
		vc.viewBuilder = {
			let tv = UITableView(frame: .zero, style: .grouped)
            tv.separatorStyle = .none
            tv.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
			return (tv, tv, true)
		}
        vc.addRefreshControl = false
        vc.themingHandler = { [weak vc] theme in
            vc?.tableview.backgroundColor = theme.mainBackColor
        }
		vc.title = "Profile Title".localized
		vc.initializer = { tv in
            let cells = [
                ProfileCaptionCell.self,
                EditableItemCell.self,
            ]
            for c in cells {
                let identifier = String(describing: c)
                tv.register(UINib(nibName: identifier, bundle: nil),
                            forCellReuseIdentifier: identifier)
            }
		}
		let viewModel = ProfileViewModel(userID: userID,
                                         net: ServicesAssembler.inject(),
                                         imgLoader: ServicesAssembler.inject(),
                                         cache: ServicesAssembler.inject())
		viewModel.dispatcher = self
		viewModel.source = viewModel
		vc.viewModel = viewModel
		cntr = vc
	}

    fileprivate lazy var imgPicker = SingleImagePicker()
    fileprivate let userID: String
    fileprivate var handler: ((UIImage) -> Void)?
}

// MARK: - ProfileDispatcherProtocol
extension ProfileCoordinator: ProfileDispatcherProtocol {
    func askForPicture(completion: @escaping ((UIImage) -> Void)) {
        handler = completion
        imgPicker.delegate = self
        showBusyIndicator()
        cntr?.vc.present(imgPicker.imagePickerController, animated: true) { [weak self] in
            self?.hideBusyIndicator()
            //(self?.cntr?.vc.view as? UITableView)?.isUserInteractionEnabled = true
        }
    }
}

// MARK: - SingleImagePickerDelegate
extension ProfileCoordinator: SingleImagePickerDelegate {
    func selected(image: UIImage) {
        handler?(image)
        handler = nil
    }
}
