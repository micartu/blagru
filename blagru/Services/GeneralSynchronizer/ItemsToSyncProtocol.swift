//
//  ItemsToSyncProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 25.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol ItemsToSyncProtocol: ItemsToSyncActionsProtocol {
    var items: [SyncItemProtocol] { get }
}

protocol ItemsToSyncActionsProtocol: class {
    func insert(_ item: SyncItemProtocol,
                at index: Int,
                completion: @escaping (() -> Void))
    func replace(_ item: SyncItemProtocol,
                 at index: Int,
                 completion: @escaping (() -> Void))
    func remove(_ item: SyncItemProtocol,
                completion: @escaping (() -> Void))
}
