//
//  MapViewModel+Info.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension MapViewModel: InfoViewModelProtocol {
    func actionJoin() {
        if let id = evId {
            dispatcher?.showInfoFor(eventId: id)
        }
    }
}
