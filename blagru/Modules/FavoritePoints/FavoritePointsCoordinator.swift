//
//  FavoritePointsCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

enum FavoritePoint {
    case chosen(ev: Ev)
}

final class FavoritePointsCoordinator: BaseCoordinator {
	override func start() {
		let vc = BaseTableViewController()
		vc.manualBuilding = true
		vc.viewBuilder = {
			let tv = UITableView(frame: .zero, style: .grouped)
			return (tv, tv, true)
		}
		vc.title = "Favorites Title".localized
        vc.addRefreshControl = false
		vc.initializer = { tv in
            let cells = [
                FavoriteCell.self,
            ]
            for c in cells {
                let identifier = String(describing: c)
                tv.register(UINib(nibName: identifier, bundle: nil),
                            forCellReuseIdentifier: identifier)
            }
            tv.separatorStyle = .none
            tv.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
		}
		let viewModel = FavoritePointsViewModel(
            fetchCache: ServicesAssembler.inject(),
            cache: ServicesAssembler.inject()
        )
		viewModel.dispatcher = self
		viewModel.source = viewModel
		vc.viewModel = viewModel
		cntr = vc
	}
}

// MARK: - FavoritePointsDispatcherProtocol
extension FavoritePointsCoordinator: FavoritePointsDispatcherProtocol {
    func chosen(ev: Ev) {
        //broadcast(result: FavoritePoint.chosen(ev: ev))
        exitWith(result: FavoritePoint.chosen(ev: ev))
    }
}
