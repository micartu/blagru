//
//  MapViewModel+Sync.swift
//  blagru
//
//  Created by Michael Artuerhof on 22.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension MapViewModel {
    internal func checkCategoriesOverNetwork() {
        let ccategories = cats.latestUsedCategories(maxItems: const.maxCategoriesToLoad)
        net.allCategories { [weak self] (netcats, error) in
            if let cs = netcats {
                guard let `self` = self else { return }
                let inCloud = ItemsToSync(items: cs,
                                          interactor: RemoveDiffsWithNetSyncer(vm: self))
                let locally = ItemsToSync(items: ccategories,
                                          interactor: self)
                self.syncer.sync(A: inCloud, B: locally) {
                    self.categories = self.cats.latestUsedCategories(maxItems: const.maxCategoriesToLoad)
                    if self.category == nil {
                        self.category = self.categories.first
                    }
                    self.formFABItems()
                }
            }
        }
    }
}

extension MapViewModel: ItemsToSyncActionsProtocol {
    func insert(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        replace(item, at: index, completion: completion)
    }

    func replace(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        if let c = item as? EvCategory {
            cats.update(categories: [c], completion: completion)
        } else {
            completion()
        }
    }

    func remove(_ item: SyncItemProtocol, completion: @escaping (() -> Void)) {
        if let c = item as? EvCategory {
            cats.delete(categories: [c], completion: completion)
        } else {
            completion()
        }
    }
}

final fileprivate class RemoveDiffsWithNetSyncer: ItemsToSyncActionsProtocol {
    private let vm: ItemsToSyncActionsProtocol
    init(vm: ItemsToSyncActionsProtocol) {
        self.vm = vm
    }

    func insert(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        remove(item, completion: completion)
    }

    func replace(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        // remove item at our side, we've got network as a single source of truth
        remove(item, completion: completion)
    }

    func remove(_ item: SyncItemProtocol, completion: @escaping (() -> Void)) {
        vm.remove(item, completion: completion)
    }
}
