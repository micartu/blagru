//
//  ImageLoader.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class ImageLoader {
    init(transport: NetTransportService, cfg: NetworkConfig, disk: FileSaverService) {
        self.transport = transport
        self.cfg = cfg
        self.disk = disk
    }

    // MARK: Private
    private let disk: FileSaverService
    private let cfg: NetworkConfig
    private let transport: NetTransportService
    private var errs = [String:Int]()
    private var lock = NSRecursiveLock()
    private let imageCache = NSCache<NSString, AnyObject>()

    private struct const {
        static let maxErr = 4
        static let waitTime: TimeInterval = 0.5
    }
}

extension ImageLoader: ImageLoaderService {
    func loadOffline(image: String) -> UIImage? {
        if let im = disk.load(image: image) {
            return im
        }
        return nil
    }

    func errableLoad(image: String, completion: @escaping ImageLoaderErrCompletion) {
        if image.count == 0 { return }

        if let asset = UIImage(named: image) {
            completion(asset, nil)
            return
        }
        lock.lock(); defer { lock.unlock() }
        if let err = self.errs[image] {
            let curErr = err + 1
            if curErr > const.maxErr {
                errs.removeValue(forKey: image)
                let e = NSError(domain: Const.domain,
                                code: -1,
                                userInfo: [
                                    NSLocalizedDescriptionKey: "Too many attempts to load an image".localized
                    ])
                completion(nil, e)
                return
            } else {
                errs[image] = curErr
            }
        } else {
            self.errs[image] = 0
        }
        let deliver = { [weak self] (im: UIImage) in
            self?.lock.lock()
            self?.errs.removeValue(forKey: image)
            self?.lock.unlock()
            completion(im, nil)
        }
        if let im = loadOffline(image: image) {
            deliver(im)
            return
        }
        errorableLoadOf(image) { [weak self] (im, err) in
            if let im = im {
                self?.disk.save(.image, name: image, with: im.jpegData(compressionQuality: 1) ?? Data())
                deliver(im)
            } else {
                delay(const.waitTime) {
                    self?.errableLoad(image: image, completion: completion)
                }
            }
        }
    }

    func load(image: String, completion: @escaping ImageLoaderCompletion) {
        errableLoad(image: image) { (im, err) in
            if let image = im {
                completion(image)
            }
        }
    }

    func invalidateCached(image: String, completion: @escaping (() -> Void)) {
        disk.remove(type: .image, name: image)
        delay(0.3) {
            completion()
        }
    }

    // TODO: remove it after you've got methods on server's side which gives you
    // a smaller version of an image
    private func resizeIfNeeded(image: UIImage) -> UIImage {
        let width = UIScreen.main.bounds.width
        if image.size.width > width {
            let h = heightFrom(width: width, proportional: image.size)
            let im = imageWith(image: image,
                               scaledToSize:  CGSize(width: width, height: h))
            return im
        }
        return image
    }

    func errorableLoadOf(_ image: String, completion: @escaping ImageLoaderErrCompletion) {
        let furl = cfg.baseUrl + image
        transport.getRawQuery(url: furl, success: { [weak self] data in
            if let im = UIImage(data: data) {
                completion(self?.resizeIfNeeded(image: im) ?? im, nil)
            } else {
                let e = NSError(domain: Const.domain,
                                code: -1,
                                userInfo: [NSLocalizedDescriptionKey: "cannot load image:" + image])
                print("error: \(e.localizedDescription)") // put me into archive or remove
                completion(nil, e)
            }
        }, failure: { e in
            completion(nil, e)
        })
    }
}
