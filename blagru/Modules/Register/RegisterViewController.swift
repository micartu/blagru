//
//  RegisterViewController.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class RegisterViewController: BaseViewController {
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var repeatPasswordField: UITextField!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!

    private var borderCount = 0

    var vm: RegisterViewModelProtocol!

    // MARK: Life cycle

    override func viewDidLoad() {
        let btns = [registerButton, eyeButton]
        for b in btns {
            b?.addTarget(self,
                         action: #selector(buttonPressed(_:)),
                         for: .touchUpInside)
        }
        registerButton.setTitle("Register".localized, for: .normal)
        lblDescription.text = "Register description".localized
        let placeholders = [
            "Login placeholder".localized,
            "Name placeholder".localized,
            "EMail placeholder".localized,
            "Password placeholder".localized,
            "Repeat Password placeholder".localized,
        ]
        let icons = [
            "flogin",
            "user",
            "mail",
            "locked",
            "locked",
        ]
        for (i, e) in efields.enumerated() {
            e.leftView = UIImageView(image: UIImage(named: icons[i]))
            e.leftViewMode = .always
            let brd = e.addBorders(edges: [.bottom],
                                   color: UIColor.black,
                                   inset: 0, thickness: 1)
            brd.forEach {
                $0.tag = (const.border + self.borderCount)
                self.borderCount += 1
            }
            e.delegate = self
            e.placeholder = placeholders[i]
        }
        super.viewDidLoad()
        navigationController?.delegate = self
        addDismissKeyboard()
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        imgLogo.tintColor = theme.tintColor
        registerButton.setTitleColor(theme.emptyBackColor, for: .normal)
        registerButton.addBorders(width: 1,
                               color: theme.tintColor,
                               cornerRadius: 5)
        for i in 0..<borderCount {
            if let v = view.viewWithTag(const.border + i) {
                v.backgroundColor = theme.prettyColor
            }
        }
        for e in efields {
            e.leftView?.tintColor = theme.tintColor
        }
        registerButton.backgroundColor = theme.tintColor
        eyeButton.tintColor = theme.tintColor
        view?.backgroundColor = theme.mainBackColor
    }

    // MARK: Private
    @objc private func buttonPressed(_ sender: UIButton) {
        switch sender {
        case registerButton:
            vm.registerPressed()
        case eyeButton:
            vm.togglePassVisibility()
        default:
            ()
        }
    }

    private struct const {
        static let btn = 313
        static let border = 717
    }
}

extension RegisterViewController {
    fileprivate var efields: [UITextField] {
        return [
            loginField,
            nameField,
            emailField,
            passwordField,
            repeatPasswordField,
        ]
    }
}

// MARK: - UINavigationControllerDelegate
extension RegisterViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              willShow viewController: UIViewController,
                              animated: Bool) {
        if viewController == self {
            navigationController
                .setNavigationBarHidden(false, animated: animated)
        }
    }
}

// MARK: - UITextFieldDelegate
extension RegisterViewController: RegisterViewProtocol {
    var login: String {
        return loginField.text ?? ""
    }

    var name: String {
        return nameField.text ?? ""
    }

    var email: String {
        return emailField.text ?? ""
    }

    var password: String {
        return passwordField.text ?? ""
    }

    var repeatPassword: String {
        return repeatPasswordField.text ?? ""
    }

    func set(enabled: Bool) {
        registerButton.isEnabled = enabled
    }

    func pass(show: Bool) {
        passwordField.isSecureTextEntry = !show
        let im: String
        if show {
            im = "eye-crossed"
        } else {
            im = "eye"
        }
        eyeButton.setImage(UIImage(named: im), for: .normal)
    }
}

// MARK: - UITextFieldDelegate
extension RegisterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let next: UITextField?
        switch textField {
        case loginField:
            next = nameField
        case nameField:
            next = emailField
        case emailField:
            next = passwordField
        case passwordField:
            next = repeatPasswordField
        case repeatPasswordField:
            buttonPressed(registerButton)
            fallthrough
        default:
            next = nil
        }
        next?.becomeFirstResponder()
        return true
    }
}

// MARK: - ViewControllerable
extension RegisterViewController: ViewControllerable {
    static var storyboardName: String {
        return "Register"
    }
}
