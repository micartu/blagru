//
//  EventCaptionCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 13/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol EventCaptionCellDelegate: class {
    func joinTouched(id: String)
}

final class EventCaptionCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnJoin: ScaledImageButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnJoin.addTarget(self,
                          action: #selector(cellTapped),
                          for: .touchUpInside)
    }

    @objc private func cellTapped () {
        delegate?.joinTouched(id: id)
    }

    var id = ""
    weak var delegate: EventCaptionCellDelegate? = nil
}
