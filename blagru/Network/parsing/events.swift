//
//  events.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

func parseEvent(from d: NSDictionary, catId: String = "") -> Ev {
    let iid = parseInt(d, key: "id")
    let id = "\(iid)"
    let lat0 = parseString(d, for: "lat0")
    let lon0 = parseString(d, for: "lon0")
    let lat1 = parseString(d, for: "lat1")
    let lon1 = parseString(d, for: "lon1")
    let descr = parseString(d, for: "comment")
    let iuserId = parseInt(d, key: "user_id")
    let userId = "\(iuserId)"
    return  Ev(
        id: id,
        lat0: lat0.toDouble,
        lon0: lon0.toDouble,
        lat1: lat1.toDouble,
        lon1: lon1.toDouble,
        comment: descr,
        category: EvCategoryDesc(id: catId),
        owner: UserDesc(id: userId),
        participants: [])
}
