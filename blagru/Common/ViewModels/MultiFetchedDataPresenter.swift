//
//  MultiFetchedDataPresenter.swift
//  blagru
//
//  Created by Michael Artuerhof on 16.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

enum MultiFetchSection {
    case manualModels
    case fetchedModels(manager: FetchManagerProtocol, source: FetchedDataInteractorInput?)
}

open class MultiFetchedDataPresenter:
    CommonViewModel,
    MultipleSectionInteractorOutput {

    // data source
    weak var source: MultipleSectionInteractorInput?

    fileprivate let cfg: [MultiFetchSection]
    fileprivate var helpers: [MultiFetchedDataHelper]
    fileprivate var sectionUpdates: [Int]
    fileprivate var helpersInitialized = false

    init(cfg: [MultiFetchSection], titles: [String]? = nil) {
        self.cfg = cfg
        if let t = titles, t.count >= cfg.count {
            self.titles = t
        } else {
            self.titles = Array(repeating: "", count: cfg.count)
        }
        helpers = [MultiFetchedDataHelper]()
        sectionUpdates = [Int]()
    }

    // MARK: MultipleSectionInteractorOutput

    func fetched(models: [CellAnyModel], section: Int) {
        self.models[section] = models
        updateData.set(true)
    }

    // MARK: CommonViewModel

    func getContents() {
        if !helpersInitialized {
            helpers.removeAll()
            sectionUpdates.removeAll()
        }
        for (i, c) in cfg.enumerated() {
            switch c {
            case .manualModels:
                source?.loadModels(with: theme, section: i)
            case .fetchedModels(var manager, _):
                if !helpersInitialized {
                    let h = MultiFetchedDataHelper(section: i, vm: self)
                    manager.delegate = h
                    helpers.append(h)
                }
                manager.fetchData()
            }
        }
        if !helpersInitialized {
            helpersInitialized = true
        }
    }

    func refreshContents() {
        models.removeAll()
        getContents()
    }

    func selected(indexPath: IndexPath) {
    }

    func model(for indexPath: IndexPath) -> Any {
        let c = cfg[indexPath.section]
        switch c {
        case .manualModels:
            return (models[indexPath.section])![indexPath.row]
        case .fetchedModels(let manager, let source):
            let nulifiedIndex = IndexPath(row: indexPath.row, section: 0)
            let obj = manager.objectAt(nulifiedIndex)
            let src: FetchedDataInteractorInput
            if let s = source {
                src = s
            } else if let s = self as? FetchedDataInteractorInput {
                src = s
            } else {
                fatalError("Cannot find source for Core Data Models!")
            }
            return src.wrap(entity: obj,
                            indexPath: indexPath,
                            with: theme)
        }
    }

    func modelsCount(for section: Int) -> Int {
        let c = cfg[section]
        switch c {
        case .manualModels:
            return (models[section])!.count
        case .fetchedModels(let manager, _):
            return manager.objectsCountFor(section: 0)
        }
    }

    func title(for section: Int) -> String {
        return titles[section]
    }

    func sectionsCount() -> Int {
        return cfg.count
    }

    func clean() {}

    // MARK: members of CommonViewModel protocol
    var theme: Theme! {
        didSet {
            getContents()
        }
    }

    func beginBatchUpdate(section: Int) {
        runOnMainThread { [weak self] in
            if self?.sectionUpdates.count == 0 {
                self?.batchUpdate.set(true)
            }
            self?.sectionUpdates.append(section)
        }
    }

    func endBatchUpdate(section: Int) {
        runOnMainThread { [weak self] in
            self?.sectionUpdates.removeAll(where: { $0 == section })
            if self?.sectionUpdates.count == 0 {
                self?.batchUpdate.set(false)
                // switch off batch update
                self?.batchUpdate.set(nil)
            }
        }
    }

    var updateItems = Channel<IIndexPathCount?> (value: nil)
    var updateData = Channel<Bool> (value: false)
    var scrollTo = Channel<IMovePath?> (value: nil)
    var removeKeyboard = Channel<Bool> (value: false)
    var busy = Channel<Bool> (value: false)
    var batchUpdate = Channel<Bool?> (value: nil)
    var make = Channel<CellChangeType?> (value: nil)

    // MARK: other members
    internal var titles: [String]
    internal var models = [Int:[CellAnyModel]]()
}

/// helper class for ruling all fetch data controller sections
/// proxied FetchManagerDelegate
private class MultiFetchedDataHelper {
    private let section: Int
    private let vm: MultiFetchedDataPresenter

    init(section: Int, vm: MultiFetchedDataPresenter) {
        self.section = section
        self.vm = vm
    }
}

extension MultiFetchedDataHelper: FetchManagerDelegate {
    func beginBatchUpdate() {
        vm.beginBatchUpdate(section: section)
    }

    func make(change: CellChangeType) {
        func modified(_ index: IndexPath) -> IndexPath {
            var m = index
            m.section = section
            return m
        }
        switch change {
        case .insert(let index):
            vm.make.set(.insert(index: modified(index)))
        case .update(let index):
            vm.make.set(.update(index: modified(index)))
        case .move(let from, let to):
            vm.make.set(.move(from: modified(from), to: modified(to)))
        case .delete(let index):
            vm.make.set(.delete(index: modified(index)))
        }
    }

    func endBatchUpdate() {
        vm.endBatchUpdate(section: section)
    }
}
