//
//  NetFavService.swift
//  blagru
//
//  Created by Michael Artuerhof on 02.07.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol NetFavService {
    func fetchFavorites(completion: @escaping (([Ev]?, NSError?) -> Void))
    func addToFavorites(eventID: String, completion: @escaping ((NSError?) -> Void))
    func removeFromFavorites(eventID: String, completion: @escaping ((NSError?) -> Void))
}
