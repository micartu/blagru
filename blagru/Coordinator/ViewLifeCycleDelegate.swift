//
//  ViewLifeCycleDelegate.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public enum LifeCycleExit {
    case back
}

public protocol ViewLifeCycleDelegate: class {
    func movedBack()
}
