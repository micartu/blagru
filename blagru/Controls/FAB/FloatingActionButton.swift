//
//  FloatingActionButton.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

/// A floating action button
///
@IBDesignable
class FloatingActionButton: UIControl {
    /// The delegate object for the floating action button
    ///
    weak var delegate: FloatingActionButtonDelegate?

    /// The list of action items.
    /// Default is `[]`.
    ///
    /// - SeeAlso: `enabledItems`
    ///
    var items: [ActionItem] = [] {
        didSet {
            itemsWithSetup = itemsWithSetup.intersection(items)
            items.forEach { item in
                setupItem(item)
            }
            configureButtonImage()
        }
    }

    /// The background color of the floating action button.
    /// Default is `UIColor(hue: 0.31, saturation: 0.37, brightness: 0.76, alpha: 1.00)`.
    ///
    /// - SeeAlso: `circleView`
    ///
    @IBInspectable var buttonColor: UIColor {
        get {
            return circleView.color
        }
        set {
            circleView.color = newValue
        }
    }

    /// The background color of the floating action button with highlighted state.
    /// Default is `nil`.
    ///
    /// - SeeAlso: `circleView`
    ///
    @IBInspectable var highlightedButtonColor: UIColor? {
        get {
            return circleView.highlightedColor
        }
        set {
            circleView.highlightedColor = newValue
        }
    }

    /// The shadow color of the floating action button and its children
    ///
    /// Default is `nil`, means there would be no shadow
    ///
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            setupShadow()
        }
    }

    /// The color of circle around the FAB
    /// Default is `UIColor.clear`
    ///
    @IBInspectable var circleColor: UIColor = .clear {
        didSet {
            circleView.color = circleColor
        }
    }

    /// The color of circle around the FAB
    /// Default is `UIColor.white`
    ///
    @IBInspectable var overlayColor: UIColor = .white {
        didSet {
            overlayView.backgroundColor = overlayColor
        }
    }

    /// The image displayed on the button by default.
    /// When only one `ActionItem` is added and `handleSingleActionDirectly` is enabled,
    /// the image from the item is shown instead.
    /// When set to `nil` an image of a plus sign is used.
    /// Default is `nil`.
    ///
    /// - SeeAlso: `imageView`
    ///
    @IBInspectable var buttonImage: UIImage? {
        didSet {
            configureButtonImage()
        }
    }

    /// The size of the image view.
    /// Default is `CGSize.zero`.
    /// If set to `.zero` the actual size of the image is used.
    ///
    /// - SeeAlso: `imageView`
    ///
    var buttonImageSize: CGSize = .zero {
        didSet {
            setNeedsUpdateConstraints()
        }
    }

    /// The tint color of the image view.
    /// Default is `UIColor.white`.
    ///
    /// - Warning: Only template images are colored.
    ///
    /// - SeeAlso: `imageView`
    ///
    @IBInspectable var buttonImageColor: UIColor {
        get {
            return imageView.tintColor
        }
        set {
            imageView.tintColor = newValue
        }
    }

    /// The default diameter of the floating action button.
    /// This is ignored if the size is defined by auto-layout.
    /// Default is `56`.
    ///
    @IBInspectable var buttonDiameter: CGFloat = 56 {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    /// The size of an action item in relation to the floating action button.
    /// Default is `0.75`.
    ///
    @IBInspectable var itemSizeRatio: CGFloat = 0.75

    /// The opening style of the floating action button itself.
    /// Default is `ButtonAnimationConfiguration.rotation()`
    ///
    /// - SeeAlso: `ButtonAnimationConfiguration`
    /// - SeeAlso: `itemAnimationConfiguration`
    ///
    var buttonAnimationConfiguration: ButtonAnimationConfiguration = .rotation()

    /// The opening style of the action items.
    /// Default is `ItemAnimationConfiguration.popUp()`
    ///
    /// - SeeAlso: `ItemAnimationConfiguration`
    /// - SeeAlso: `buttonAnimationConfiguration`
    ///
    var itemAnimationConfiguration: ItemAnimationConfiguration = .popUp()

    /// When enabled and only one action item is added, the floating action button will not open,
    /// but the action from the action item will be executed directly when the button is tapped.
    /// Also the image of the floating action button will be replaced with the one from the action item.
    ///
    /// Default is `true`.
    ///
    @IBInspectable var handleSingleActionDirectly: Bool = true {
        didSet {
            configureButtonImage()
        }
    }

    /// When enabled, the floating action button will close after an action item was tapped,
    /// otherwise the action button will stay open and has to be closed explicitly.
    ///
    /// Default is `true`.
    ///
    /// - SeeAlso: `close`
    ///
    @IBInspectable var closeAutomatically: Bool = true

    /// The current state of the floating action button.
    /// Possible values are
    ///   - `.opening`
    ///   - `.open`
    ///   - `.closing`
    ///   - `.closed`
    ///
    var buttonState: FloatingActionButtonState = .closed

    // content properties

    /// The round background view of the floating action button.
    /// Read only.
    /// could be modified directly only for animation purposes!
    ///
    /// - SeeAlso: `buttonColor`
    /// - SeeAlso: `highlightedButtonColor`
    ///
    lazy var circleView: CircleView = {
        let view = CircleView()
        view.isUserInteractionEnabled = false
        view.color = circleColor
        return view
    }()

    /// The image view of the floating action button.
    /// Read only.
    /// could be modified directly only for animation purposes!
    ///
    /// - Warning: Setting the image of the `imageView` directly will not work.
    ///            Use `buttonImage` instead.
    ///
    /// - SeeAlso: `buttonImage`
    /// - SeeAlso: `buttonImageColor`
    ///
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = false
        imageView.backgroundColor = .clear
        imageView.tintColor = .blue
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    /// The overlay view.
    /// Default background color is `UIColor(white: 0, alpha: 0.5)`.
    /// Read only.
    ///
    fileprivate(set) lazy var overlayView: UIControl = {
        let control = UIControl()
        control.isUserInteractionEnabled = true
        control.backgroundColor = overlayColor
        control.addTarget(self, action: #selector(overlayViewWasTapped), for: .touchUpInside)
        control.isEnabled = false
        control.alpha = 0
        return control
    }()

    /// Initializes and returns a newly allocated floating action button object with the specified frame rectangle.
    ///
    /// - Parameter frame: The frame rectangle for the floating action button, measured in points.
    ///                    The origin of the frame is relative to the superview in which you plan to add it.
    ///                    This method uses the frame rectangle to set the center and bounds properties accordingly.
    ///
    /// - Returns: An initialized floating action button object.
    ///
    /// - SeeAlso: init?(coder: NSCoder)
    ///
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    /// Returns an object initialized from data in a given unarchiver.
    ///
    /// - Parameter aDecoder: An unarchiver object.
    ///
    /// - Returns: `self`, initialized using the data in decoder.
    ///
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    /// Initializes and returns a newly allocated floating action button object with the specified image and action.
    ///
    /// - Parameter image: The image of the action item. Default is `nil`.
    /// - Parameter action: The action handler of the action item. Default is `nil`.
    ///
    /// - Returns: An initialized floating action button object.
    ///
    /// - SeeAlso: init(frame: CGRect)
    ///
    convenience init(image: UIImage, action: ((ActionItem) -> Void)? = nil) {
        self.init()
        addItem(image: image, action: action)
    }

    internal lazy var itemContainerView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    internal var currentButtonAnimationConfiguration: ButtonAnimationConfiguration?
    internal var currentItemAnimationConfiguration: ItemAnimationConfiguration?
    internal var openItems: [ActionItem] = []

    fileprivate var defaultItemConfiguration: ((ActionItem) -> Void)?
    fileprivate var itemsWithSetup: Set<ActionItem> = []

    fileprivate var dynamicConstraints: [NSLayoutConstraint] = []
}

// MARK: - Public Methods

extension FloatingActionButton {
    /// Add an action item with title, image and action to the list of items.
    /// The item will be pre configured with the default values.
    ///
    /// - Parameter image: The image of the action item. Default is `nil`.
    /// - Parameter action: The action handler of the action item. Default is `nil`.
    ///
    /// - Returns: The item that was added. This can be configured after it has been added.
    ///
    @discardableResult func addItem(image: UIImage? = nil,
                                    action: ((ActionItem) -> Void)? = nil) -> ActionItem {
        let item = ActionItem()
        item.imageView.image = image
        item.action = action

        addItem(item)

        return item
    }

    /// Add an action item to the list of items.
    /// The item will be updated with the default configuration values.
    ///
    /// - Parameter item: The action item.
    ///
    func addItem(_ item: ActionItem) {
        items.append(item) // this will call `didSet` of `items`
    }

    /// Remove an action item from the list of items.
    ///
    /// - Parameter item: The action item.
    ///
    /// - Returns: The item that was removed. `nil` if `item` was not found.
    ///
    @discardableResult func removeItem(_ item: ActionItem) -> ActionItem? {
        guard let index = items.firstIndex(of: item) else {
            return nil
        }
        return removeItem(at: index)
    }

    /// Remove and returns the action item at the specified position in the list of items.
    ///
    /// - Parameter index: The index of the action item. `index` must
    ///   be a valid index of the list of items.
    ///
    /// - Returns: The item that was removed.
    ///
    @discardableResult func removeItem(at index: Int) -> ActionItem {
        return items.remove(at: index)
    }

    /// Calls the given closure on each item that is or was added to the floating action button.
    /// Default is `nil`.
    ///
    ///   ````
    ///   let actionButton = FloatingActionButton()
    ///
    ///   actionButton.configureDefaultItem { item in
    ///       item.imageView.contentMode = .scaleAspectFill
    ///
    ///       item.titleLabel.font = .systemFont(ofSize: 14)
    ///
    ///       item.layer.shadowColor = UIColor.black.cgColor
    ///       item.layer.shadowOpacity = 0.3
    ///       item.layer.shadowOffset = CGSize(width: 1, height: 1)
    ///       item.layer.shadowRadius = 0
    ///   }
    ///   ````
    ///
    /// - Parameter body: A closure that takes an action item as a parameter.
    ///
    func configureDefaultItem(_ body: ((ActionItem) -> Void)?) {
        defaultItemConfiguration = body
        items.forEach { item in
            defaultItemConfiguration?(item)
        }
    }

    /// All items that will be shown when floating action button is opened.
    /// This excludes hidden items and items that have user interaction disabled.
    ///
    var enabledItems: [ActionItem] {
        return items.filter { item -> Bool in
            !item.isHidden && item.isUserInteractionEnabled
        }
    }
}

// MARK: - UIView

extension FloatingActionButton {
    /// The natural size for the floating action button.
    ///
    open override var intrinsicContentSize: CGSize {
        return CGSize(width: buttonDiameter, height: buttonDiameter)
    }

    /// Updates constraints for the view.
    ///
    open override func updateConstraints() {
        updateDynamicConstraints()
        super.updateConstraints()
    }

    /// Tells the view that its superview changed.
    ///
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if superview == nil {
            close(animated: false)
            removeRelatedViewsFromSuperview()
        }
    }
}

// MARK: - Setup

fileprivate extension FloatingActionButton {
    func setup() {
        backgroundColor = .clear
        clipsToBounds = false
        isUserInteractionEnabled = true
        addTarget(self, action: #selector(buttonWasTapped), for: .touchUpInside)

        setupShadow()

        circleView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false

        addSubview(circleView)
        circleView.addSubview(imageView)

        createStaticConstraints()
        createDynamicConstraints()

        configureButtonImage()
    }

    func setupShadow() {
        if let color = shadowColor {
            layer.shadowColor = color.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 1)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = 2
        } else {
            layer.shadowOffset = CGSize.zero
            layer.shadowOpacity = 0
            layer.shadowRadius = 0
            layer.shadowColor = UIColor.clear.cgColor
        }
    }

    func createStaticConstraints() {
        circleView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        circleView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        circleView.widthAnchor.constraint(equalTo: circleView.heightAnchor).isActive = true
        circleView.widthAnchor.constraint(lessThanOrEqualTo: widthAnchor).isActive = true
        circleView.heightAnchor.constraint(lessThanOrEqualTo: heightAnchor).isActive = true
        let widthConstraint = circleView.widthAnchor.constraint(equalTo: widthAnchor)
        widthConstraint.priority = .defaultHigh
        widthConstraint.isActive = true
        let heightConstraint = circleView.heightAnchor.constraint(equalTo: heightAnchor)
        heightConstraint.priority = .defaultHigh
        heightConstraint.isActive = true

        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: circleView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: circleView.centerYAnchor).isActive = true

        imageView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        imageView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .vertical)
        circleView.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        circleView.setContentHuggingPriority(.fittingSizeLevel, for: .vertical)
    }

    func configureButtonImage() {
        imageView.image = currentButtonImage
    }

    func setupItem(_ item: ActionItem) {
        guard !itemsWithSetup.contains(item) else {
            return
        }
        itemsWithSetup.insert(item)

        item.imageView.tintColor = buttonColor

        item.layer.shadowColor = layer.shadowColor
        item.layer.shadowOpacity = layer.shadowOpacity
        item.layer.shadowOffset = layer.shadowOffset
        item.layer.shadowRadius = layer.shadowRadius

        item.addTarget(self, action: #selector(itemWasTapped(sender:)), for: .touchUpInside)

        defaultItemConfiguration?(item)
    }

    func updateDynamicConstraints() {
        NSLayoutConstraint.deactivate(dynamicConstraints)
        dynamicConstraints.removeAll()
        createDynamicConstraints()
        NSLayoutConstraint.activate(dynamicConstraints)
        setNeedsLayout()
    }

    func createDynamicConstraints() {
        dynamicConstraints.append(contentsOf: imageSizeConstraints)
    }

    var imageSizeConstraints: [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []
        if buttonImageSize == .zero {
            let multiplier = CGFloat(1 / sqrt(2))
            constraints.append(imageView.widthAnchor.constraint(lessThanOrEqualTo: circleView.widthAnchor, multiplier: multiplier))
            constraints.append(imageView.heightAnchor.constraint(lessThanOrEqualTo: circleView.heightAnchor, multiplier: multiplier))
        } else {
            constraints.append(imageView.widthAnchor.constraint(equalToConstant: buttonImageSize.width))
            constraints.append(imageView.heightAnchor.constraint(equalToConstant: buttonImageSize.height))
        }
        return constraints
    }
}

// MARK: - Helper

internal extension FloatingActionButton {
    var currentButtonImage: UIImage? {
        if let img = buttonImage {
            return img
        } else if isSingleActionButton, let image = enabledItems.first?.imageView.image {
            return image
        }
        return nil
    }

    var isSingleActionButton: Bool {
        return handleSingleActionDirectly && enabledItems.count == 1
    }
}

// MARK: - Actions

fileprivate extension FloatingActionButton {
    @objc func buttonWasTapped() {
        switch buttonState {
        case .open, .opening:
            close()

        case .closed:
            handleSingleActionOrOpen()

        default:
            break
        }
    }

    @objc func itemWasTapped(sender: ActionItem) {
        guard buttonState == .open || buttonState == .opening else {
            return
        }

        if closeAutomatically {
            close()
        }
        sender.callAction()
    }

    @objc func overlayViewWasTapped() {
        close()
    }

    func handleSingleActionOrOpen() {
        guard !enabledItems.isEmpty else {
            return
        }

        if isSingleActionButton {
            let item = enabledItems.first
            item?.callAction()
        } else {
            open()
        }
    }
}
