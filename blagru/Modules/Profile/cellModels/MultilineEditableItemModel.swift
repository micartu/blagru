//
//  MultilineEditableItemModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 08.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

struct MultilineEditableItemModel: CommonEditableModel {
    let theme: Theme
    let id: String
    var focused: Bool
    let title: String
    var placeholder: String
    var text: String
    var customizer: ((Theme, MultilineEditableItemCell) -> Void)?
    let delegate: MultilineEditableItemCellDelegate?

    func setup(cell: MultilineEditableItemCell) {
        cell.id = id
        cell.delegate = delegate

        customizer?(theme, cell)

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.regularFont.withSize(theme.sz.middle4)
        cell.lblTitle.textColor = theme.secondaryTextColor

        cell.setEditableText(text)
        cell.editItem.isUserInteractionEnabled = true
        cell.editItem.isEditable = true
        cell.editItem.layer.borderWidth = 1
        cell.editItem.layer.borderColor = theme.borderColor.cgColor
        cell.editItem.layer.backgroundColor = theme.inactiveColor.cgColor
        cell.editItem.layer.cornerRadius = 5
        cell.editItem.clipsToBounds = true

        if focused {
            delay(0.2) { [weak cell] in
                cell?.editItem.becomeFirstResponder()
            }
        }
    }
}
