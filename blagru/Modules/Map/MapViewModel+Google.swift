//
//  MapViewModel+Google.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import GoogleMaps
import Repeat

extension MapViewModel: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        camera = position
        zoom = position.zoom
        saveCurrentPosition(of: mapView)
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        switch mode {
        case .search:
            fallthrough
        case .add:
            if let s = selecting {
                let pt = pointer?.add(point: coordinate,
                                      type: s,
                                      state: .hightlighted)
                switch s {
                case .A:
                    pt0Ind = pt
                case .B:
                    pt1Ind = pt
                }
                if let i0 = pt0Ind,
                    let i1 = pt1Ind {
                    pointer?.connect(ind0: i0, ind1: i1)
                }
            }
        case .idle:
            ()
        }
        switchModeIfNeeded()
    }

    func resetPointSelectionState() {
        if let i0 = spt0Ind,
            let i1 = spt1Ind {
            pointer?.changePointWith(ind: i0, state: .idle)
            pointer?.changePointWith(ind: i1, state: .idle)
        }
        pointer?.removeArcPolyline()
    }

    @discardableResult
    internal func changeSelectedPointStateTo(ind0: Int64, ind1: Int64) -> Bool {
        if pointer?.sameArc(ind0: ind0, ind1: ind1) == false {
            resetPointSelectionState()
            if ind0 == pt0Ind || ind0 == pt1Ind { // a point, which user selected
                pointer?.drawArcPolyline(ind0: ind0, ind1: ind1)
                view?.makeChooserView(shrink: false)
                view?.makeInfoPanel(hidden: true, animated: true)
                view?.make(btns: [.favorites, .savedList], hidden: false)
                spt0Ind = nil
                spt1Ind = nil
            } else { // a downloaded point
                pointer?.changePointWith(ind: ind0, state: .pt)
                pointer?.changePointWith(ind: ind1, state: .pt)
                pointer?.drawArcPolyline(ind0: ind0, ind1: ind1)
                if let ev = pointer?.event(ind: ind0) {
                    view?.makeChooserView(shrink: true)
                    evId = ev.id
                    // TODO: replace user's ID with his name, maybe from Cache?
                    let d = "Created by:".localized + " " + ev.owner.id + "\n" +
                        "Amount of Participants".localized + " \(ev.participants.count)"
                    view?.make(btns: [.favorites, .savedList], hidden: true)
                    view?.showInfoMsg(d)
                    view?.makeInfoPanel(hidden: false, animated: true)
                } else {
                    evId = nil
                    view?.make(btns: [.favorites, .savedList], hidden: false)
                    view?.makeChooserView(shrink: false)
                    view?.makeInfoPanel(hidden: true, animated: true)
                }
                spt0Ind = ind0
                spt1Ind = ind1
            }
            return true
        }
        return false
    }

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let ind = marker.userData as? Int64, // existing point?
            let ind1 = pointer?.findConnectionTo(point: ind) {
            return changeSelectedPointStateTo(ind0: ind, ind1: ind1)
        }
        return false
    }

    // MARK: saving / retrieving map position
    internal func saveCurrentPosition(of mapView: GMSMapView) {
        if posTimer != nil {
            posTimer?.removeAllObservers(thenStop: true)
            posTimer = nil
        }
        posTimer = Repeater.once(
            after: .seconds(const.defTimeout),
            queue: DispatchQueue.main
        ) { [weak self] _ in
            let sz = mapView.bounds.size
            let lt = mapView.projection.coordinate(for: CGPoint.zero)
            let rb = mapView.projection.coordinate(for: CGPoint(x: sz.width, y: sz.height))
            self?.storage.savedMapPosition.value = SavedMapPosition(
                lat: (lt.latitude + rb.latitude) / 2,
                lon: (lt.longitude + rb.longitude) / 2,
                zoom: self?.zoom ?? -1
            )
        }
    }

    internal func extractSavedMapPos() -> (CLLocationCoordinate2D?, Float) {
        if let p = storage.savedMapPosition.value {
            return (CLLocationCoordinate2D(latitude: p.lat,
                                           longitude: p.lon), p.zoom)
        }
        return (nil, -1)
    }
}

extension MapViewModel {
}

// MARK: - K-V-Contrainer definitions for needed fields
fileprivate extension KeyValueStorageService {
    var savedMapPosition: KeyValueContainer<SavedMapPosition> {
        makeContainer()
    }
}

// MARK: - needed types

fileprivate struct SavedMapPosition: Codable {
    let lat: Double
    let lon: Double
    let zoom: Float
}
