//
//  LocatorService.swift
//  blagru
//
//  Created by Michael Artuerhof on 25.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol LocatorDelegate: class {
    func location(enabled: Bool)
    func location(lat: Double, lon: Double)
}

protocol LocatorService {
    var delegate: LocatorDelegate? { get set }
    var lon: Double { get }
    var lat: Double { get }
    var acc: Double { get }
    func setActiveMode(_ active: Bool)
}
