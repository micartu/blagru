//
//  Double+Radians.swift
//  blagru
//
//  Created by Michael Artuerhof on 11.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension Double {
    var toRadians: Double {
        return Double.pi * self / 180
    }
}

extension Double {
    var toDegrees: Double {
        return 180 * self / Double.pi
    }
}
