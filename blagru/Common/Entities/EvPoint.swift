//
//  EvPoint.swift
//  blagru
//
//  Created by Michael Artuerhof on 16.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct EvPoint {
    let ind: Int64
    let lon: Double
    let lat: Double
    let typeA: Bool
    var state: PointState
    var connectedInd: Int64?
    var event: EvDescProtocol?
}
