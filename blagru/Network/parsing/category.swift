//
//  category.swift
//  blagru
//
//  Created by Michael Artuerhof on 22.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

func parseCategory(from d: NSDictionary) -> EvCategory {
    let iid = parseInt(d, key: "id")
    let id = "\(iid)"
    let name = parseString(d, for: "name")
    let img = parseString(d, for: "image")
    let icon = img.count > 0 ? img : name
    return EvCategory(
        id: id,
        name: name,
        icon: icon)
}
