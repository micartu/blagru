//
//  ViewControllerable.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ViewControllerable: class {
    static var storyboardName: String { get }
}

extension ViewControllerable where Self: UIViewController {
    static func create() -> Self {
        let storyboard = self.storyboard()
        let className = NSStringFromClass(Self.self)
        let finalClassName = className.components(separatedBy: ".").last!

        let viewControllerId = finalClassName
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerId)

        return viewController as! Self
    }

    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: storyboardName, bundle: nil)
    }
}
