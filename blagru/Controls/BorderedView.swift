//
//  BorderedView.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class BorderedView: UIView {
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable
    var customBorderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = customBorderColor.cgColor
        }
    }

    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
}
