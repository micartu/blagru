//
//  GoogleSignAppDelegate.swift
//  blagru
//
//  Created by Michael Artuerhof on 19.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

final class GoogleSignAppDelegate: NSObject { }

extension GoogleSignAppDelegate: AppDelegateProtocol {
    func description() -> String {
        return "GoogleSignAppDelegate"
    }

    func initModule(application: UIApplication,
                    options: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Use Firebase library to configure APIs
        FirebaseApp.configure()

        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        return true
    }
}

extension GoogleSignAppDelegate: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!,
              didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        let obj: Any
        if let e = error {
            print("GOOGLE OAuth err: \(e.localizedDescription)")
            obj = e
        } else {
            let i = SocialData(netType: Const.social.GP,
                               accessToken: user.authentication.accessToken,
                               idToken: user.userID,
                               expire: user.authentication.accessTokenExpirationDate)
            obj = i
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Const.notification.authSocial),
                                        object: obj,
                                        userInfo: nil)
    }
}
