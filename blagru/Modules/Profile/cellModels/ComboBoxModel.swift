//
//  ComboBoxModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

struct ComboBoxModel: CommonCell {
    let theme: Theme
    let id: String
    let title: String
    var descr: [String: Any]
    var customizer: ((Theme, TitledViewCell) -> Void)?
    var delegate: RadioItemsCellDelegate?

    func setup(cell: TitledViewCell) {
        cell.id = id
        cell.radioDelegate = delegate

        customizer?(theme, cell)

        if title.count > 0 {
            cell.lblTitle.showView()
            cell.lblTitle.text = title
            cell.lblTitle.font = theme.regularFont.withSize(theme.sz.middle4)
            cell.lblTitle.textColor = theme.secondaryTextColor
        } else {
            cell.lblTitle.hideView()
        }

        cell.setupComboBoxFrom(descr: descr, theme: theme)
    }
}
