//
//  Cache+User.swift
//  blagru
//
//  Created by Michael Artuerhof on 09/07/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheUserService {
    func userExistsWith(id: String) -> Bool {
        return existsUserId(id, in: getContext())
    }

    func getUserWith(id: String) -> User? {
        if let u = getCUserWithID(id, in: getContext()) {
            return convert(user: u)
        }
        return nil
    }

    func create(user: User, completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.create(user: user, in: context)
        }, completion: completion)
    }

    func update(user: User, completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let u = self.getCUserWithID(user.id, in: context) {
                self.update(user: u, from: user)
            } else {
                self.create(user: user, in: context)
            }
        }, completion: completion)
    }

    func deleteUserWith(id: String, completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.delete(id: id, in: context)
        }, completion: completion)
    }

    // MARK: - Internal

    internal func getCurrentCUser(in context: NSManagedObjectContext) -> CUser? {
        let id = secret.userID
        if let u = getCUserWithID(id, in: context) {
            return u
        }
        return nil
    }

    internal func getCUserWithID(_ id: String, in context: NSManagedObjectContext) -> CUser? {
        let predicate = getIDStrPredicate(value: id)
        return CUser.mr_findFirst(with: predicate, in: context)
    }

    internal func convert(user cu: CUser) -> User {
        return User(id: cu.id!,
                    login: cu.login ?? "",
                    name: cu.name ?? "",
                    email: cu.login ?? "",
                    avatar: cu.avatar ?? "",
                    password: "")
    }

    @discardableResult
    internal func create(user: User, in context: NSManagedObjectContext) -> CUser {
        let u = CUser.mr_createEntity(in: context)!
        update(user: u, from: user)
        return u
    }

    internal func update(user cu: CUser, from u: User) {
        cu.id = u.id
        cu.login = u.login
        cu.avatar = u.avatar
        cu.email = u.email
        cu.name = u.name
    }

    internal func existsUserId(_ id: String, in context: NSManagedObjectContext) -> Bool {
        return existsEntityWith(id,
                                EntityType: CUser.self,
                                in: context)
    }

    internal func deleteCUser(_ cu: CUser, in context: NSManagedObjectContext) {
        cu.mr_deleteEntity(in: context)
    }

    internal func delete(id: String, in context: NSManagedObjectContext) {
        let predicate = getIDStrPredicate(value: id)
        if let users = CUser.mr_findAll(with: predicate, in: context) as? [CUser] {
            for u in users {
                deleteCUser(u, in: context)
            }
        }
    }
}

// part of CacheFetchRequestsService
extension Cache {
    func fetchDataControllerForParticipantsOfEvent(evId: String) -> FetchManagerProtocol {
        let context = NSManagedObjectContext.mr_default()
        let evPred = NSPredicate(format: "ANY participating_events.ev_id == [n] %@", evId)
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [evPred])
        let request = CUser.mr_requestAllSorted(by: "login",
                                                ascending: false,
                                                with: predicate,
                                                in: context)
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = const.szBatch
        let frc = NSFetchedResultsController(fetchRequest: request,
                                             managedObjectContext: context,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil) as! NSFetchedResultsController<CUser>
        return FetchManager(fetchedResultsController: frc,
                            converter: self)
    }
}
