//
//  PtChooserViewProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 04.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

enum dstButtonType {
    case source
    case dest
    case stop
}

protocol PtChooserViewProtocol {
    func blink(button: dstButtonType)
    func showMsg(_ msg: String, timeout: TimeInterval)
    func makeStackView(hidden: Bool,
                       animated: Bool,
                       completion: (() -> Void)?)
    func make(button: OptionalButtonType, hidden: Bool)
    func make(button: OptionalButtonType, active: Bool)
    func makeButtons(hide: [OptionalButtonType], appear: [OptionalButtonType])
}

extension PtChooserViewProtocol {
    func makeStackView(hidden: Bool,
                       animated: Bool) {
        makeStackView(hidden: hidden, animated: animated, completion: nil)
    }

    func make(btns: [OptionalButtonType], hidden: Bool) {
        for b in btns {
            make(button: b, hidden: hidden)
        }
    }
}
