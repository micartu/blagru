//
//  CircleView.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

/// A colored circle with an highlighted state
///
@IBDesignable class CircleView: UIView {
    /// The color of the circle.
    ///
    @IBInspectable var color: UIColor = .lightGray {
        didSet {
            setNeedsDisplay()
        }
    }

    /// The color of the circle when highlighted. Default is `nil`.
    ///
    @IBInspectable var highlightedColor: UIColor? {
        didSet {
            setNeedsDisplay()
        }
    }

    /// A Boolean value indicating whether the circle view draws a highlight.
    /// Default is `false`.
    ///
    var isHighlighted = false {
        didSet {
            setNeedsDisplay()
        }
    }

    internal override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    /// Returns an object initialized from data in a given unarchiver.
    ///
    /// - Parameter aDecoder: An unarchiver object.
    ///
    /// - Returns: `self`, initialized using the data in decoder.
    ///
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    /// Draws the receiver’s image within the passed-in rectangle
    /// Overrides `draw(rect: CGRect)` from `UIView`.
    ///
    override func draw(_: CGRect) {
        drawCircle(inRect: bounds)
    }

    fileprivate var highlightedColorFallback = UIColor.lightGray
}

// MARK: - Private Methods

fileprivate extension CircleView {
    func setup() {
        backgroundColor = .clear
    }

    func drawCircle(inRect rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()

        let diameter = min(rect.width, rect.height)
        var circleRect = CGRect()
        circleRect.size.width = diameter
        circleRect.size.height = diameter
        circleRect.origin.x = (rect.width - diameter) / 2
        circleRect.origin.y = (rect.height - diameter) / 2

        let circlePath = UIBezierPath(ovalIn: circleRect)
        currentColor.setFill()
        circlePath.fill()

        context.restoreGState()
    }

    var currentColor: UIColor {
        if !isHighlighted {
            return color
        }

        if let highlightedColor = highlightedColor {
            return highlightedColor
        }

        return highlightedColorFallback
    }
}
