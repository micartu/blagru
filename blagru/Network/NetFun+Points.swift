//
//  NetFun+Points.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetPointsService {
    func pointsIn(catId: String,
                  lat0: Double,
                  lon0: Double,
                  lat1: Double,
                  lon1: Double,
                  completion: @escaping (([Ev]?, NSError?) -> Void)) {
        let k = 6
        let dd: NSDictionary = [
            "type_id": catId.toInt,
            "lat0": lat0.rounded(toPlaces: k),
            "lon0": lon0.rounded(toPlaces: k),
            "lat1": lat1.rounded(toPlaces: k),
            "lon1": lon1.rounded(toPlaces: k),
        ]
        let d = dd.toJSONString()?.data(using: .utf8) ?? Data()
        post(url: "/events/get/", data: d, success: { (ans: NSDictionary) in
            if let evs = ans["events"] as? [NSDictionary] {
                let points = evs.map { parseEvent(from: $0, catId: catId) }
                completion(points, nil)
            } else {
                let e = NSError(domain: Const.domain,
                                code: -1,
                                userInfo: [NSLocalizedDescriptionKey: "no 'events' field in the answer".localized])
                completion(nil, e)
            }
        }, failure: { e in
            completion(nil, e)
        })
    }
}
