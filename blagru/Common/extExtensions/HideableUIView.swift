//
//  HideableUIView.swift
//  blagru
//
//  Created by Michael Artuerhof on 11.05.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

private var hideableUIViewConstraintsKey: UInt8 = 0

extension UIView {
    private var _parentConstraintsReference: [NSLayoutConstraint]! {
        get {
            return objc_getAssociatedObject(self, &hideableUIViewConstraintsKey) as? [NSLayoutConstraint] ?? []
        }
        set {
            objc_setAssociatedObject(self, &hideableUIViewConstraintsKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    func hideView() {
        self.isHidden = true
        if let parentView = self.superview, _parentConstraintsReference.count == 0 {
            // get the constraints that involve this view to re-add them when the view is shown
            for parentConstraint in parentView.constraints {
                if parentConstraint.firstItem === self || parentConstraint.secondItem === self {
                    _parentConstraintsReference.append(parentConstraint)
                }
            }
            parentView.removeConstraints(_parentConstraintsReference)
        }
    }

    func showView() {
        // reapply any previously existing constraints
        if let parentView = self.superview {
            parentView.addConstraints(_parentConstraintsReference)
        }

        _parentConstraintsReference = []
        self.isHidden = false
    }
}
