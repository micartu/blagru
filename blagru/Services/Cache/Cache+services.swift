//
//  Cache+services.swift
//  blagru
//
//  Created by Michael Artuerhof on 11.05.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension Cache: ServiceLocatorModulProtocol {
    func registerServices(serviceLocator s: ServicesAssembler) {
        s.registerSingleton(singletonInstance: self as CacheUserService)
        s.registerSingleton(singletonInstance: self as CacheEvService)
        s.registerSingleton(singletonInstance: self as CachePointService)
        s.registerSingleton(singletonInstance: self as CacheEvAndPointService)
        s.registerSingleton(singletonInstance: self as CacheCategoryService)
        s.registerSingleton(singletonInstance: self as CacheFetchRequestsService)
    }
}
