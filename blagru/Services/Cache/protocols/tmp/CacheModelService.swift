//
//  CacheModelService.swift
//  blagru
//
//  Created by Michael Artuerhof on 19.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol ModelDescProtocol {
    var id: String { get }
}

struct ModelDesc: ModelDescProtocol {
    let id: String
}

protocol CacheModelService {
    func itemExistsWith(barcode: String, serial: String) -> Bool
    func itemWith(barcode: String) -> Model?
    func itemWith(barcode: String, serial: String) -> Model?
    func create(items: [Model],
                completion: @escaping (() -> Void))
    func update(items: [Model],
                completion: @escaping (() -> Void))
    func itemsForCurrentUser() -> [Model]
    func delete(itemEANs: [ModelDescProtocol], completion: @escaping (() -> Void))
    func deleteAllModelsForCurrentUser(completion: @escaping (() -> Void))
    func deleteAllModels(completion: @escaping (() -> Void))
}
