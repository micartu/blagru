//
//  CEvPoint+CoreDataProperties.swift
//  blagru
//
//  Created by Michael Artuerhof on 16.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CEvPoint {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CEvPoint> {
        return NSFetchRequest<CEvPoint>(entityName: "CEvPoint")
    }

    @NSManaged public var connectedInd: Int64
    @NSManaged public var ind: Int64
    @NSManaged public var lon: Double
    @NSManaged public var lat: Double
    @NSManaged public var typeA: Bool
    @NSManaged public var state: String?
    @NSManaged public var event: CEv?

}
