//
//  Decodable+JSON.swift
//  blagru
//
//  Created by Michael Artuerhof on 07.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension Decodable {
    init?(fromJSONString JSONString: String?) {
        guard let JSONString = JSONString else { return nil }
        do {
            if let data = JSONString.data(using: .utf8) {
                self = try JSONDecoder().decode(Self.self, from: data)
            } else {
                return nil
            }
        } catch {
            print("fromJSONString failed: \(error)")
            return nil
        }
    }
}
