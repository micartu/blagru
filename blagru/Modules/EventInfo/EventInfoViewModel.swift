//
//  EventInfoViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 13/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol EventInfoDispatcherProtocol: class, Authorizable {
}

protocol EventInfoViewModelProtocol {
}

final class EventInfoViewModel: MultiFetchedDataPresenter {
	weak var dispatcher: (AlertProtocol & EventInfoDispatcherProtocol)?
    private let net: NetEventService
    private let secret: SecretService
    private let eventId: String
    private let cache: CacheEvService
    private weak var imageLoader: ImageLoaderService!

    init(eventId: String,
         fetchCache: CacheFetchRequestsService,
         evCache: CacheEvService,
         secret: SecretService,
         net: NetEventService,
         imageLoader: ImageLoaderService) {
        self.eventId = eventId
        self.net = net
        self.secret = secret
        self.cache = evCache
        self.imageLoader = imageLoader
        let manager = fetchCache.fetchDataControllerForParticipantsOfEvent(evId: eventId)
        let cfg: [MultiFetchSection] = [
            .manualModels,
            .fetchedModels(manager: manager, source: nil),
        ]
        super.init(cfg: cfg)
	}
}

// MARK: - SingleSectionInteractorInput
extension EventInfoViewModel: MultipleSectionInteractorInput {
    func loadModels(with theme: Theme, section: Int) {
        if let ev = cache.EvWith(id: eventId) {
            // TODO: replace user's ID with the name from cache?
            let d = "Created by:".localized + " " + ev.owner.id + "\n" +
                "Amount of Participants".localized + " \(ev.participants.count)"
            let joined = ev.participants.contains(where: { $0.id == secret.userID })
            let c = EventCaptionModel(
                theme: theme,
                id: "caption",
                title: "Event: \(eventId)",
                descr: d,
                joined: joined,
                image: "concert",
                imgLoader: imageLoader,
                delegate: self)
            fetched(models: [c], section: section)
        } else {
            fetched(models: [], section: section)
        }
	}
}

// MARK: - FetchedDataInteractorInput
extension EventInfoViewModel: FetchedDataInteractorInput {
    func wrap(entity: Any,
              indexPath: IndexPath,
              with theme: Theme) -> CellAnyModel {
        if let i = entity as? User {
            return EventInfoModel(
                theme: theme,
                id: i.login,
                name: i.name,
                image: i.avatar,
                imgLoader: imageLoader,
                delegate: self)
        }
        fatalError("EventInfoViewModel: Asked about an unknown entity: \(entity)")
    }
}

// MARK: - EventInfoCellDelegate
extension  EventInfoViewModel: EventInfoCellDelegate {
    func cellTouched(id: String) {
        print("touched: \(id)") // TODO: delete me
    }
}

// MARK: - EventCaptionCellDelegate
extension  EventInfoViewModel: EventCaptionCellDelegate {
    func joinTouched(id: String) {
        dispatcher?.askAuthorizationAndExecute { [weak self] in
            self?.net.join(eventId: id) { ev, error in
                if let ev = ev {
                    print("joined: \(ev)") // TODO: delete me
                }
            }
        }
    }
}

// MARK: - EventInfoViewModelProtocol
extension  EventInfoViewModel: EventInfoViewModelProtocol {
}
