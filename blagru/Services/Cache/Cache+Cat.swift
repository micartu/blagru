//
//  Cache+Cat.swift
//  blagru
//
//  Created by Michael Artuerhof on 15.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheCategoryService {
    func categoryExistsWith(id: String) -> Bool {
        return existsEntityWithPredicate(getCategoryPredicate(id: id),
                                         EntityType: CCategory.self,
                                         in: getContext())
    }

    func create(categories: [EvCategory],
                completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.createOrUpdateCategories(categories, in: context)
        }, completion: completion)
    }

    func update(categories: [EvCategory], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.createOrUpdateCategories(categories, in: context)
        }, completion: completion)
    }

    func categoryWith(id: String) -> EvCategory? {
        if let ci = getCCategoryWithID(id,
                                    in: getContext()) {
            return convert(category: ci)
        }
        return nil
    }

    func latestUsedCategories(maxItems: Int) -> [EvCategory] {
        if let cats = CCategory.mr_findAllSorted(
            by: "used_date",
            ascending: false,
            in: getContext()) as? [CCategory] {
            let countToDrop = cats.count > maxItems ? cats.count - maxItems : 0
            let limited = cats.dropLast(countToDrop)
            return limited.map { convert(category: $0) }
        }
        return []
    }

    func delete(categories: [CategoryDescProtocol],
                completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            for i in categories {
                self.deleteCategoryWithID(i.cat_id, in: context)
            }
        }, completion: completion)
    }

    func deleteAllCategories(completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let cmnts = CCategory.mr_findAll(in: context) as? [CCategory] {
                    for c in cmnts {
                        self.deleteCCategory(c, in: context)
                    }
                }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    internal func getCategoryPredicate(id: String) -> NSPredicate {
        let idPred = getStrPredicate(key: "cat_id", value: id)
        return NSCompoundPredicate(andPredicateWithSubpredicates: [idPred])
    }

    internal func getCCategoryWithID(_ id: String, in context: NSManagedObjectContext) -> CCategory? {
        return CCategory.mr_findFirst(
            with: getCategoryPredicate(id: id),
            in: context)
    }

    internal func getCCategoryWithID(_ id: String, serial: String) -> CCategory? {
        return getCCategoryWithID(id, in: getContext())
    }

    @discardableResult
    internal func createCCategory(_ c: EvCategory,
                                  in context: NSManagedObjectContext) -> CCategory {
        let cc = CCategory.mr_createEntity(in: context)!
        updateCategoryContents(cc, from: c)
        return cc
    }

    internal func createOrUpdateCategories(_ categories: [EvCategory],
                                           in context: NSManagedObjectContext) {
        for m in categories {
            let ci: CCategory
            if let _ci = getCCategoryWithID(m.id, in: context) {
                ci = _ci
                updateCategoryContents(ci, from: m)
            } else {
                ci = createCCategory(m, in: context)
            }
        }
    }

    internal func updateCategoryContents(_ cm: CCategory, from m: EvCategory) {
        cm.cat_id = m.id
        cm.name = m.name
        cm.used_date = Date()
    }

    internal func convert(category c: CCategory) -> EvCategory {
        return EvCategory(id: c.cat_id!,
                          name: c.name ?? "",
                          icon: c.icon ?? "")
    }

    internal func deleteCCategory(_ c: CCategory, in context: NSManagedObjectContext) {
        c.mr_deleteEntity(in: context)
    }

    internal func deleteCategoryWithID(_ id: String, in context: NSManagedObjectContext) {
        let pred = getCategoryPredicate(id: id)
        if let cats = CCategory.mr_findAll(with: pred, in: context) as? [CCategory] {
            for c in cats {
                deleteCCategory(c, in: context)
            }
        }
    }
}

// part of CacheFetchRequestsService
extension Cache {
    func fetchDataControllerForCategories() -> FetchManagerProtocol {
        let context = NSManagedObjectContext.mr_default()
        let request = CCategory.mr_requestAllSorted(by: "name",
                                              ascending: false,
                                              in: context)
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = const.szBatch
        let frc = NSFetchedResultsController(fetchRequest: request,
                                             managedObjectContext: context,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil) as! NSFetchedResultsController<CCategory>
        return FetchManager(fetchedResultsController: frc,
                            converter: self)
    }
}
