//
//  FavSyncerService.swift
//  blagru
//
//  Created by Michael Artuerhof on 02.07.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol FavSyncerService {
    func syncFavorites(completion: @escaping ((NSError?) -> Void))
}
