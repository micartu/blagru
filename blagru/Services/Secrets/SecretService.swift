//
//  SecretService.swift
//  blagru
//
//  Created by Michael Artuerhof on 26.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import Foundation

protocol SecretService {
    /// is it a social authorization?
    var isSocialAuth: Bool { get set }
    /// what kind of social network provider do we use?
    var socialAuthType: String { get set }
    /// social network access token
    var socialAuthToken: String { get set }
    /// social auth extra data (for every type could be different)
    var socialAuthExtra: [String:String] { get set }
    /// social data expire date
    var socialAuthExpire: Date? { get set }

    /// means user is through security (like placed his finger on touch id, or entered his/her PIN)
    var authorized: Bool { get }
    /// means user entered his/her credentials
    var isAuthCreated: Bool { get set }
    /// current session id (like a token from oauth2)
    var session: String { get set }
    /// current refresh session id (like a refresh token from oauth2)
    var restore: String { get set }
    /// user's ID
    var userID: String { get set }
    /// user's login
    var ulogin: String { get set }
    /// user's password
    var password: String { get set }
    /// pushes id
    var uid: String { get set }
    /// removes all saved data about user's credentials
    func erase()
}
