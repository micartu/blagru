//
//  BaseViewController.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, Themeble {
    weak var lifeDelegate: ViewLifeCycleDelegate? = nil

    /// hook is being called in didMove(toParent:)
    /// if toParent == nil
    var exiter: (() -> Void)? = nil

    /// hook is being called in apply(theme:)
    var themingHandler: ((Theme) -> Void)? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        checkTraitCollection()
    }

    fileprivate func checkTraitCollection() {
        let tm: ThemeManagerProtocol = ServicesAssembler.inject()
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                if !tm.isDarkTheme {
                    tm.inverseThemeColors()
                }
            } else {
                // User Interface is Light
                if tm.isDarkTheme {
                    tm.inverseThemeColors()
                }
            }
        }
        apply(theme: tm.getCurrentTheme())
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        checkTraitCollection()
    }

    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        if parent == nil {
            exiter?()
            lifeDelegate?.movedBack()
        }
    }

    // MARK: - Helpers

    func viewWithClearBack() -> UIView {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        return header
    }

    // MARK: Themeble

    func apply(theme: Theme) {
        runOnMainThread { [weak self] in
            self?.navigationItem.leftBarButtonItem?.tintColor = theme.tintColor
            self?.theme = theme
            self?.themingHandler?(theme)
        }
    }

    func addDismissKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }

    // MARK: animation stuff
    internal func applyTouchAnimation(_ sender: UIView,
                                      completion: @escaping (() -> Void)) {
        let anim = CABasicAnimation(keyPath: "transform.scale")
        let kD: Double = 0.25
        anim.fromValue = 1
        anim.toValue = 0.82
        anim.duration = kD
        sender.layer.add(anim, forKey: nil)
        delay(kD) {
            completion()
        }
    }

    // MARK: Private/internal
    internal var theme: Theme!
}
