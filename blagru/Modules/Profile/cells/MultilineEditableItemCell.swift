//
//  MultilineEditableItemCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol MultilineEditableItemCellDelegate: class {
    func editEnded(id: String, text: String)
    func updated(id: String, text: String)
}

final class MultilineEditableItemCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var editItem: UITextView!
    @IBOutlet weak var heightC: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        editItem.delegate = self
    }

    func setEditableText(_ text: String) {
        checkLines(text)
        editItem.text = text
    }

    private func checkLines(_ t: String) {
        let newlines = t.split(separator: "\n").count
        let n = newlines > 0 ? newlines : 1
        heightC.constant = CGFloat(n) * const.cy + const.off
    }

    var id = ""
    weak var delegate: MultilineEditableItemCellDelegate? = nil
    private struct const {
        static let cy: CGFloat = 18
        static let off: CGFloat = 5
    }
}

// MARK: - UITextViewDelegate
extension MultilineEditableItemCell: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        delegate?.editEnded(id: id, text: editItem.text ?? "")
    }

    func textViewDidChange(_ textView: UITextView) {
        let t = textView.text ?? ""
        checkLines(t)
        delegate?.updated(id: id, text: t)
    }
}
