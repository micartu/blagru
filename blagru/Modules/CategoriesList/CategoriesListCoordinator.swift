//
//  CategoriesListCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

enum CategorySelected {
    case chosen(category: EvCategory)
}

final class CategoriesListCoordinator: BaseCoordinator {
	override func start() {
		let vc = BaseTableViewController()
		vc.manualBuilding = true
		vc.viewBuilder = {
			let tv = UITableView(frame: .zero, style: .grouped)
			return (tv, tv, true)
		}
		vc.title = "Categories Title".localized
        vc.addRefreshControl = false
		vc.initializer = { tv in
            let cells = [
                CategoryCell.self,
            ]
            for c in cells {
                let identifier = String(describing: c)
                tv.register(UINib(nibName: identifier, bundle: nil),
                            forCellReuseIdentifier: identifier)
            }
            tv.separatorStyle = .none
            tv.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
		}
		let viewModel = CategoriesListViewModel(
            fetchCache: ServicesAssembler.inject(),
            cache: ServicesAssembler.inject()
        )
		viewModel.dispatcher = self
		viewModel.source = viewModel
		vc.viewModel = viewModel
		cntr = vc
	}
}

// MARK: - CategoriesListDispatcherProtocol
extension CategoriesListCoordinator: CategoriesListDispatcherProtocol {
    func chosen(category: EvCategory) {
        exitWith(result: CategorySelected.chosen(category: category))
    }
}
