//
//  NetFun+Fav.swift
//  blagru
//
//  Created by Michael Artuerhof on 02.07.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetFavService {
    func fetchFavorites(completion: @escaping (([Ev]?, NSError?) -> Void)) {
        post(url: "/events/get_favorite/", data: Data(), success: { (ans: NSDictionary) in
            if let evs = ans["events"] as? [NSDictionary] {
                let ev = evs.map { parseEvent(from: $0) }
                completion(ev, nil)
            } else {
                let e = NSError(domain: Const.domain,
                                code: -1,
                                userInfo: [NSLocalizedDescriptionKey: "no 'events' field in the answer".localized])
                completion(nil, e)
            }
        }, failure: { e in
            completion(nil, e)
        })
    }

    func addToFavorites(eventID: String, completion: @escaping ((NSError?) -> Void)) {
        post(url: "/events/add_favorite/", data: Data(), success: { (ans: NSDictionary) in
            completion(nil)
        }, failure: { e in
            completion(e)
        })
    }

    func removeFromFavorites(eventID: String, completion: @escaping ((NSError?) -> Void)) {
        post(url: "/events/remove_favorite/", data: Data(), success: { (ans: NSDictionary) in
            completion(nil)
        }, failure: { e in
            completion(e)
        })
    }
}
