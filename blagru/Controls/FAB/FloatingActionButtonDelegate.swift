//
//  FloatingActionButtonDelegate.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

/// Floating action button delegate protocol
///
@objc protocol FloatingActionButtonDelegate: class {
    /// Is called before opening animation. Button state is .opening.
    ///
    @objc optional func floatingActionButtonWillOpen(_ button: FloatingActionButton)

    /// Is called after opening animation. Button state is .opened.
    ///
    @objc optional func floatingActionButtonDidOpen(_ button: FloatingActionButton)

    /// Is called before closing animation. Button state is .closing.
    ///
    @objc optional func floatingActionButtonWillClose(_ button: FloatingActionButton)

    /// Is called after closing animation. Button state is .closed.
    ///
    @objc optional func floatingActionButtonDidClose(_ button: FloatingActionButton)
}
