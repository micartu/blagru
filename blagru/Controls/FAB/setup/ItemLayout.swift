//
//  ItemLayout.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

/// Item layout
///
struct ItemLayout {
    /// A closure that defines the layout of given action items relative to an action button.
    ///
    var layout: (_ items: [ActionItem], _ actionButton: FloatingActionButton) -> Void

    /// Initializes and returns a newly allocated item layout object with given layout closure.
    ///
    /// - Parameter layout: A closure that defines the the layout of given action items relative to an action button.
    ///
    /// - Returns: An initialized item layout object.
    ///
    init(layout: @escaping (_ items: [ActionItem], _ actionButton: FloatingActionButton) -> Void) {
        self.layout = layout
    }

    /// Returns an item layout object that places the items in a vertical line with given inter item spacing.
    ///
    /// - Parameter interItemSpacing: The distance between two adjacent items.
    /// - Parameter firstItemSpacing: The distance between the action button and the first action item.
    ///                               When `firstItemSpacing` is 0 or less `interItemSpacing` is used instead.
    ///                               Default is 0.
    ///
    /// - Returns: An item layout object.
    ///
    static func verticalLine(withInterItemSpacing interItemSpacing: CGFloat = 12,
                             firstItemSpacing: CGFloat = 0) -> ItemLayout {
        return ItemLayout { items, actionButton in
            var previousItem: ActionItem?
            for item in items {
                let previousView = previousItem ?? actionButton
                let isFirstItem = (previousItem == nil)
                let spacing = selectSpacing(forFirstItem: isFirstItem, defaultSpacing: interItemSpacing, firstItemSpacing: firstItemSpacing)
                item.bottomAnchor.constraint(equalTo: previousView.topAnchor, constant: -spacing).isActive = true
                item.circleView.centerXAnchor.constraint(equalTo: actionButton.centerXAnchor).isActive = true
                previousItem = item
            }
        }
    }

    /// Returns an item layout object that places the items in a circle around the action button with given radius.
    ///
    /// - Parameter radius: The distance between the center of an item and the center of the button itself.
    ///
    /// - Returns: An item layout object.
    ///
    static func circular(withRadius radius: CGFloat = 100) -> ItemLayout {
        return ItemLayout { items, actionButton in
            let numberOfItems = items.count
            var index: Int = 0
            for item in items {
                let angle = ItemAnimationConfiguration.angleForItem(at: index, numberOfItems: numberOfItems, actionButton: actionButton)
                let horizontalDistance = radius * cos(angle)
                let verticalDistance = radius * sin(angle)

                item.circleView.centerXAnchor.constraint(equalTo: actionButton.centerXAnchor, constant: horizontalDistance).isActive = true
                item.circleView.centerYAnchor.constraint(equalTo: actionButton.centerYAnchor, constant: verticalDistance).isActive = true

                index += 1
            }
        }
    }
}

fileprivate extension ItemLayout {
    static func selectSpacing(forFirstItem isFirstItem: Bool, defaultSpacing: CGFloat, firstItemSpacing: CGFloat) -> CGFloat {
        if isFirstItem && firstItemSpacing > 0 {
            return firstItemSpacing
        }
        return defaultSpacing
    }
}
