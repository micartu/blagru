//
//  NetPointsService.swift
//  blagru
//
//  Created by Michael Artuerhof on 11.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol NetPointsService {
    func pointsIn(catId: String,
                  lat0: Double,
                  lon0: Double,
                  lat1: Double,
                  lon1: Double,
                  completion: @escaping (([Ev]?, NSError?) -> Void))
}
