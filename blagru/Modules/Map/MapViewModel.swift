//
//  MapViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 03/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import GoogleMaps
import Repeat

protocol MapDispatcherProtocol: class, Authorizable {
    func showInfoFor(eventId: String)
    func showProfileFor(userID: String)
    func showFavorites()
    func showCategoriesSelector()
}

protocol MapViewModelProtocol:
    ViewReadinessProtocol,
    ViewAppearanceProtocol {
    func addEventAction()
    func searchEventAction()
    func curPositionAction()
    func set(pointer: MapPointerService)
}

enum MapOpMode: Int, Codable {
    case idle
    case add
    case search
}

protocol NetMapService:
    NetPointsService,
    NetEventService,
    NetFavService { }

final class MapViewModel: NSObject {
	weak var dispatcher: (AlertProtocol & MapDispatcherProtocol)?
	weak var view: MapViewProtocol?

    internal var posTimer: Repeater?
    internal let storage: KeyValueStorageService
    internal var locator: LocatorService
    internal var pointer: MapPointerService?
    internal let net: NetMapService
    internal let secret: SecretService

    // map variables
    internal var moveToLocatorOnce = false
    internal var zoom: Float = 14
    internal var camera: GMSCameraPosition?

    // state
    internal var mode: MapOpMode = .idle {
        didSet {
            storage.mapOpMode.value = mode
            switchModeIfNeeded(networkCall: false)
        }
    }
    internal var selecting: PointType?
    /// point of interest 0
    internal var pt0Ind: Int64?
    /// point of interest 1
    internal var pt1Ind: Int64?
    /// selected point 0
    internal var spt0Ind: Int64?
    /// selected point 1
    internal var spt1Ind: Int64?
    /// do we show panel with A, B buttons and options?
    internal var selectorShown = false
    internal var ptIndices = [Int64]()
    /// event id
    internal var evId: String?

    internal var usrEvent: Ev? {
        didSet {
            storage.userEvent.value = usrEvent
            checkOpMode()
        }
    }

    /// cache for restoring points
    internal let cache: CacheEvAndPointService

    /// cache for categories for user
    internal let cats: CacheCategoryService

    // categories
    internal var categories = [EvCategory]()
    internal var category: EvCategory? {
        didSet {
            storage.currentCategory.value = category
        }
    }
    internal let syncer: GeneralSynchronizer

	init(storage: KeyValueStorageService,
         net: NetMapService,
         secret: SecretService,
         cache: CacheEvAndPointService,
         cats: CacheCategoryService,
         locator: LocatorService) {
        self.locator = locator
        self.net = net
        self.cache = cache
        self.cats = cats
        self.secret = secret
        self.storage = storage
        syncer = GeneralSynchronizer(statusItems: [],
                                     statusDict: StatusDictType(), // an empty status dictionary
                                     deb: ConsoleDebugger(name: "cats")) { (items, dict) in
                                        // DISCLAIMER:
                                        // DO NOTHING, WE DO NOT NEED TO DETERMINE DIFFERENCIES
                                        // WITH THE SERVER SIDE WE INTEND
                                        // ONLY TO UPDATE OUR OWN DATABASE!
        }
        super.init()
        self.locator.delegate = self
        category = storage.currentCategory.value
	}

    internal struct const {
        static let defTimeout: TimeInterval = 1
        static let msgDelay: TimeInterval = 5
        static let maxCategories = 2
        static let maxCategoriesToLoad = 100
    }
}

// MARK: - MapViewModelProtocol
extension MapViewModel: MapViewModelProtocol {
    func viewIsReady() {
        categories = cats.latestUsedCategories(maxItems: const.maxCategoriesToLoad)
        formFABItems()
        checkCategoriesOverNetwork()
        view?.makeChooserPanel(hidden: true, animated: false)
        view?.makeInfoPanel(hidden: true, animated: false)
        restorePoints()
        moveToCurrentPosition()
    }

    func viewWillAppear() {
        checkOpMode()
        checkLocator()
    }

    func set(pointer: MapPointerService) {
        self.pointer = pointer
    }

    func addEventAction() {
        removeSelection()
        mode = .add
    }

    func searchEventAction() {
        removeSelection()
        mode = .search
        removeAllIrrelevantPoints { [weak self] in
            self?.queryServerForPoints()
        }
    }

    func curPositionAction() {
        func moveToLastSaved() {
            moveToCurrentPosition()
        }
        if storage.useLocator.value == nil {
            dispatcher?.showYesNO(
                title: "Can you share your position?".localized,
                message: "if yes, you would be able to add point where you are".localized,
                actionYes: { [weak self] _ in
                    self?.storage.useLocator.value = true
                    self?.checkLocator()
                    moveToLastSaved()
                },
                actionNo: { [weak self] _ in
                    self?.storage.useLocator.value = false
                    moveToLastSaved()
                })
        } else {
            moveToLastSaved()
        }
    }

    func setActive(category: EvCategory) {
        let icon = imageFor(category: category)
        view?.setActiveFAB(icon: icon)
        self.category = category
        switchModeIfNeeded()
    }

    func setPointFromFavorites(ev: Ev) {
        pointer?.removeArcPolyline()
        pointer?.removeAllPoints { [weak self] in
            let a = MapPointDescr(
                type: .A,
                coord: CLLocationCoordinate2D(latitude: ev.lat0, longitude: ev.lon0),
                state: .hightlighted)
            let b = MapPointDescr(
                type: .B,
                coord: CLLocationCoordinate2D(latitude: ev.lat1, longitude: ev.lon1),
                state: .hightlighted)
            self?.pointer?.add(points: [a, b]) { indicies in
                let at = MapAttachDescr(
                    ind0: indicies[0],
                    ind1: indicies[1],
                    ev: ev
                )
                self?.pointer?.attachInBatch(pts: [at]) {
                    self?.pt0Ind = indicies[0]
                    self?.pt1Ind = indicies[1]
                    self?.switchModeIfNeeded()
                }
            }
        }
    }
}

// MARK: - Actions
extension MapViewModel {
    internal func selectAButton() {
        view?.blink(button: .source)
        view?.showMsg(
            "Please select source point".localized,
            timeout: const.msgDelay
        )
        selecting = .A
    }

    internal func selectBButton() {
        view?.blink(button: .dest)
        view?.showMsg(
            "Please select destination point".localized,
            timeout: const.msgDelay
        )
        selecting = .B
    }

    internal func moveToCurrentPosition() {
        if locator.lat == 0 && locator.lon == 0 {
            // do not move into equator because of the wrong data
            moveToLocatorOnce = true
            let (svPos, svZoom) = extractSavedMapPos()
            if let p = svPos {
                view?.moveMapTo(point: p, zoom: svZoom)
            }
            return
        }
        view?.moveMapTo(
            point: CLLocationCoordinate2DMake(locator.lat, locator.lon),
            zoom: zoom
        )
    }

    internal func formFABItems() {
        let profile = ActionInit(image: UIImage(named: "user")!) { [weak self] in
            self?.dispatcher?.askAuthorizationAndExecute {
                guard let `self` = self else { return }
                self.dispatcher?.showProfileFor(userID: self.secret.userID)
            }
        }
        var a = [ActionInit]()
        a.append(profile)
        for (i, c) in categories.enumerated() {
            let icon = imageFor(category: c)
            let cat = ActionInit(image: icon) { [weak self] in
                self?.setActive(category: c)
            }
            a.append(cat)
            if i >= const.maxCategories { break }
        }
        let cats = ActionInit(image: UIImage(named: "additional")!) { [weak self] in
            self?.dispatcher?.showCategoriesSelector()
        }
        a.append(cats)
        view?.setFAB(actions: a.reversed())
        if let curCategory = storage.currentCategory.value {
            view?.setActiveFAB(icon: imageFor(category: curCategory))
        }
    }

    internal func isSelectedPointInFavorites() -> Bool {
        return selectedPointInFavoritesID() == nil ? false : true
    }

    internal func selectedPointInFavoritesID() -> String? {
        if let ind0 = pt0Ind,
            let ind1 = pt1Ind,
            let pt0 = pointer?.coordinatesOf(ind: ind0),
            let pt1 = pointer?.coordinatesOf(ind: ind1) {
            let favs = cache.userFavorites()
            let kDiff: Double = 0.00001
            for f in favs {
                if ((abs(f.lat0 - pt0.latitude) < kDiff &&
                    abs(f.lon0 - pt0.longitude) < kDiff) &&
                    (abs(f.lat1 - pt1.latitude) < kDiff &&
                    abs(f.lon1 - pt1.longitude) < kDiff)) ||
                   ((abs(f.lat1 - pt0.latitude) < kDiff &&
                    abs(f.lon1 - pt0.longitude) < kDiff) &&
                    (abs(f.lat0 - pt1.latitude) < kDiff &&
                    abs(f.lon0 - pt1.longitude) < kDiff)) {
                    return f.id
                }
            }
        }
        return nil
    }

    internal func removeAllIrrelevantPoints(completion: @escaping (() -> Void)) {
        pointer?.remove(indicies: ptIndices) { [weak self] in
            self?.ptIndices.removeAll()
            completion()
        }
    }

    private func restorePoints() {
        pointer?.restorePoints()
        let pts = cache.allPoints()
        for p in pts {
            // start or end point
            if p.state == .pt {
                if p.typeA {
                    spt0Ind = p.ind
                } else {
                    spt1Ind = p.ind
                }
            }
            if p.state == .hightlighted {
                if p.typeA {
                    pt0Ind = p.ind
                } else {
                    pt1Ind = p.ind
                }
            }
            if p.state != .hightlighted {
                ptIndices.append(p.ind)
            }
        }
        if let m = storage.mapOpMode.value {
            mode = m
        }
        if let i0 = pt0Ind,
            let i1 = pt1Ind {
            pointer?.connect(ind0: i0, ind1: i1)
        }
    }

    internal func removeSelection() {
        // reset selected point
        spt0Ind = nil
        spt1Ind = nil
    }

    internal func removeSelectedPt0() {
        if let ind = pt0Ind {
            pointer?.remove(indicies: [ind]) {}
            pt0Ind = nil
        }
    }

    internal func removeSelectedPt1() {
        if let ind = pt1Ind {
            pointer?.remove(indicies: [ind]) {}
            pt1Ind = nil
        }
    }

    fileprivate func checkLocator() {
        if let use = storage.useLocator.value {
            if use {
                view?.enableUserPositionOnMap()
            }
            locator.setActiveMode(use)
        }
    }
}

// MARK: - state change/common methods
extension MapViewModel {
    internal func resetToIdleMode() {
        view?.makeChooserView(shrink: false)
        view?.makeInfoPanel(hidden: true, animated: true)
        removeSelectedPt0()
        removeSelectedPt1()
        evId = nil
        mode = .idle
        resetPoints()
        if let ev = usrEvent {
            net.delete(event: ev) { event, err in
                // TODO: add something here!
            }
        }
        usrEvent = nil
        pointSelector(show: false)
        switchModeIfNeeded(networkCall: false)
    }

    internal func queryServerForPoints() {
        if let ind0 = pt0Ind,
            let ind1 = pt1Ind,
            let pt0 = pointer?.coordinatesOf(ind: ind0),
            let pt1 = pointer?.coordinatesOf(ind: ind1) {
            guard let cid = category?.id else {
                dispatcher?.show(
                    error: "Network failure".localized + ":"
                    + "Cannot find categories".localized)
                return
            }
            net.pointsIn(
                catId: cid,
                lat0: pt0.latitude,
                lon0: pt0.longitude,
                lat1: pt1.latitude,
                lon1: pt1.longitude) { [weak self] (evs, error) in
                    if let events = evs {
                        var des = [MapPointDescr]()
                        for ev in events {
                            let a = MapPointDescr(
                                type: .A,
                                coord: CLLocationCoordinate2D(latitude: ev.lat0, longitude: ev.lon0),
                                state: .idle)
                            des.append(a)
                            let b = MapPointDescr(
                                type: .B,
                                coord: CLLocationCoordinate2D(latitude: ev.lat1, longitude: ev.lon1),
                                state: .idle)
                            des.append(b)
                        }
                        self?.pointer?.add(points: des) { indicies in
                            self?.cache.update(events: events) {
                                assert(indicies.count == events.count * 2)
                                self?.ptIndices = indicies
                                var b = [MapAttachDescr]()
                                var k = 0
                                for i in 0..<events.count {
                                    let e = MapAttachDescr(
                                        ind0: indicies[k],
                                        ind1: indicies[k + 1],
                                        ev: events[i]
                                    )
                                    b.append(e)
                                    k += 2
                                }
                                self?.pointer?.attachInBatch(pts: b) {}
                            }
                        }
                    } else if let e = error {
                        self?.dispatcher?.show(error: e.localizedDescription)
                    }
            }
        }
    }

    internal func switchModeIfNeeded(networkCall: Bool = true) {
        // check if selection window is shown if apropriate
        if !selectorShown && mode != .idle && pt1Ind == nil {
            pointSelector(show: true)
        }
        let g = DispatchGroup()
        // blinkButtonsAndCheckMode
        if pt0Ind == nil {
            selectAButton()
        } else if pt1Ind == nil {
            selectBButton()
        } else {
            view?.blink(button: .stop)
            selecting = nil
            // hide any messages
            view?.makeStackView(hidden: false, animated: true)
            if let p0 = spt0Ind, let p1 = spt1Ind {
                changeSelectedPointStateTo(ind0: p0, ind1: p1)
            } else if let p0 = pt0Ind, let p1 = pt1Ind {
                changeSelectedPointStateTo(ind0: p0, ind1: p1)
            }
            if networkCall {
                g.enter()
                removeAllIrrelevantPoints { [weak self] in
                    if self?.mode == MapOpMode.search {
                        self?.queryServerForPoints()
                    }
                    g.leave()
                }
            }
        }
        g.notify(queue: .main) { [weak self] in
            self?.checkOpMode()
        }
    }

    internal func checkOpMode() {
        let adding: Bool?
        switch mode {
        case .add:
            adding = true
            view?.makeChooserView(shrink: false)
            var toShow: [OptionalButtonType] = [.savedList]
            var toHide: [OptionalButtonType] = []
            if pt0Ind != nil && pt1Ind != nil { // two points are selected
                view?.make(
                    button: .favorites,
                    active: isSelectedPointInFavorites()
                )
                toShow.append(.favorites)
                if storage.userEvent.value == nil { // user haven't created an event yet
                    toShow.append(.done)
                } else { // the event have been already created! No need to commit it to server:
                    toHide.append(.done)
                }
            } else {
                toHide.append(contentsOf: [.favorites, .done])
            }
            view?.makeButtons(hide: toHide, appear: toShow)
            removeAllIrrelevantPoints { [weak self] in
                self?.view?.makeInfoPanel(hidden: true, animated: true)
            }
        case .search:
            var toShow: [OptionalButtonType] = []
            var toHide: [OptionalButtonType] = [.done]
            if pt0Ind != nil && pt1Ind != nil { // two points are selected
                view?.make(
                    button: .favorites,
                    active: isSelectedPointInFavorites()
                )
                toShow.append(contentsOf: [.savedList, .favorites])
            } else {
                toShow.append(.savedList)
                toHide.append(.favorites)
            }
            view?.makeButtons(hide: toHide, appear: toShow)
            adding = false
        case .idle:
            adding = nil
            evId = nil
            view?.set(button: .createEvent, active: false)
            view?.set(button: .searchEvent, active: false)
            view?.makeChooserPanel(hidden: true, animated: true)
            view?.makeInfoPanel(hidden: true, animated: true)
        }
        if let a = adding {
            view?.makeChooserPanel(hidden: false, animated: true)
            view?.set(button: .createEvent, active: a)
            view?.set(button: .searchEvent, active: !a)
        }
    }

    fileprivate func imageFor(category: EvCategory) -> UIImage {
        if let img = UIImage(named: category.name) {
            return img
        }
        return UIImage(named: "unknown_category")!
    }
}

// MARK: - LocatorDelegate
extension MapViewModel: LocatorDelegate  {
    func location(enabled: Bool) {
        view?.userPosition(enabled: enabled)
    }

    func location(lat: Double, lon: Double) {
        if moveToLocatorOnce {
            let loc = CLLocationCoordinate2D(latitude: lat,
                                             longitude: lon)
            view?.moveMapTo(point: loc, zoom: zoom)
            if moveToLocatorOnce {
                moveToLocatorOnce = false
            }
        }
    }
}

// MARK: - K-V-Contrainer definitions for needed fields
fileprivate extension KeyValueStorageService {
    var currentCategory: KeyValueContainer<EvCategory> {
        makeContainer()
    }

    var userEvent: KeyValueContainer<Ev> {
        makeContainer()
    }

    var mapOpMode: KeyValueContainer<MapOpMode> {
        makeContainer()
    }

    var useLocator: KeyValueContainer<Bool> {
        makeContainer()
    }
}
