//
//  ExtendableTableViewCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 10/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

open class ExtendableTableViewCell: UITableViewCell {
    @IBOutlet weak var viewContents: UIView!

    var theme: Theme!

    // what kind of cell we represent
    var id = ""

    // set them if you want to change offsets of added labels into viewContents
    var topOffsetInitial: CGFloat? = nil
    var topOffset: CGFloat? = nil
    var imgTopOffset: CGFloat = const.imgTopOffset

    // x-offset for labels in viewContents area
    var labelXOffset = const.kLabelOffset

    public func initialize() {
        // remove old contents if any
        for v in viewContents.subviews {
            v.removeFromSuperview()
        }
    }

    func addOnly(title: String, font: UIFont) {
        let l = addLabel()
        l.font = font
        l.text = title
    }

    func add(title: String, font: UIFont, description: String, dfont: UIFont? = nil) {
        let (l, p) = addTwoLabels(image: "", width: 0, height: 0)
        l.font = font
        l.text = title
        p.text = description
        if let df = dfont {
            p.font = df
        } else {
            p.font = font
        }
    }

    func add(attTitle: NSAttributedString, description: String, dfont: UIFont) {
        let (l, p) = addTwoLabels(image: "", width: 0, height: 0)
        l.attributedText = attTitle
        p.text = description
        p.font = dfont
    }

    func add(title: String, font: UIFont, quantity: NSAttributedString) {
        let (l, p) = addTwoLabels(image: "", width: 0, height: 0)
        l.font = font
        l.text = title
        p.attributedText = quantity
    }

    func add(attTitle: NSAttributedString, attDescr: NSAttributedString) {
        let (l, p) = addTwoLabels(image: "", width: 0, height: 0)
        l.attributedText = attTitle
        p.attributedText = attDescr
    }

    public func finishSetup() {
        if let lc = lastConnected {
            viewContents
                .addConstraint(
                    lc.bottomAnchor.constraint(equalTo: viewContents.bottomAnchor,
                                               constant: -const.kOffset))
        }
        lastConnected = nil
    }

    // MARK: - Internal

    internal func addTwoLabels(image: String,
                               width: CGFloat,
                               height: CGFloat,
                               imgXoffset: CGFloat = 0) -> (UILabel, UILabel) {
        let first = addToView(image: image, width: width, height: height, imgXoffset: imgXoffset)
        assert(lastConnected != nil)
        let v = lastConnected!
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(l)
        v.addConstraints([l.topAnchor.constraint(equalTo: v.topAnchor),
                          l.trailingAnchor.constraint(equalTo: v.trailingAnchor, constant: -2)])
        l.sizeToFit()
        return (first, l)
    }

    internal func add(image: String,
                      width: CGFloat,
                      height: CGFloat,
                      text: String) {
        let l = addToView(image: image, width: width, height: height)
        l.font = theme.regularFont.withSize(theme.sz.middle2)
        l.textColor = theme.inactiveColor
        l.text = text
    }

    internal func addLabel(topOffset: CGFloat = 0,
                           trailing: CGFloat = -const.kTrailingOffset) -> UILabel {
        return addToView(image: "",
                         width: 0,
                         height: 0,
                         imgXoffset: 0,
                         labelTrailing: trailing,
                         labelTopOffset: topOffset)
    }

    func addToView(image: String,
                   width: CGFloat,
                   height: CGFloat,
                   imgXoffset: CGFloat = 0,
                   labelTrailing: CGFloat = -const.kTrailingOffset,
                   labelTopOffset: CGFloat = 0) -> UILabel {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        viewContents.addSubview(v)
        viewContents.addConstraints([v.leadingAnchor.constraint(equalTo: viewContents.leadingAnchor),
                                     v.trailingAnchor.constraint(equalTo: viewContents.trailingAnchor)])
        let uiimage: UIImage
        if image.count > 0 {
            uiimage = UIImage(named: image) ?? UIImage()
        } else {
            uiimage = UIImage()
        }
        let im = UIImageView(image: uiimage)
        im.translatesAutoresizingMaskIntoConstraints = false
        im.contentMode = .scaleAspectFit
        v.addSubview(im)
        v.addConstraints([im.topAnchor.constraint(equalTo: v.topAnchor, constant: imgTopOffset),
                          im.leadingAnchor.constraint(equalTo: v.leadingAnchor, constant: imgXoffset),
                          im.widthAnchor.constraint(equalToConstant: width),
                          im.heightAnchor.constraint(equalToConstant: height)])

        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(l)
        v.addConstraints([l.topAnchor.constraint(equalTo: v.topAnchor, constant: labelTopOffset),
                          l.leadingAnchor.constraint(equalTo: v.leadingAnchor, constant: labelXOffset),
                          l.trailingAnchor.constraint(lessThanOrEqualTo: v.trailingAnchor, constant: labelTrailing),
                          l.bottomAnchor.constraint(equalTo: v.bottomAnchor, constant: -const.kOffset)])
        l.numberOfLines = 0
        l.sizeToFit()

        addTopConstraintTo(view: v)
        count += 1
        lastConnected = v

        return l
    }

    internal func addTopConstraintTo(view v: UIView) {
        // finalize adding address:
        if let lc = lastConnected {
            let offset: CGFloat
            if let o = topOffset {
                offset = o
            } else {
                offset = const.kOffset * 2
            }
            viewContents.addConstraint(v.topAnchor.constraint(equalTo: lc.bottomAnchor, constant: offset))
        } else {
            let offset: CGFloat
            if let o = topOffsetInitial {
                offset = o
            } else {
                offset = const.kOffset
            }
            viewContents.addConstraint(v.topAnchor.constraint(equalTo: viewContents.topAnchor, constant: offset))
        }
    }

    private struct const {
        static let imgTopOffset: CGFloat = 3
        static let kOffset: CGFloat = 2
        static let kTrailingOffset: CGFloat = 70
        static let kLabelOffset: CGFloat = 20
    }

    internal var count = 0
    internal weak var lastConnected: UIView? = nil
}
