//
//  ThemeManager.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

let kThemeChangedNotification = "kThemeChangedNotification"

protocol ThemeManagerProtocol: class {
    func getCurrentTheme() -> Theme
    var isDarkTheme: Bool { get }
    func inverseThemeColors()
}

protocol ThemebleRouting: class {
    func change(theme: Theme)
}

final class ThemeManager: ThemeManagerProtocol {
    init() {
        let sz: CGFloat = 12
        self.theme = Theme(tintColor: #colorLiteral(red: 0.1569, green: 0.451, blue: 0.6471, alpha: 1) /* #2873a5 */,
                           warnColor: #colorLiteral(red: 1, green: 0.1765, blue: 0.3333, alpha: 1) /* #ff2d55 */,
                           inactiveColor: #colorLiteral(red: 0.7804, green: 0.7804, blue: 0.8, alpha: 1) /* #c7c7cc */,
                           mainTextColor: UIColor.black,
                           secondaryTextColor: #colorLiteral(red: 0.5569, green: 0.5569, blue: 0.5765, alpha: 1) /* #8e8e93 */,
                           separatorColor: #colorLiteral(red: 0.8196, green: 0.8196, blue: 0.8392, alpha: 1) /* #d1d1d6 */,
                           emptyBackColor: UIColor.white,
                           secondaryBackColor: #colorLiteral(red: 0.9725, green: 0.9725, blue: 0.9725, alpha: 1) /* #f8f8f8 */,
                           mainBackColor: UIColor.white,
                           borderColor: #colorLiteral(red: 0.9373, green: 0.9373, blue: 0.9569, alpha: 1) /* #efeff4 */,
                           allowedColor: #colorLiteral(red: 0.2314, green: 0.7765, blue: 0.3804, alpha: 1) /* #3bc661 */,
                           prettyColor: #colorLiteral(red: 0.9451, green: 0.8588, blue: 0.9373, alpha: 1) /* #f1dbef */,
                           pointAColor: #colorLiteral(red: 1, green: 0.1765, blue: 0.3333, alpha: 1) /* #ff2d55 */,
                           pointBColor: #colorLiteral(red: 0.2314, green: 0.7765, blue: 0.3804, alpha: 1) /* #3bc661 */,
                           regularFont: UIFont(name: "Roboto-Regular", size: sz)!,
                           boldFont: UIFont(name: "Roboto-Black", size: sz)!,
                           mediumFont: UIFont(name: "Roboto-Medium", size: sz)!,
                           semiboldFont: UIFont(name: "Roboto-Light", size: sz)!,
                           sz: ThemeSizes(header0: 49,
                                          header1: 46,
                                          header2: 34,
                                          header3: 33,
                                          middle0: 19,
                                          middle1: 17,
                                          middle2: 16,
                                          middle3: 15,
                                          middle4: 14,
                                          small0: 13,
                                          small1: 12,
                                          small2: 11,
                                          small3: 10))
    }

    // MARK: ThemeManagerProtocol
    func inverseThemeColors() {
        let t = getCurrentTheme()
        let nt = Theme(
            tintColor: t.tintColor,
            warnColor: t.warnColor,
            inactiveColor: t.inactiveColor.inverse(),
            mainTextColor: t.mainTextColor.inverse(),
            secondaryTextColor: t.secondaryTextColor.inverse(),
            separatorColor: t.separatorColor.inverse(),
            emptyBackColor: t.emptyBackColor,
            secondaryBackColor: t.secondaryBackColor.inverse(),
            mainBackColor: t.mainBackColor.inverse(),
            borderColor: t.borderColor,
            allowedColor: t.allowedColor,
            prettyColor: t.prettyColor,
            pointAColor: t.pointAColor,
            pointBColor: t.pointBColor,
            regularFont: t.regularFont,
            boldFont: t.boldFont,
            mediumFont: t.mediumFont,
            semiboldFont: t.semiboldFont,
            sz: t.sz
        )
        theme = nt
        isDarkTheme = !isDarkTheme
    }

    func getCurrentTheme() -> Theme {
        if let t = theme {
            return t
        }
        fatalError("Theme isn't set in Theme Manager!")
    }

    var isDarkTheme: Bool = false

    // MARK: Private

    private var theme: Theme?
}

extension ThemeManager: ThemebleRouting {
    func change(theme: Theme) {
        self.theme = theme
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kThemeChangedNotification),
                                        object: nil,
                                        userInfo: nil)
    }
}
