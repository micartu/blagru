//
//  AuthorizableBaseCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 28.05.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

enum Access {
    case granted
    case denied
}

class AuthorizableBaseCoordinator: BaseCoordinator {
    private let secret: SecretService

    init(secret: SecretService, parent: BaseCoordinator?) {
        self.secret = secret
        super.init(parent: parent)
    }

    override func broadcast(result: Any?, of coordinator: BaseCoordinator) {
        if let a = result as? Access {
            switch a {
            case .granted:
                if let h = _handler {
                    // remove login screen from stack:
                    if let nav = navigationController as? UINavigationController {
                        // find our view and return exactly to it:
                        var nvcs = [UIViewController]()
                        let vcs = nav.viewControllers
                        for v in vcs {
                            nvcs.append(v)
                            if v == cntr?.vc {
                                break
                            }
                        }
                        nav.setViewControllers(nvcs, animated: true)
                        _handler = nil
                        delay(0.4) {
                            h()
                        }
                    }
                }
            default:
                ()
            }
        } else {
            super.broadcast(result: result, of: coordinator)
        }
    }

    private var _handler: SimpleHandler?
}

extension AuthorizableBaseCoordinator: Authorizable {
    func checkAuthorizationAndExecute(completion: @escaping SimpleHandler) {
        if secret.isAuthCreated {
            completion()
        } else {
            // save handler for using it after authorization will come up
            _handler = completion
            // and then check credentials in login screen:
            let login = LoginCoordinator(parent: self)
            coordinate(to: login)
        }
    }

    func askAuthorizationAndExecute(completion: @escaping SimpleHandler) {
        if secret.isAuthCreated {
            completion()
        } else {
            showYesNO(
                title: "Authorization needed".localized,
                message: "In order to make that operation you need to be authorized, authorize?".localized,
                actionYes: { [weak self] _ in
                    // save handler for using it after authorization will come up
                    self?._handler = completion
                    // and then check credentials in login screen:
                    let login = LoginCoordinator(parent: self)
                    self?.coordinate(to: login)
                },
                actionNo: nil)
        }
    }
}
