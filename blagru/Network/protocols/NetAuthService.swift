//
//  NetAuthService.swift
//  blagru
//
//  Created by Michael Artuerhof on 08.07.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol NetAuthService {
    func socialAuth(type: String,
                    token: String,
                    success: ((NSDictionary) -> Void)?,
                    failure: ((NSError) -> Void)?)
    func auth(login: String,
              password: String,
              success: ((NSDictionary) -> Void)?,
              failure: ((NSError) -> Void)?)
    func register(login: String,
                  name: String,
                  email: String,
                  password: String,
                  success: ((NSDictionary) -> Void)?,
                  failure: ((NSError) -> Void)?)
    func logout(success: (() -> Void)?,
                failure: ((NSError) -> Void)?)
}
