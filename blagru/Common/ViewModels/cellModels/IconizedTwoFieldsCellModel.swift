//
//  IconizedTwoFieldsCellModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 26.09.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol IconizedTwoFieldsCellModel: CommonCell { }

struct IconizedTwoFieldsModel: IconizedTwoFieldsCellModel {
    let id: String
    let theme: Theme
    let icon: String
    let title: String
    let subtitle: String
    let delegate: IconizedTwoFieldsCellDelegate?
    let imgLoader: ImageLoaderProtocol
    let customizer: ((IconizedTwoFieldsCell, Theme) -> Void)?

    func setup(cell: IconizedTwoFieldsCell) {
        cell.id = id
        cell.delegate = delegate
        customizer?(cell, theme)
        cell.backgroundColor = theme.emptyBackColor

        cell.lblTitle.font = theme.regularFont.withSize(theme.sz.middle3)
        cell.lblTitle.textColor = theme.mainTextColor
        cell.lblTitle.text = title

        cell.lblSubtitle.font = theme.regularFont.withSize(theme.sz.middle3)
        cell.lblSubtitle.textColor = theme.secondaryTextColor
        cell.lblSubtitle.text = subtitle

        imgLoader.load(image: icon) { [weak cell] im in
            cell?.iconView.image = im
        }
    }
}
