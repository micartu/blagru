//
//  NetFun.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

final class NetFun {
    internal let netmanager: NetManagerService
    internal let named: String

    init(netmanager: NetManagerService, named: String) {
        self.named = named
        self.netmanager = netmanager
    }

    // MARK: - Internal functions

    /// function get
    /// sends GET message without parameters
    /// T could be Array of JSONs or just a NSDictionary out of a single JSON
    internal func get<T>(url: String,
                         success: @escaping ((T) -> Void),
                         failure: @escaping ((NSError) -> Void)) {
        getWithParams(url: url, params: nil,
                      success: success, failure: failure)
    }

    internal func getWithParams<T>(url: String,
                                   params: [String: Any]?,
                                   success: @escaping ((T) -> Void),
                                   failure: @escaping ((NSError) -> Void)) {
        netmanager.used(by: named)
        netmanager.getQuery(url: url,
                            params: params,
                            success: success,
                            failure: failure)
    }

    /// function get
    /// sends GET message with parameters
    /// T could be Array of JSONs or just a NSDictionary out of a single JSON
    internal func getCusomizedWithParams<T>(url: String,
                                            params: [String: Any]?,
                                            success: @escaping ((T) -> Void),
                                            failure: @escaping ((NSError) -> Void)) {
        netmanager.used(by: named)
        netmanager.getCustomURLQuery(url: url,
                                     params: params,
                                     success: success,
                                     failure: failure)
    }

    /// function post
    /// sends usual POST message with data as input
    /// T could be Array of JSONs or just a NSDictionary out of a single JSON
    internal func post<T>(url: String,
                          data: Data,
                          success: ((T) -> Void)?,
                          failure: ((NSError) -> Void)?) {
        netmanager.used(by: named)
        netmanager.postQuery(url: url,
                             data: data,
                             success: success,
                             failure: failure)
    }

    /// function post
    /// sends POST message in form-data format
    internal func xpost<T>(url: String,
                           parameters: [String: Any],
                           success: ((T) -> Void)?,
                           failure: ((NSError) -> Void)?) {
        netmanager.used(by: named)
        netmanager.xpostQuery(url: url,
                              parameters: parameters,
                              success: success,
                              failure: failure)
    }

    // sends GET query in form-data format
    internal func xget<T>(url: String,
                          success: ((T) -> Void)?,
                          failure: ((NSError) -> Void)?) {
        netmanager.used(by: named)
        netmanager.xgetQuery(url: url,
                             success: success,
                             failure: failure)
    }

    deinit {
        removeAllOperations()
    }
}
