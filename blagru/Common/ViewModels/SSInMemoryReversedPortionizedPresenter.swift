//
//  SSInMemoryReversedPortionizedPresenter.swift
//  blagru
//
//  Created by Michael Artuerhof on 16/09/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

open class SSInMemoryReversedPortionizedPresenter: SSInMemoryPortionizedPresenter {
    // MARK: SSInMemoryPortionizedInteractorOutput
    override func fetched(models: [CellAnyModel], offset: Int, size: Int) {
        runOnMainThread { [weak self] in
            guard let `self` = self else { return }
            self.busy.accept(false)
            let toLoad = offset
            if self.loaded < toLoad && models.count > 0 {
                self.loaded = toLoad
                // the block of models should precede the current ones:
                self.models.insert(contentsOf: models, at: 0)
                self.updateData.accept(true)
            }
        }
    }

    // MARK: CommonViewModel
    override func model(for indexPath: IndexPath) -> Any {
        // TODO: test me!!!
        if models.count > 0 &&
            indexPath.row <= models.count / 2 &&
            needToFetchNext {
            let toLoad = indexPath.row / const.sz + 2
            needToFetchNext = false
            if loaded < toLoad {
                busy.accept(true)
                source?.loadModels(with: theme,
                                   offset: toLoad,
                                   size: const.sz)
            }
        }
        if models.count > 0 &&
            indexPath.row > models.count / 3 &&
            indexPath.row <= models.count / 2 &&
            !needToFetchNext {
            needToFetchNext = true
        }
        return models[indexPath.row]
    }

    private var needToFetchNext = false
}
