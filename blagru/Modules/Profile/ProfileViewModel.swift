//
//  ProfileViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProfileDispatcherProtocol: class {
    func askForPicture(completion: @escaping ((UIImage) -> Void))
}

protocol ProfileViewModelProtocol {
}

final class ProfileViewModel: SingleSectionPresenter {
	weak var dispatcher: (AlertProtocol & ProfileDispatcherProtocol)?

    private let userID: String
    private let cache: CacheUserService
    private weak var imgLoader: ImageLoaderService!
    private let net: NetProfileService

    init(userID: String,
         net: NetProfileService,
         imgLoader: ImageLoaderService,
         cache: CacheUserService) {
        self.userID = userID
        self.imgLoader = imgLoader
        self.net = net
        self.cache = cache
	}

    fileprivate struct const {
        static let name = "ed:name"
        static let login = "ed:login"
    }
}

// MARK: - SingleSectionInteractorInput
extension ProfileViewModel: SingleSectionInteractorInput {
    private func userAvatar() -> String {
        return "/media/user_avatars/\(userID).jpg"
    }

	func loadModels(with theme: Theme) {
        if let u = cache.getUserWith(id: userID) {
            let name = u.name.count > 0 ?
                u.name : u.login
            let c = ProfileCaptionModel(
                theme: theme,
                id: "caption",
                title: name,
                descr: "",
                image: userAvatar(),
                imgLoader: imgLoader,
                delegate: self)
            let n = EditableItemModel(
                theme: theme,
                id: const.name,
                focused: false,
                type: .text,
                title: "Name".localized,
                placeholder: "Enter your name".localized,
                text: u.name,
                customizer: nil,
                delegate: self)
            let ll = EditableItemModel(
                theme: theme,
                id: const.login,
                focused: false,
                type: .text,
                title: "Login".localized,
                placeholder: "Enter your login".localized,
                text: u.login,
                customizer: nil,
                delegate: self)
            fetched(models: [c, n, ll])
        } else {
            let c = ProfileCaptionModel(
                theme: theme,
                id: "caption",
                title: userID,
                descr: "NOT FOUND!",
                image: "user",
                imgLoader: imgLoader,
                delegate: self)
            fetched(models: [c])
        }
	}
}

// MARK: - ProfileViewModelProtocol
extension  ProfileViewModel: ProfileViewModelProtocol {
}

// MARK: - ProfileCaptionCellDelegate
extension  ProfileViewModel: ProfileCaptionCellDelegate {
    func captionAvatarTouched() {
        dispatcher?.askForPicture { [weak self] image in
            self?.net.uploadProfile(avatar: image, success: {
                self?.imgLoader.invalidateCached(image: self?.userAvatar() ?? "") {
                    self?.refreshContents()
                }
            }, failure: { e in
                self?.dispatcher?.show(error: e.localizedDescription)
            })
        }
    }
}

// MARK: - EditableItemCellDelegate
extension  ProfileViewModel: EditableItemCellDelegate {
    fileprivate func change(id: String, text: String) {
        if let ou = cache.getUserWith(id: userID) {
            let u: User
            var d: NSDictionary?
            switch id {
            case const.name:
                u = User(
                    id: userID,
                    login: ou.login,
                    name: text,
                    email: ou.email,
                    avatar: ou.avatar,
                    password: ou.password)
                if ou.name != text {
                    d = ["name": text]
                }
            case const.login:
                u = User(
                    id: userID,
                    login: text,
                    name: ou.name,
                    email: ou.email,
                    avatar: ou.avatar,
                    password: ou.password)
                if ou.login != text {
                    d = ["login": text]
                }
            default:
                return
            }
            cache.update(user: u) { [weak self] in
                if let usr = d {
                    self?.net.save(user: usr, completion: {
                        self?.refreshContents()
                    }, failure: { e in
                        self?.dispatcher?.show(error: e.localizedDescription)
                    })
                }
            }
        }
    }

    func editReturned(id: String, text: String) {
        change(id: id, text: text)
    }

    func editEnded(id: String, text: String) {
        change(id: id, text: text)
    }
}
