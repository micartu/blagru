//
//  MapCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 03/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class MapCoordinator: AuthorizableBaseCoordinator {
	override func start() {
		let vc = MapViewController.create()
        let viewModel = MapViewModel(storage: ServicesAssembler.inject(),
                                     net: ServicesAssembler.inject(),
                                     secret: ServicesAssembler.inject(),
                                     cache: ServicesAssembler.inject(),
                                     cats: ServicesAssembler.inject(),
                                     locator: ServicesAssembler.inject())
		viewModel.dispatcher = self
		vc.vm = viewModel
		viewModel.view = vc
		cntr = vc
        self.viewModel = viewModel
	}

    override func exitFrom(coordinator: BaseCoordinator, with result: Any?) {
        if coordinator is FavoritePointsCoordinator ||
            coordinator is CategoriesListCoordinator {
            navigationController?.pop(animated: true)
            if let r = result as? FavoritePoint {
                switch r {
                case .chosen(let ev):
                    viewModel.setPointFromFavorites(ev: ev)
                }
            } else if let r = result as? CategorySelected {
                switch r {
                case .chosen(let cat):
                    viewModel.setActive(category: cat)
                }
            }
        }
        super.exitFrom(coordinator: coordinator, with: result)
    }

    fileprivate var viewModel: MapViewModel!
}

// MARK: - MapDispatcherProtocol
extension MapCoordinator: MapDispatcherProtocol {
    func showInfoFor(eventId: String) {
        let c = EventInfoCoordinator(eventId: eventId, parent: self)
        coordinate(to: c)
    }

    func showProfileFor(userID: String) {
        let c = ProfileCoordinator(userID: userID, parent: self)
        coordinate(to: c)
    }

    func showFavorites() {
        let c = FavoritePointsCoordinator(parent: self)
        coordinate(to: c)
    }

    func showCategoriesSelector() {
        let c = CategoriesListCoordinator(parent: self)
        coordinate(to: c)
    }
}
