//
//  Ev.swift
//  blagru
//
//  Created by Michael Artuerhof on 11.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

/// an event which consists of two points
struct Ev: Codable {
    let id: String
    let lat0: Double
    let lon0: Double
    let lat1: Double
    let lon1: Double
    let comment: String
    let category: EvCategoryDesc
    let owner: UserDesc
    var participants: [UserDesc]
}

extension Ev: EvDescProtocol {
    var evId: String {
        return id
    }
}
