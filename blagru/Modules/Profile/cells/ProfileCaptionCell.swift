//
//  ProfileCaptionCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol ProfileCaptionCellDelegate: class {
    func captionAvatarTouched()
}

final class ProfileCaptionCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func avatarTouched() {
        delegate?.captionAvatarTouched()
    }

    var id = ""
    weak var delegate: ProfileCaptionCellDelegate?
}
