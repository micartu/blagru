//
//  UIImage+resize.swift
//  blagru
//
//  Created by Michael Artuerhof on 30.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

extension UIImage {
    func imageWith(newSize: CGSize) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let image = renderer.image { _ in
            self.draw(in: CGRect.init(origin: CGPoint.zero, size: newSize))
        }
        return image
    }
}
