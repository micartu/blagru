//
//  MapPointer.swift
//  blagru
//
//  Created by Michael Artuerhof on 09.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

struct MapPoint {
    let ind: Int64
    var type: PointType
    var state: PointState
    var marker: GMSMarker
}

final class MapPointer {
    private var curId: Int64 = 0
    private var points = [Int64:MapPoint]()
    private var events = [Int64:Ev]()
    private var connectedPts = [Int64:MapPoint]()
    private let lock = NSRecursiveLock()
    private let cache: CacheEvAndPointService
    private var arcLine: GMSPolyline?
    private var arcLineIndicies: (Int64, Int64)?
    private var theme: Theme!
    private var top: CGFloat = 20
    private weak var mapView: GMSMapView!

    init(mapView: GMSMapView,
         cache: CacheEvAndPointService) {
        self.mapView = mapView
        self.cache = cache
    }
}

extension MapPointer {
    // MARK: UI part
    func set(theme: Theme) {
        self.theme = theme
    }

    func set(top: CGFloat) {
        self.top = top
    }
}

extension MapPointer: MapPointerService {
    // MARK: MapPointerService
    fileprivate func _add(ind: Int64,
                          point: CLLocationCoordinate2D,
                          type: PointType,
                          state: PointState) {
        let m = GMSMarker(position: point)
        runOnMainThread { [weak self] in
            m.map = self?.mapView
        }
        m.icon = state.image(type: type, themed: theme)
        lock.lock()
        m.userData = ind
        let p = MapPoint(ind: ind, type: type, state: state, marker: m)
        points[ind] = p
        lock.unlock()
    }

    func add(point: CLLocationCoordinate2D,
             type: PointType,
             state: PointState) -> Int64 {
        let ind: Int64
        lock.lock()
        curId += 1
        ind = curId
        lock.unlock()
        _add(ind: ind, point: point, type: type, state: state)
        // add it to the cache:
        let pt = EvPoint(
            ind: ind,
            lon: point.longitude,
            lat: point.latitude,
            typeA: type == .A,
            state: state,
            connectedInd: nil,
            event: nil)
        cache.update(points: [pt]) {}
        return curId
    }

    func add(points: [MapPointDescr], completion: @escaping (([Int64]) -> Void)) {
        var indicies = [Int64]()
        var pts = [EvPoint]()
        for p in points {
            let ind: Int64
            lock.lock()
            curId += 1
            ind = curId
            lock.unlock()
            _add(ind: ind,
                 point: p.coord,
                 type: p.type,
                 state: p.state)
            indicies.append(ind)
            // add it to the cache:
            let pt = EvPoint(
                ind: ind,
                lon: p.coord.longitude,
                lat: p.coord.latitude,
                typeA: p.type == .A,
                state: p.state,
                connectedInd: nil,
                event: nil)
            pts.append(pt)
        }
        cache.update(points: pts) {
            completion(indicies)
        }
    }

    func changePointWith(ind: Int64, state: PointState) {
        runOnMainThread { [weak self] in
            guard let `self` = self else { return }
            if var p = self.points[ind] {
                p.state = state
                p.marker.icon = state.image(type: p.type, themed: self.theme)
                self.points[ind] = p
                // update cache
                if var pt = self.cache.pointWith(ind: ind) {
                    pt.state = state
                    self.cache.update(points: [pt]) {}
                }
            }
        }
    }

    fileprivate func _connect(ind0: Int64, ind1: Int64) {
        lock.lock()
        if let p0 = points[ind0],
            let p1 = points[ind1] {
            connectedPts[ind0] = p1
            connectedPts[ind1] = p0
        }
        lock.unlock()
    }

    func connect(ind0: Int64, ind1: Int64) {
        _connect(ind0: ind0, ind1: ind1)
        // update cache
        var pts = [EvPoint]()
        func connectPtInCacheWith(_ ind0: Int64, to ind1: Int64) {
            if var p = cache.pointWith(ind: ind0) {
                p.connectedInd = ind1
                pts.append(p)
            }
        }
        connectPtInCacheWith(ind0, to: ind1)
        connectPtInCacheWith(ind1, to: ind0)
        if pts.count > 0 {
            cache.update(points: pts) {}
        }
    }

    fileprivate func _attachTo(ind: Int64, event: Ev) {
        lock.lock()
        events[ind] = event
        lock.unlock()
    }

    func attachTo(ind: Int64, event: Ev) {
        _attachTo(ind: ind, event: event)
        if var p = cache.pointWith(ind: ind) {
            p.event = EvDesc(evId: event.id)
            cache.update(events: [event]) { [weak self] in
                self?.cache.update(points: [p]) {}
            }
        }
    }

    func attachInBatch(pts: [MapAttachDescr], completion: @escaping (() -> Void)) {
        var points = [EvPoint]()
        for p in pts {
            let evd: EvDesc?
            if let e = p.ev {
                evd = EvDesc(evId: e.id)
            } else {
                evd = nil
            }
            func attach(ind: Int64, to i: Int64) {
                if var pp = cache.pointWith(ind: ind) {
                    pp.connectedInd = i
                    pp.event = evd
                    if let e = p.ev {
                        _attachTo(ind: ind, event: e)
                    }
                    _connect(ind0: ind, ind1: i)
                    points.append(pp)
                }
            }
            attach(ind: p.ind0, to: p.ind1)
            attach(ind: p.ind1, to: p.ind0)
        }
        cache.update(points: points, completion: completion)
    }

    func event(ind: Int64) -> Ev? {
        return events[ind]
    }

    func findConnectionTo(point: Int64) -> Int64? {
        if let p = connectedPts[point] {
            return p.ind
        }
        return nil
    }

    fileprivate func createArc(startLocation: CLLocationCoordinate2D,
                               endLocation: CLLocationCoordinate2D) -> GMSPolyline {
        // copyied from
        // https://stackoverflow.com/questions/46934276/how-to-draw-an-arc-on-google-maps-in-ios
        var start = startLocation
        var end = endLocation

        let gradientColors: GMSStrokeStyle
        if start.heading(to: end) < 0.0 {
            // need to reverse the start and end, and reverse the color
            start = endLocation
            end = startLocation

            gradientColors = GMSStrokeStyle.gradient(
                from: theme.pointBColor,
                to:  theme.pointAColor)
        } else {
            gradientColors = GMSStrokeStyle.gradient(
                from: theme.pointAColor,
                to: theme.pointBColor)
        }
        let path = GMSMutablePath()

        // Curve Line
        let k = abs(0.3 * sin((start.heading(to: end)).toRadians)) // was 0.3

        let d = GMSGeometryDistance(start, end)
        let h = GMSGeometryHeading(start, end)
        // Midpoint position
        let p = GMSGeometryOffset(start, d * 0.5, h)
        // Apply some mathematics to calculate position of the circle center
        let x = (1 - k * k) * d * 0.5 / (2 * k)
        let r = (1 + k * k) * d * 0.5 / (2 * k)
        let c = GMSGeometryOffset(p, x, h + 90.0)

        // Polyline options
        // Calculate heading between circle center and two points
        var h1 = GMSGeometryHeading(c, start)
        var h2 = GMSGeometryHeading(c, end)

        if h1 > 180 {
            h1 = h1 - 360
        }
        if h2 > 180 {
            h2 = h2 - 360
        }

        // Calculate positions of points on circle border and add them to polyline options
        let numpoints = 100.0
        let step = (h2 - h1) / numpoints
        for i in stride(from: 0.0, to: numpoints, by: 1) {
            let pi = GMSGeometryOffset(c, r, h1 + i * step)
            path.add(pi)
        }
        path.add(end)

        // Draw polyline
        let polyline = GMSPolyline(path: path)
        polyline.map = mapView // Assign GMSMapView as map
        polyline.strokeWidth = 5.0
        polyline.spans = [GMSStyleSpan(style: gradientColors)]

        return polyline
    }

    func drawArcPolyline(ind0: Int64, ind1: Int64) {
        if let p0 = points[ind0],
            let p1 = points[ind1] {

            let a = p0.marker.position
            let b = p1.marker.position

            arcLine = createArc(startLocation: a, endLocation: b)
            arcLineIndicies = (ind0, ind1)

            let bounds = GMSCoordinateBounds(coordinate: a, coordinate: b)
            let insets = UIEdgeInsets(top: top, left: 20, bottom: 20, right: 20)
            if let camera = mapView.camera(for: bounds, insets: insets) {
                mapView.animate(to: camera)
            }
        }
    }

    func connectedLineIndicies() -> (Int64, Int64)? {
        return arcLineIndicies
    }

    func sameArc(ind0: Int64?, ind1: Int64?) -> Bool {
        if let i0 = ind0,
            let i1 = ind1,
            let a = arcLineIndicies {
            if ((a.0 == i0 && a.1 == i1) ||
                (a.1 == i0 && a.0 == i1)) {
                return true
            }
        }
        return false
    }

    func coordinatesOf(ind: Int64) -> CLLocationCoordinate2D? {
        if let p = points[ind] {
            return p.marker.position
        }
        return nil
    }

    func removeArcPolyline() {
        if let line = arcLine {
            line.map = nil
            arcLine = nil
            arcLineIndicies = nil
        }
    }

    func remove(indicies: [Int64], completion: @escaping (() -> Void)) {
        var iindicies = [Int64]()
        for ind in indicies {
            if let p = points[ind] {
                runOnMainThread {
                    p.marker.map = nil
                }
                lock.lock()
                points[ind] = nil
                events[ind] = nil
                if let pt2 = connectedPts[ind] {
                    connectedPts[pt2.ind] = nil
                    iindicies.append(pt2.ind)
                }
                iindicies.append(ind)
                connectedPts[ind] = nil
                lock.unlock()
            }
        }
        cache.delete(points: iindicies.map { EvPointDesc(ind: $0) },
                     completion: completion)
    }

    func removeAllPoints(completion: @escaping (() -> Void)) {
        runOnMainThread { [weak self] in
            guard let `self` = self else { return }
            for (_, p) in self.points {
                p.marker.map = nil
            }
        }
        lock.lock()
        curId = 0
        points.removeAll()
        events.removeAll()
        connectedPts.removeAll()
        lock.unlock()
        cache.deleteAllPoints(completion: completion)
    }

    func restorePoints() {
        let pts = cache.allPoints()
        if pts.count > 0 {
            for p in pts {
                if p.ind > curId {
                    curId = p.ind
                }
                let coord = CLLocationCoordinate2D(
                    latitude: p.lat,
                    longitude: p.lon)
                _add(ind: p.ind,
                     point: coord,
                     type: p.typeA ? .A : .B,
                     state: p.state)
                if let evd = p.event {
                    if let ev = cache.EvWith(id: evd.evId) {
                        _attachTo(ind: p.ind, event: ev)
                    }
                }
                if let connected = p.connectedInd {
                    _connect(ind0: p.ind, ind1: connected)
                }
            }
        }
    }
}
