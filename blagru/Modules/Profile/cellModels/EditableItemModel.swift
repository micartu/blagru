//
//  EditableItemModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 08.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

enum EditableItemType {
    case text
    case phone
    case password
    case birthday
    case mail
}

struct EditableItemModel: CommonEditableModel {
    let theme: Theme
    let id: String
    var focused: Bool
    let type: EditableItemType
    let title: String
    var placeholder: String
    var text: String
    var customizer: ((Theme, EditableItemCell) -> Void)?
    let delegate: EditableItemCellDelegate?

    func setup(cell: EditableItemCell) {
        cell.id = id
        cell.delegate = delegate

        customizer?(theme, cell)

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.regularFont.withSize(theme.sz.middle4)
        cell.lblTitle.textColor = theme.secondaryTextColor

        cell.editItem.borderStyle = .none
        cell.editItem.text = text
        cell.editItem.isSecureTextEntry = false
        cell.editItem.placeholder = placeholder
        cell.editItem.layer.borderWidth = 1
        cell.editItem.layer.borderColor = theme.borderColor.cgColor
        cell.editItem.layer.backgroundColor = theme.secondaryBackColor.cgColor
        cell.editItem.layer.cornerRadius = 5
        cell.editItem.clipsToBounds = true

        if focused {
            delay(0.2) { [weak cell] in
                cell?.editItem.becomeFirstResponder()
            }
        }

        switch type {
        case .text:
            cell.editItem.keyboardType = .default
        case .phone:
            cell.editItem.keyboardType = .namePhonePad
        case .password:
            cell.editItem.isSecureTextEntry = true
            cell.editItem.keyboardType = .default
        case .mail:
            cell.editItem.keyboardType = .emailAddress
        case .birthday:
            cell.editItem.keyboardType = .numbersAndPunctuation
        }
    }
}
