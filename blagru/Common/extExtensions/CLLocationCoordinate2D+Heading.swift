//
//  CLLocationCoordinate2D+Heading.swift
//  blagru
//
//  Created by Michael Artuerhof on 22.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import CoreLocation

extension CLLocationCoordinate2D {
    func heading(to point: CLLocationCoordinate2D) -> Double {

        func degreesToRadians(_ degrees: Double) -> Double { return degrees * Double.pi / 180.0 }
        func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / Double.pi }

        let lat1 = degreesToRadians(latitude)
        let lon1 = degreesToRadians(longitude)

        let lat2 = degreesToRadians(point.latitude)
        let lon2 = degreesToRadians(point.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)

        return radiansToDegrees(radiansBearing)
    }
}
