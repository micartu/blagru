//
//  Locator.swift
//  blagru
//
//  Created by Michael Artuerhof on 25.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import CoreLocation
import CoreMotion

final class Locator: NSObject {
    weak var delegate: LocatorDelegate? = nil
    var lon: Double = 0
    var lat: Double = 0
    var acc: Double = 0
    private let secrets: SecretService
    private let locationManager = CLLocationManager()
    private var inited = false

    init(secrets: SecretService) {
        self.secrets = secrets
        super.init()
        delegate?.location(enabled: false)
    }

    fileprivate func initLocator() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = true
    }
}

// MARK: - LocatorService
extension Locator: LocatorService {
    func setActiveMode(_ active: Bool) {
        if !inited {
            inited = true
            initLocator()
        }
        if active {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.stopUpdatingLocation()
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension Locator: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedAlways else {
            delegate?.location(enabled: false)
            return
        }
        delegate?.location(enabled: true)
    }

    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        if let last = locations.last {
            lon = last.coordinate.longitude
            lat = last.coordinate.latitude
            acc = min(last.horizontalAccuracy, last.verticalAccuracy)
            delegate?.location(lat: lat, lon: lon)
        }
    }
}
