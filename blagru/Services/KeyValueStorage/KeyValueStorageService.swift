//
//  KeyValueStorageService.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol KeyValueStorageService {
    func value<T: Codable>(forKey key: String) -> T?
    func setValue<T: Codable>(_ value: T?, forKey key: String)
}

extension KeyValueStorageService {
    func makeContainer<T: Codable>(key: String = #function, def: T? = nil) -> KeyValueContainer<T> {
        KeyValueContainer(storage: self, key: key, defaultValue: def)
    }
}
