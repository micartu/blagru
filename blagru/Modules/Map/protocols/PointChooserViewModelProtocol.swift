//
//  PointChooserViewModelProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 04.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

enum OptionalButtonType {
    case favorites
    case savedList
    case delete
    case done
}

protocol PointChooserViewModelProtocol {
    func swipeUp()
    func swipeRight()
    func swipeLeft()

    func actionA()
    func actionB()
    func actionFavorites()
    func actionSavedList()
    func actionDelete()
    func actionDone()
}
