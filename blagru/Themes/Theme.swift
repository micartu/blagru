//
//  ThemeManagerProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol Themeble: class {
    func apply(theme: Theme)
}

struct ThemeSizes {
    let header0: CGFloat
    let header1: CGFloat
    let header2: CGFloat
    let header3: CGFloat

    let middle0: CGFloat
    let middle1: CGFloat
    let middle2: CGFloat
    let middle3: CGFloat
    let middle4: CGFloat

    let small0: CGFloat
    let small1: CGFloat
    let small2: CGFloat
    let small3: CGFloat
}

struct Theme {
    // colors
    let tintColor: UIColor
    let warnColor: UIColor
    let inactiveColor: UIColor
    let mainTextColor: UIColor
    let secondaryTextColor: UIColor
    let separatorColor: UIColor
    let emptyBackColor: UIColor
    let secondaryBackColor: UIColor
    let mainBackColor: UIColor
    let borderColor: UIColor
    let allowedColor: UIColor
    let prettyColor: UIColor
    let pointAColor: UIColor
    let pointBColor: UIColor
    // fonts
    let regularFont: UIFont
    let boldFont: UIFont
    let mediumFont: UIFont
    let semiboldFont: UIFont
    // sizes, used in labels, edit fields etc
    let sz: ThemeSizes
}
