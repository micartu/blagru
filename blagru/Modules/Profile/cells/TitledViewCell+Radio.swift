//
//  TitledViewCell+Radio.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

extension TitledViewCell {
    func setupRadioBoxFrom(descr: [String:Any], theme: Theme) {
        let selected: Int
        if let s = descr[const.selected] as? Int {
            selected = s
        } else {
            selected = -1
        }
        // set theme in order to make baseclass work properly:
        self.theme = theme
        imgTopOffset = 5
        labelXOffset = const.sx + 10
        count = 0
        initialize()
        if let items = descr[const.fields] as? [String] {
            for (i, item) in items.enumerated() {
                let active = (i == selected)
                add(image: "", width: const.sx, height: const.sx, text: item)
                if let last = lastConnected {
                    let b = UIButton()
                    b.tag = i + 1
                    b.addTarget(self,
                                action: #selector(itemTapped(_:)),
                                for: .touchUpInside)
                    last.attachConnectedToBorders(view: b)
                    for v in last.subviews {
                        if let im = v as? UIImageView {
                            setRadio(image: im, active: active)
                        }
                        if let label = v as? UILabel {
                            label.textColor = theme.mainTextColor
                        }
                    }
                }
            }
        }
        finishSetup()
    }

    private func setRadio(image: UIImageView, active: Bool) {
        image.tintColor = active ?
            theme.tintColor : theme.inactiveColor
        let pict: String
        if active {
            pict = const.rpict1
        } else {
            pict = const.rpict0
        }
        image.image = UIImage(named: pict)
    }

    internal func setRadioIn(view: UIView, active: Bool) {
        for v in view.subviews {
            if let im = v as? UIImageView {
                setRadio(image: im, active: active)
            }
        }
    }
}
