//
//  MapKitDelegate.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit
import GoogleMaps

final class MapKitDelegate { }

extension MapKitDelegate: AppDelegateProtocol {
    func description() -> String {
        return "MapKitDelegate"
    }

    func initModule(application: UIApplication,
                    options: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if let plistPath = Bundle.main.path(forResource: "mapcfg", ofType: "plist") {
            let opt = NSDictionary(contentsOfFile: plistPath)!
            if let key = opt["api_key"] as? String {
                GMSServices.provideAPIKey(key)
                return true
            }
        }
        return true
    }
}
