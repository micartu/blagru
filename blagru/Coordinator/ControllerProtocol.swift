//
//  ControllerProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

public protocol ControllerProtocol {
    var vc: UIViewController { get }
}
