//
//  SSInMemoryPortionizedPresenter.swift
//  blagru
//
//  Created by Michael Artuerhof on 29/07/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

// SSInMemoryPortionizedPresenter = SingleSectionAllInMemoryPortionizedPresenter
// the name is too long, so it was shortened a little bit;
// used to load data in portions but store it all in memory

protocol SSInMemoryPortionizedInteractorInput: class {
    func loadModels(with theme: Theme, offset: Int, size: Int)
}

protocol SSInMemoryPortionizedInteractorOutput: class {
    func fetched(models: [CellAnyModel], offset: Int, size: Int)
}

open class SSInMemoryPortionizedPresenter:
   CommonViewModel,
   SSInMemoryPortionizedInteractorOutput {
    // data source
    weak var source: SSInMemoryPortionizedInteractorInput?

    // MARK: SSInMemoryPortionizedInteractorOutput

    func fetched(models: [CellAnyModel], offset: Int, size: Int) {
        runOnMainThread { [weak self] in
            guard let `self` = self else { return }
            self.busy.accept(false)
            let toLoad = offset
            if self.loaded < toLoad && models.count > 0 {
                let moveToLastVisible = IMovePath(row: self.models.count,
                                                  section: 0, animated: true)
                self.loaded = toLoad
                self.models.append(contentsOf: models)
                self.updateData.accept(true)
                self.scrollTo.accept(moveToLastVisible)
            }
        }
    }

    // MARK: CommonViewModel

    func getContents() {
        if busy.value || source == nil { return }
        models.removeAll()
        loaded = -1
        busy.accept(true)
		source?.loadModels(with: theme, offset: 1, size: const.sz)
    }

    func refreshContents() {
        getContents()
    }

    func selected(indexPath: IndexPath) {
    }

    func model(for indexPath: IndexPath) -> Any {
        if models.count > 0 && indexPath.row >= models.count * 3 / 4 {
            let toLoad = indexPath.row / const.sz + 1
            if loaded < toLoad {
                busy.accept(true)
                source?.loadModels(with: theme,
                                   offset: toLoad,
                                   size: const.sz)
            }
        }
        return models[indexPath.row]
    }

    func modelsCount(for section: Int) -> Int {
        return models.count
    }

    func title(for section: Int) -> String {
        return ""
    }

    func sectionsCount() -> Int {
        return 1
    }

    // MARK: useful functions
    internal func findModelWith(id: String) -> (CellAnyModel?, Int) {
        for i in models.indices {
            let m = models[i]
            if m.id == id {
                return (m, i)
            }
        }
        return (nil, -1)
    }

    // MARK: members of CommonViewModel protocol
    var theme: Theme! {
        didSet {
            runOnMainThread { [weak self] in
                self?.getContents()
            }
        }
    }
    var updateItems = BehaviorRelay<IIndexPathCount?> (value: nil)
    var updateData = BehaviorRelay<Bool> (value: false)
    var scrollTo = BehaviorRelay<IMovePath?> (value: nil)
    var removeKeyboard = BehaviorRelay<Bool> (value: false)
    var busy = BehaviorRelay<Bool> (value: false)
    var batchUpdate = BehaviorRelay<Bool?> (value: nil)
    var make = BehaviorRelay<CellChangeType?> (value: nil)

    // MARK: other members
    internal var models = [CellAnyModel]()
    internal var loaded = -1

	internal struct const {
		static let sz = 20
	}
}
