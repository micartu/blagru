//
//  ItemPreparation.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

/// Item preparation
///
struct ItemPreparation {
    /// A closure that prepares a given action item for animation.
    ///
    var prepare: (_ item: ActionItem, _ index: Int, _ numberOfItems: Int, _ actionButton: FloatingActionButton) -> Void

    /// Initializes and returns a newly allocated item preparation object with given prepare closure.
    ///
    /// - Parameter layout: A closure that defines the the layout of given action items relative to an action button.
    ///
    /// - Returns: An initialized item layout object.
    ///
    init(prepare: @escaping (_ item: ActionItem,
        _ index: Int,
        _ numberOfItems: Int,
        _ actionButton: FloatingActionButton) -> Void) {
        self.prepare = prepare
    }

    /// Returns an item preparation object that
    ///   - sets `item.alpha` to `1` and
    ///   - `item.transform` to `identity`.
    ///
    /// - Returns: An item preparation object.
    ///
    static func identity() -> ItemPreparation {
        return ItemPreparation { item, _, _, _ in
            item.transform = .identity
            item.alpha = 1
        }
    }

    /// Returns an item preparation object that
    ///   - sets `item.alpha` to `0` and
    ///   - scales the item by given ratio.
    ///
    /// - Parameter ratio: The factor by which the item is scaled
    ///
    /// - Returns: An item preparation object.
    ///
    static func scale(by ratio: CGFloat = 0.4) -> ItemPreparation {
        return ItemPreparation { item, _, _, _ in
            item.scale(by: ratio)
            item.alpha = 0
        }
    }

    /// Returns an item preparation object that
    ///   - sets `item.alpha` to `0`,
    ///   - offsets the item by given values and
    ///   - scales the item by given ratio.
    ///
    /// - Parameter translationX: The value in points by which the item is offsetted horizontally
    /// - Parameter translationY: The value in points by which the item is offsetted vertically
    /// - Parameter scale: The factor by which the item is scaled
    ///
    /// - Returns: An item preparation object.
    ///
    static func offset(translationX: CGFloat, translationY: CGFloat, scale: CGFloat = 0.4) -> ItemPreparation {
        return ItemPreparation { item, _, _, _ in
            item.scale(by: scale, translationX: translationX, translationY: translationY)
            item.alpha = 0
        }
    }

    /// Returns an item preparation object that
    ///   - sets `item.alpha` to `0`,
    ///   - offsets the item horizontally by given values.
    ///
    /// - Parameter distance: The value in points by which the item is offsetted horizontally
    ///                       towards the closest vertical edge of the screen.
    /// - Parameter scale: The factor by which the item is scaled
    ///
    /// - Remark: The item is offsetted towards the closest vertical edge of the screen.
    ///
    /// - Returns: An item preparation object.
    ///
    static func horizontalOffset(distance: CGFloat = 50, scale: CGFloat = 0.4) -> ItemPreparation {
        return ItemPreparation { item, _, _, actionButton in
            let translationX = actionButton.isOnLeftSideOfScreen ? -distance : distance
            item.scale(by: scale, translationX: translationX)
            item.alpha = 0
        }
    }

    /// Returns an item preparation object that
    ///   - sets `item.alpha` to `0`,
    ///   - offsets the item horizontally by given values.
    ///
    /// - Parameter distance: The value in points by which the item is offsetted
    ///                       towards the action button.
    /// - Parameter scale: The factor by which the item is scaled
    ///
    /// - Remark: The item is offsetted towards the action button.
    ///
    /// - Returns: An item preparation object.
    ///
    static func circularOffset(distance: CGFloat = 50, scale: CGFloat = 0.4) -> ItemPreparation {
        return ItemPreparation { item, index, numberOfItems, actionButton in
            let itemAngle = ItemAnimationConfiguration.angleForItem(at: index,
                                                                    numberOfItems: numberOfItems,
                                                                    actionButton: actionButton)
            let transitionAngle = itemAngle + CGFloat.pi
            let translationX = distance * cos(transitionAngle)
            let translationY = distance * sin(transitionAngle)
            item.scale(by: scale, translationX: translationX, translationY: translationY)
            item.alpha = 0
        }
    }
}

// MARK: - Helpers

internal extension ItemAnimationConfiguration {
    static func angleForItem(at index: Int, numberOfItems: Int, actionButton: FloatingActionButton) -> CGFloat {
        precondition(numberOfItems > 0)
        precondition(index >= 0)
        precondition(index < numberOfItems)

        let startAngle: CGFloat = actionButton.isOnLeftSideOfScreen ? 2 * .pi : .pi
        let endAngle: CGFloat = 1.5 * .pi

        switch (numberOfItems, index) {
        case (1, _):
            return (startAngle + endAngle) / 2
        case (2, 0):
            return startAngle + 0.1 * (endAngle - startAngle)
        case (2, 1):
            return endAngle - 0.1 * (endAngle - startAngle)
        default:
            return startAngle + CGFloat(index) * (endAngle - startAngle) / (CGFloat(numberOfItems) - 1)
        }
    }
}


fileprivate extension ActionItem {
    func scale(by factor: CGFloat, translationX: CGFloat = 0, translationY: CGFloat = 0) {
        let scale = scaleTransformation(factor: factor)
        let translation = CGAffineTransform(translationX: translationX, y: translationY)
        transform = scale.concatenating(translation)
    }
}

fileprivate extension ActionItem {
    func scaleTransformation(factor: CGFloat) -> CGAffineTransform {
        let scale = CGAffineTransform(scaleX: factor, y: factor)

        let center = circleView.center
        let circleCenterScaled = point(center, transformed: scale)
        let translationX = center.x - circleCenterScaled.x
        let translationY = center.y - circleCenterScaled.y
        let translation = CGAffineTransform(translationX: translationX, y: translationY)
        return scale.concatenating(translation)
    }

    func point(_ point: CGPoint, transformed transform: CGAffineTransform) -> CGPoint {
        let anchorPoint = CGPoint(x: bounds.width * layer.anchorPoint.x, y: bounds.height * layer.anchorPoint.y)
        let relativePoint = CGPoint(x: point.x - anchorPoint.x, y: point.y - anchorPoint.y)
        let transformedPoint = relativePoint.applying(transform)
        let result = CGPoint(x: anchorPoint.x + transformedPoint.x, y: anchorPoint.y + transformedPoint.y)
        return result
    }
}

internal extension UIView {
    var isOnLeftSideOfScreen: Bool {
        return isOnLeftSide(ofView: UIApplication.shared.keyWindow)
    }

    func isOnLeftSide(ofView superview: UIView?) -> Bool {
        guard let superview = superview else {
            return false
        }
        let point = convert(center, to: superview)
        return point.x < superview.bounds.size.width / 2
    }
}
