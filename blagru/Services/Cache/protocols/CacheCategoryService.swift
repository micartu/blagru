//
//  CacheCategoryService.swift
//  blagru
//
//  Created by Michael Artuerhof on 15.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol CategoryDescProtocol {
    var cat_id: String { get }
}

struct CategoryDesc: CategoryDescProtocol {
    let cat_id: String
}

protocol CacheCategoryService {
    func categoryExistsWith(id: String) -> Bool
    func create(categories: [EvCategory],
                completion: @escaping (() -> Void))
    func update(categories: [EvCategory],
                completion: @escaping (() -> Void))
    func latestUsedCategories(maxItems: Int) -> [EvCategory]
    func categoryWith(id: String) -> EvCategory?
    func delete(categories: [CategoryDescProtocol],
                completion: @escaping (() -> Void))
    func deleteAllCategories(completion: @escaping (() -> Void))
}
