//
//  LoginViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import GoogleSignIn

protocol LoginDispatcherProtocol: class {
    func authorized()
    func callRegister()
}

protocol LoginViewModelProtocol {
    func loginPressed()
    func registerPressed()
    func googlePressed()
    func togglePassVisibility()
}

protocol LoginViewProtocol: class {
    var login: String { get }
    var password: String { get }

    func set(enabled: Bool)
    func pass(show: Bool)
}

final class LoginViewModel {
    weak var dispatcher: (AlertProtocol & LoginDispatcherProtocol)?
    weak var view: LoginViewProtocol?
    internal let net: NetAuthService
    internal var secret: SecretService
    internal let cache: CacheUserService

    private var passShown = false

    init(net: NetAuthService,
         secret: SecretService,
         cache: CacheUserService) {
        self.net = net
        self.secret = secret
        self.cache = cache
        NotificationCenter.default.addObserver(self, selector: #selector(handleSocialAuth(obj:)),
                                               name: NSNotification.Name(rawValue: Const.notification.authSocial),
                                               object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension LoginViewModel: LoginViewModelProtocol {
    func registerPressed() {
        dispatcher?.callRegister()
    }

    func googlePressed() {
        dispatcher?.showBusyIndicator()
        view?.set(enabled: false)
        GIDSignIn.sharedInstance()?.signIn()
    }

    func loginPressed() {
        guard let l = view?.login,
            let p = view?.password,
            l.count > 0,
            p.count > 0
            else { return }
        view?.set(enabled: false)
        dispatcher?.showBusyIndicator()
        secret.isSocialAuth = false
        net.auth(login: l,
                 password: p, success: { [weak self] ans in
                    if let usr = ans["user"] as? NSDictionary {
                        let u = parseUser(from: usr)
                        self?.cache.update(user: u) { // create / update given user in cache
                            self?.view?.set(enabled: true)
                            self?.secret.isAuthCreated = true
                            self?.secret.userID = u.id
                            self?.dispatcher?.hideBusyIndicator()
                            self?.dispatcher?.authorized()
                        }
                    } else {
                        self?.dispatcher?.show(error: "User wasn't found in server's answer!".localized)
                    }
        }, failure: { [weak self] e in
            self?.view?.set(enabled: true)
            self?.dispatcher?.hideBusyIndicator()
            self?.dispatcher?.show(error: e.localizedDescription)
        })
    }

    func togglePassVisibility() {
        passShown = !passShown
        view?.pass(show: passShown)
    }
}

extension LoginViewModel {
    @objc
    fileprivate func handleSocialAuth(obj: Notification) {
        view?.set(enabled: true)
        dispatcher?.hideBusyIndicator()
        if let e = obj.object as? NSError {
            dispatcher?.show(error: e.localizedDescription)
        } else if let s = obj.object as? SocialData {
            dispatcher?.showBusyIndicator()
            net.socialAuth(
                type: s.netType,
                token: s.accessToken,
                success: { [weak self] ans in
                    self?.dispatcher?.hideBusyIndicator()
                    self?.secret.isSocialAuth = true
                    self?.dispatcher?.authorized()
                }, failure: { [weak self] e in
                    self?.dispatcher?.hideBusyIndicator()
                    self?.secret.isSocialAuth = false
                    self?.dispatcher?.show(error: e.localizedDescription)
            })
        }
    }
}
