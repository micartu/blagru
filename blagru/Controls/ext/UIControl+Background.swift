//
//  UIControl+Background.swift
//  blagru
//
//  Created by Michael Artuerhof on 16.07.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

extension UIControl {
    internal func createBackgroundWith(_ color: UIColor) -> UIImage {
        let size: CGSize = CGSize(width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let bgImage: UIImage = (UIGraphicsGetImageFromCurrentImageContext() as UIImage?)!
        UIGraphicsEndImageContext()
        clipsToBounds = true
        return bgImage
    }
}
