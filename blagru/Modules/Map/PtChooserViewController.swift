//
//  PtChooserViewController.swift
//  blagru
//
//  Created by Michael Artuerhof on 04.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit
import Repeat

final class PtChooserViewController: BaseViewController {
    var vm: PointChooserViewModelProtocol!

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var btnA: UIButton!
    @IBOutlet weak var btnB: UIButton!
    @IBOutlet weak var btnDelete: ScaledImageButton!
    @IBOutlet weak var btnDone: ScaledImageButton!
    @IBOutlet weak var btnFav: ScaledImageButton!
    @IBOutlet weak var btnSaved: ScaledImageButton!
    @IBOutlet weak var lblDescr: UILabel!
    @IBOutlet weak var stackView: UIStackView!

    fileprivate var msgTimer: Repeater?
    fileprivate var blinkTimer: Repeater?

    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipe))
        swipeRight.direction = .right

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipe))
        swipeLeft.direction = .left

        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(upSwipe))
        swipeUp.direction = .up
        [
            swipeLeft,
            swipeRight,
            swipeUp,
        ].forEach {
            view.addGestureRecognizer($0)
        }
        buttons().forEach {
            $0.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        }
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(rightSwipe))
        lblDescr.isUserInteractionEnabled = true
        lblDescr.addGestureRecognizer(tap)
        makeStackView(hidden: false, animated: false)
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        let clr = theme.inactiveColor.withAlphaComponent(0.8).cgColor
        container.layer.cornerRadius = 8
        container.layer.borderColor = clr
        container.layer.borderWidth = 1
        container.layer.backgroundColor = clr
        view.backgroundColor = .clear
        let sclr = theme.secondaryTextColor.cgColor
        for b in buttons() {
            if b is UIButton { // A and B buttons
                b.layer.cornerRadius = 8
                b.layer.borderColor = sclr
                b.layer.borderWidth = 1
                b.layer.backgroundColor = sclr
            } else if let s = b as? ScaledImageButton { // stack view buttons
                s.buttonImageColor = theme.tintColor
            }
        }
        lblDescr.font = theme.regularFont.withSize(theme.sz.middle4)
    }

    func buttons() -> [UIControl] {
        return [
            btnA,
            btnB,
            btnFav,
            btnSaved,
            btnDelete,
            btnDone,
        ]
    }

    // MARK: - Actions

    @objc fileprivate func buttonAction(_ sender: UIView) {
        let action: (() -> Void)?
        switch sender {
        case btnA:
            action = { [weak self] in
                self?.vm.actionA()
            }
        case btnB:
            action = { [weak self] in
                self?.vm.actionB()
            }
        case btnFav:
            action = { [weak self] in
                self?.vm.actionFavorites()
            }
        case btnSaved:
            action = { [weak self] in
                self?.vm.actionSavedList()
            }
        case btnDelete:
            action = { [weak self] in
                self?.vm.actionDelete()
            }
        case btnDone:
            action = { [weak self] in
                self?.vm.actionDone()
            }
        default:
            action = nil
        }
        if let a = action {
            applyTouchAnimation(sender, completion: a)
        }
    }

    @objc fileprivate func rightSwipe() {
        vm.swipeRight()
    }

    @objc fileprivate func leftSwipe() {
        vm.swipeLeft()
    }

    @objc fileprivate func upSwipe() {
        vm.swipeUp()
    }
}

// MARK: - PtChooserViewProtocol
extension PtChooserViewController: PtChooserViewProtocol {
    func blink(button: dstButtonType) {
        blinkTimer?.removeAllObservers(thenStop: true)
        blinkTimer = nil
        var blankBlink = true
        func blinkLogic(_ btn: UIButton?, blankBlink: Bool) {
            runOnMainThread { [weak self] in
                guard let `self` = self else { return }
                btn?.layer.backgroundColor = blankBlink ?
                    self.theme.prettyColor.cgColor : self.theme.inactiveColor.cgColor
            }
        }
        // reset buttons to the normal (nonblinking) state:
        [btnA, btnB].forEach { blinkLogic($0, blankBlink: false) }
        let kT: TimeInterval = 0.25
        switch button {
        case .source:
            blinkTimer = Repeater.init(interval: .seconds(kT)) { [weak self] _ in
                blankBlink = !blankBlink
                blinkLogic(self?.btnA, blankBlink: blankBlink)
            }
        case .dest:
            blinkTimer = Repeater.init(interval: .seconds(kT)) { [weak self] _ in
                blankBlink = !blankBlink
                blinkLogic(self?.btnB, blankBlink: blankBlink)
            }
        default:
            ()
        }
        blinkTimer?.start()
    }

    func showMsg(_ msg: String, timeout: TimeInterval) {
        msgTimer?.removeAllObservers(thenStop: true)
        msgTimer = nil
        lblDescr.text = msg
        if timeout < 0 { // show it forever
            // hide stack (and show label) view, exit
            makeStackView(hidden: true, animated: true)
            return
        }
        if stackView.transform == .identity {
            makeStackView(hidden: true, animated: true)
        }
        msgTimer = Repeater.once(after: .seconds(timeout)) { [weak self] _ in
            runOnMainThread {
                self?.makeStackView(hidden: false, animated: true)
            }
        }
    }

    func makeStackView(hidden: Bool,
                       animated: Bool,
                       completion: (() -> Void)? = nil) {
        func makeIT() {
            let tr: CGAffineTransform
            if hidden {
                tr = CGAffineTransform(translationX: 300, y: 0)
            } else {
                tr = .identity
            }
            stackView.transform = tr
            lblDescr.alpha = hidden ? 1 : 0
        }
        if animated {
            UIView.animate(withDuration: 0.3, delay: 0, animations: {
                makeIT()
            }, completion: { _ in
                completion?()
            })
        } else {
            makeIT()
            completion?()
        }
    }

    private func convert(button: OptionalButtonType) -> ScaledImageButton {
        let btn: ScaledImageButton
        switch button {
        case .delete:
            btn = btnDelete
        case .savedList:
            btn = btnSaved
        case .favorites:
            btn = btnFav
        case .done:
            btn = btnDone
        }
        return btn
    }

    func make(button: OptionalButtonType, hidden: Bool) {
        let btn = convert(button: button)
        btn.isHidden = hidden
    }

    func makeButtons(hide: [OptionalButtonType], appear: [OptionalButtonType]) {
        let toHide = hide.map { convert(button: $0) }
        let toShow = appear.map { convert(button: $0) }
        toHide.forEach { $0.isHidden = true }
        toShow.forEach { $0.isHidden = false }
    }

    func make(button: OptionalButtonType, active: Bool) {
        let btn = convert(button: button)
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let t = self?.theme else { return }
            btn.buttonImageColor = active ? t.tintColor : t.mainBackColor
        }
    }
}
