//
//  FavoriteCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol FavoriteCellDelegate: TouchableCell {
    func starTouchedOn(id: String)
}

final class FavoriteCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(cellTapped))
        addGestureRecognizer(tap)
        btnFavorite.isUserInteractionEnabled = true
        btnFavorite.addTarget(self,
                              action: #selector(starTapped),
                              for: .touchUpInside)
    }

    @objc private func cellTapped () {
        delegate?.cellTouched(id: id)
    }

    @objc private func starTapped () {
        delegate?.starTouchedOn(id: id)
    }

    var id = ""
    weak var delegate: FavoriteCellDelegate?
}
