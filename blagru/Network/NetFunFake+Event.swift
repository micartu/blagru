//
//  NetFunFake+Event.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFunFake: NetEventService {
    func allCategories(completion: @escaping (([EvCategory]?, NSError?) -> Void)) {
        let cats = ["taxi", "booze", "concert"].map { EvCategory(id: $0, name: $0, icon: $0) }
        completion(cats, nil)
    }

    func connectedUsersForEvent(id: String,
                                completion: @escaping (([User]?, NSError?) -> Void)) {
        let u1 = User(id: "1", login: "u1", name: "ivan", email: "ivan@mail.ru", avatar: "user", password: "")
        let u2 = User(id: "2", login: "u1", name: "ivan", email: "ivan@mail.ru", avatar: "twitter", password: "")
        let u3 = User(id: "3", login: "u1", name: "ivan", email: "ivan@mail.ru", avatar: "facebook", password: "")
        completion([u1, u2, u3], nil)
    }

    func create(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(event, nil)
    }

    func update(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(event, nil)
    }

    func join(eventId: String, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(nil, nil)
    }

    func delete(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(event, nil)
    }
}
