//
//  CUser+CoreDataProperties.swift
//  blagru
//
//  Created by Michael Artuerhof on 19.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CUser> {
        return NSFetchRequest<CUser>(entityName: "CUser")
    }

    @NSManaged public var id: String?
    @NSManaged public var avatar: String?
    @NSManaged public var email: String?
    @NSManaged public var login: String?
    @NSManaged public var name: String?
    @NSManaged public var surname: String?
    @NSManaged public var owning_events: NSSet?
    @NSManaged public var participating_events: NSSet?
    @NSManaged public var favorite_evs: NSSet?

}

// MARK: Generated accessors for owning_events
extension CUser {

    @objc(addOwning_eventsObject:)
    @NSManaged public func addToOwning_events(_ value: CEv)

    @objc(removeOwning_eventsObject:)
    @NSManaged public func removeFromOwning_events(_ value: CEv)

    @objc(addOwning_events:)
    @NSManaged public func addToOwning_events(_ values: NSSet)

    @objc(removeOwning_events:)
    @NSManaged public func removeFromOwning_events(_ values: NSSet)

}

// MARK: Generated accessors for participating_events
extension CUser {

    @objc(addParticipating_eventsObject:)
    @NSManaged public func addToParticipating_events(_ value: CEv)

    @objc(removeParticipating_eventsObject:)
    @NSManaged public func removeFromParticipating_events(_ value: CEv)

    @objc(addParticipating_events:)
    @NSManaged public func addToParticipating_events(_ values: NSSet)

    @objc(removeParticipating_events:)
    @NSManaged public func removeFromParticipating_events(_ values: NSSet)

}

// MARK: Generated accessors for favorite_evs
extension CUser {

    @objc(addFavorite_evsObject:)
    @NSManaged public func addToFavorite_evs(_ value: CEv)

    @objc(removeFavorite_evsObject:)
    @NSManaged public func removeFromFavorite_evs(_ value: CEv)

    @objc(addFavorite_evs:)
    @NSManaged public func addToFavorite_evs(_ values: NSSet)

    @objc(removeFavorite_evs:)
    @NSManaged public func removeFromFavorite_evs(_ values: NSSet)

}
