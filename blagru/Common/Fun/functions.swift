//
//  functions.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import STPopup

func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func runOnMainThread(_ closure: @escaping ()->()) {
    if Thread.isMainThread {
        closure()
    } else {
        DispatchQueue.main.async {
            closure()
        }
    }
}

func threadId() -> String {
    return Thread.current.debugDescription
}

func applicationVersion() -> String {
    if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
        var v = " \(version)"
        if let bv = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            v += ".\(bv)"
        }
        return v
    }
    else {
        return ""
    }
}

func open(link: String) {
    if link.contains("http") {
        if let url = URL(string: link) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

func appendErrorMessagesFrom(_ dict: NSDictionary,
                             error: String,
                             field: String) -> (String, Bool) {
    return (error, false)
}

func formErrorMessageFrom(_ error: NSError) -> String {
    var descr = error.localizedDescription
    if let infos = error.userInfo[Const.err.fields.data] as? [NSDictionary] {
        for info in infos {
            for (k, _) in info {
                if let f = k as? String {
                    (descr, _) = appendErrorMessagesFrom(info, error: descr, field: f)
                }
            }
        }
    }
    return descr
}

private struct const {
    static let rowSeparator: Character = "|"
    static let colSeparator: Character = "~"
}

func extractDictFromString(_ s: String) -> [String: String] {
    let rows = s.split(separator: const.rowSeparator)
    if rows.count > 0 {
        var d = [String: String]()
        for r in rows {
            let k = r.split(separator: const.colSeparator)
            if k.count > 1 {
                d[String(k[0])] = String(k[1])
            }
        }
        return d
    }
    return [:]
}

func dictToString(_ d: [String: String]) -> String {
    var s = ""
    for (k, v) in d {
        if s.count > 0 {
            s += String(const.rowSeparator)
        }
        s += "\(k)\(const.colSeparator)\(v)"
    }
    return s
}

func currencyFormatter() -> NumberFormatter {
    let pf = NumberFormatter()
    pf.numberStyle = .currency
    pf.currencySymbol = "₽"
    pf.maximumFractionDigits = 2
    pf.minimumFractionDigits = 0
    return pf
}

func check(value: Int64, mask: Int64) -> Bool {
    if value & mask == mask {
        return true
    }
    return false
}

func imageWith(image: UIImage, scaledToSize newSize: CGSize, scale: CGFloat = 0.0) -> UIImage {
    let r = UIGraphicsImageRenderer(size: newSize)
    let im = r.image { _ in
        let sz = CGSize(width: newSize.width,
                        height: newSize.height)
        image.draw(
            in: CGRect(origin: CGPoint.zero,
                       size: sz)
        )
    }
    return im
}

@inline(__always)
func heightFrom(width: CGFloat, proportional sz: CGSize) -> CGFloat {
    assert(sz.width > 0.001)
    return sz.height / sz.width * width
}

@inline(__always)
func widthFrom(height: CGFloat, proportional sz: CGSize) -> CGFloat {
    assert(sz.height > 0.001)
    return sz.width / sz.height * height
}

func generatePopUp(for vc: UIViewController,
                   yfactor: CGFloat = 0.8,
                   xfactor: CGFloat = 0.8) -> STPopupController {
    return generatePopUp(for: vc,
                         width: -1,
                         height: -1,
                         xfactor: xfactor,
                         yfactor: yfactor)
}

private func getMeasureFor(value: CGFloat, lessThen: CGFloat) -> CGFloat {
    if value > lessThen || value < 0 {
        return lessThen
    }
    return value
}

func generatePopUp(for vc: UIViewController,
                   width: CGFloat,
                   height: CGFloat,
                   xfactor: CGFloat = 0.8,
                   yfactor: CGFloat = 0.8) -> STPopupController {
    let popUp = STPopupController(rootViewController: vc)
    popUp.style = .formSheet
    popUp.transitionStyle = .fade
    let sz = UIScreen.main.bounds.size
    popUp.containerView.layer.cornerRadius = 8
    let widthPortrait = getMeasureFor(value: width, lessThen: sz.width * xfactor)
    let heightPortrait = getMeasureFor(value: height, lessThen: sz.height * yfactor)
    vc.contentSizeInPopup = CGSize(width: widthPortrait,
                                   height: heightPortrait)
    let widthLand = getMeasureFor(value: height, lessThen: sz.height * xfactor)
    let heighLand = getMeasureFor(value: width, lessThen: sz.width * yfactor)
    vc.landscapeContentSizeInPopup = CGSize(width: widthLand,
                                            height: heighLand)
    return popUp
}
