//
//  ItemsToSync.swift
//  blagru
//
//  Created by Michael Artuerhof on 22.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

final class ItemsToSync: ItemsToSyncProtocol {
    var items: [SyncItemProtocol]
    private let interactor: ItemsToSyncActionsProtocol?

    init(items: [SyncItemProtocol],
         interactor: ItemsToSyncActionsProtocol?) {
        self.items = items
        self.interactor = interactor
    }

    func insert(_ item: SyncItemProtocol,
                at index: Int,
                completion: @escaping (() -> Void)) {
        interactor?.insert(item, at: index,
                           completion: completion) ??
            completion()
    }

    func replace(_ item: SyncItemProtocol,
                 at index: Int,
                 completion: @escaping (() -> Void)) {
        interactor?.replace(item, at: index,
                            completion: completion) ??
            completion()
    }

    func remove(_ item: SyncItemProtocol,
                completion: @escaping (() -> Void)) {
        interactor?.remove(item, completion: completion) ??
            completion()
    }
}
