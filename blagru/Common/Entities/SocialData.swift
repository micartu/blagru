//
//  SocialData.swift
//  blagru
//
//  Created by Michael Artuerhof on 19.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct SocialData {
    let netType: String
    let accessToken: String
    let idToken: String
    let expire: Date?
}
