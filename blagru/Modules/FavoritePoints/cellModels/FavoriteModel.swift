//
//  FavoriteModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol FavoriteCellModel: CommonCell { }

struct FavoriteModel: FavoriteCellModel {
    let theme: Theme
    let id: String
    let title: String
    let descr: String
	weak var delegate: FavoriteCellDelegate?

	func setup(cell: FavoriteCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblTitle.textColor = theme.mainTextColor

        cell.lblMessage.text = descr
        cell.lblMessage.font = theme.regularFont.withSize(theme.sz.small0)
        cell.lblMessage.textColor = theme.mainTextColor

        cell.containerView.layer.cornerRadius = 8
        cell.containerView.layer.borderWidth = 1
        cell.containerView.layer.borderColor = theme.borderColor.cgColor
        cell.containerView.backgroundColor = theme.mainBackColor
        cell.backgroundColor = .clear
    }
}
