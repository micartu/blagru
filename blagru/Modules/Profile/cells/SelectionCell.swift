//
//  SelectionCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import DropDown

final class SelectionCell: DropDownCell {
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var borderView: BorderedView!
}
