//
//  TouchableRoundedBadgedModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 30.09.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol TouchableRoundedBadgedCellModel: CommonCell { }

struct TouchableRoundedBadgedModel: TouchableRoundedBadgedCellModel {
    let id: String
    let theme: Theme
    let image: String
    let icon: UIImage?
    let count: Int
    weak var delegate: TouchableCell?
    let imgLoader: ImageLoaderProtocol
    var customizer: ((TouchableRoundedBadgedCell, Theme) -> Void)?
    let touchAction: ((TouchableAnimatableCell) -> Void)?

    func setup(cell: TouchableRoundedBadgedCell) {
        cell.id = id
        cell.isOpaque = false
        cell.backgroundColor = .clear
        cell.delegate = delegate
        cell.touchAction = touchAction

        customizer?(cell, theme)

        if count > 0 {
            let c = count < 100 ? count : 99
            cell.lblBadge.isHidden = false
            cell.lblBadge.text = "\(c)"
            cell.lblBadge.font = theme.regularFont.withSize(theme.sz.small3)
            cell.lblBadge.textColor = theme.emptyBackColor
            cell.lblBadge.layer.cornerRadius = cell.lblBadge.bounds.width / 2
            cell.lblBadge.layer.masksToBounds = true
            cell.lblBadge.backgroundColor = theme.warnColor
        } else {
            cell.lblBadge.isHidden = true
        }
        if let i = icon {
            cell.iconView.image = i
        } else {
            imgLoader.load(image: image) { [weak cell] im in
                if cell?.id != self.id { return }
                cell?.iconView.image = im
            }
        }
    }
}
