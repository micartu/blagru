//
//  NetEventService.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol NetEventService {
    func connectedUsersForEvent(id: String,
                                completion: @escaping (([User]?, NSError?) -> Void))
    func allCategories(completion: @escaping (([EvCategory]?, NSError?) -> Void))
    func create(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void))
    func update(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void))
    func join(eventId: String, completion: @escaping ((Ev?, NSError?) -> Void))
    func delete(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void))
}
