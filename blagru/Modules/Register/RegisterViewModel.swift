//
//  RegisterViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol RegisterDispatcherProtocol: class {
    func authorized()
}

protocol RegisterViewModelProtocol {
    func registerPressed()
    func togglePassVisibility()
}

protocol RegisterViewProtocol: class {
    var login: String { get }
    var name: String { get }
    var email: String { get }
    var password: String { get }
    var repeatPassword: String { get }

    func set(enabled: Bool)
    func pass(show: Bool)
}

final class RegisterViewModel {
    weak var dispatcher: (AlertProtocol & RegisterDispatcherProtocol)?
    weak var view: RegisterViewProtocol?
    internal let net: NetAuthService
    internal var secret: SecretService
    internal let cache: CacheUserService

    private var passShown = false

    init(net: NetAuthService,
         secret: SecretService,
         cache: CacheUserService) {
        self.net = net
        self.secret = secret
        self.cache = cache
    }
}

extension RegisterViewModel: RegisterViewModelProtocol {
    func registerPressed() {
        guard let l = view?.login,
            let m = view?.email,
            let n = view?.name,
            let p = view?.password,
            let p2 = view?.repeatPassword,
            l.count > 0,
            p.count > 0
            else { return }
        if p != p2 {
            dispatcher?.show(error: "passwords must be equal!".localized)
        } else {
            view?.set(enabled: false)
            dispatcher?.showBusyIndicator()
            net.register(
                login: l,
                name: n,
                email: m,
                password: p,
                success: { [weak self] ans in
                    let u = parseUser(from: ans)
                    self?.view?.set(enabled: true)
                    self?.dispatcher?.hideBusyIndicator()
                    self?.secret.isAuthCreated = true
                    self?.secret.isSocialAuth = false
                    self?.secret.userID = u.id
                    self?.dispatcher?.authorized()
                },
                failure: { [weak self] e in
                    self?.view?.set(enabled: true)
                    self?.dispatcher?.hideBusyIndicator()
                    self?.dispatcher?.show(error: e.localizedDescription)
                })
        }
    }

    func togglePassVisibility() {
        passShown = !passShown
        view?.pass(show: passShown)
    }
}
