//
//  ImageLoaderService.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

typealias ImageLoaderErrCompletion =  ((UIImage?, NSError?) -> Void)
typealias ImageLoaderCompletion =  ((UIImage) -> Void)

protocol ImageLoaderService: class {
    func errorableLoadOf(_ image: String, completion: @escaping ImageLoaderErrCompletion)
    func loadOffline(image: String) -> UIImage?
    func load(image: String, completion: @escaping ImageLoaderCompletion)
    func errableLoad(image: String, completion: @escaping ImageLoaderErrCompletion)
    func invalidateCached(image: String, completion: @escaping (() -> Void))
}
