//
//  RoundTextField.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.04.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class RoundTextField: UITextField {
    @IBInspectable
    var cornerRadius: CGFloat = 20 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable
    var xOffset: CGFloat = 40 {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var imageOffset: CGFloat = 8 {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var rightOffset: CGFloat = 30 {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var rightViewCX: CGFloat = 10 {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var imageWidth: CGFloat = 0 {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var imageHeight: CGFloat = 0 {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var imageName: String = "" {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var imageTintColor: UIColor = UIColor.blue {
        didSet {
            sharedInit()
        }
    }

    @IBInspectable
    var customBGColor: UIColor? = nil {
        didSet {
            refresh(color: customBGColor)
        }
    }

    private func refresh(color: UIColor?) {
        if let color = color {
            background = createBackgroundWith(color)
        }
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: xOffset, dy: 0)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: xOffset, dy: 0)
    }

    override init(frame: CGRect) {
        super.init(frame: frame);
        sharedInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }

    override func prepareForInterfaceBuilder() {
        sharedInit()
    }

    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        if let im = UIImage(named: imageName) {
            let sz = im.size
            return CGRect(x: imageOffset / 2,
                          y: (bounds.size.height - sz.height) / 2,
                          width: sz.width,
                          height: sz.height)
        }
        return .zero
    }

    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.width - rightOffset,
                      y: (bounds.height - rightViewCX) / 2,
                      width: rightViewCX, height: rightViewCX)
    }

    func sharedInit() {
        refresh(color: customBGColor)
        if imageName.count > 0 {
            if let im = UIImage(named: imageName) {
                let imv = UIImageView(image: im)
                imv.tintColor = imageTintColor
                imv.frame = CGRect(x: 0, y: 0,
                                   width: im.size.width,
                                   height: im.size.height)
                leftView = imv
                leftViewMode = .always
            }
        }
    }
}
