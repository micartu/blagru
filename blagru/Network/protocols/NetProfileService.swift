//
//  NetProfileService.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.07.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol NetProfileService {
    func uploadProfile(avatar: UIImage,
                       success: (() -> Void)?,
                       failure: ((NSError) -> Void)?)
    func save(user: NSDictionary,
              completion: @escaping (() -> Void),
              failure: @escaping ((NSError) -> Void))
}
