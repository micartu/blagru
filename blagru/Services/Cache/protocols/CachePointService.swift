//
//  CachePointService.swift
//  blagru
//
//  Created by Michael Artuerhof on 16.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol EvPointDescProtocol {
    var ind: Int64 { get }
}

struct EvPointDesc: EvPointDescProtocol {
    let ind: Int64
}

protocol CachePointService {
    func pointExistsWith(ind: Int64) -> Bool
    func create(points: [EvPoint],
                completion: @escaping (() -> Void))
    func update(points: [EvPoint],
                completion: @escaping (() -> Void))
    func pointWith(ind: Int64) -> EvPoint?
    func allPoints() -> [EvPoint]
    func delete(points: [EvPointDescProtocol],
                completion: @escaping (() -> Void))
    func deleteAllPoints(completion: @escaping (() -> Void))
}
