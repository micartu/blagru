//
//  Cache+Convert.swift
//  blagru
//
//  Created by Michael Artuerhof on 21/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheConvertionService {
    func convert(object: Any) -> Any {
        if let cu = object as? CUser {
            return convert(user: cu)
        }
        if let ce = object as? CEv {
            return convert(event: ce)
        }
        if let cc = object as? CCategory {
            return convert(category: cc)
        }
        if let cp = object as? CEvPoint {
            return convert(point: cp)
        }
        fatalError("Cache: cannot find for core data object: \(object) an equivalent entity")
    }
}
