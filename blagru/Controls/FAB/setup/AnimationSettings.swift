//
//  AnimationSettings.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

/// General animation configuration settings
///
struct AnimationSettings {
    /// Duration of the animation.
    /// Default is `0.3`
    ///
    var duration: TimeInterval = 0.3

    /// Damping ratio of the animation.
    /// Default is `0.55`
    ///
    /// - Remark: Not used for transitions.
    ///
    var dampingRatio: CGFloat = 0.55

    /// Initial velocity of the animation.
    /// Default is `0.3`
    ///
    /// - Remark: Not used for transitions.
    ///
    var initialVelocity: CGFloat = 0.3

    /// Delay in between two item animations.
    /// Default is `0.1`
    ///
    /// - Remark: Only used for item animations.
    ///
    var interItemDelay: TimeInterval = 0.1

    /// Initializes and returns a newly allocated animation settings object with specified parameters.
    ///
    /// - Parameter duration: Duration of the animation. Default is `0.3`.
    /// - Parameter dampingRatio: Damping ratio of the animation. Default is `0.55`
    /// - Parameter initialVelocity: Initial velocity of the animation. Default is `0.3`
    /// - Parameter interItemDelay: Delay in between two item animations. Default is `0.1`
    ///
    /// - Returns: An initialized animation settings object.
    ///
    init(duration: TimeInterval = 0.3,
         dampingRatio: CGFloat = 0.55,
         initialVelocity: CGFloat = 0.3,
         interItemDelay: TimeInterval = 0.1) {
        self.duration = duration
        self.dampingRatio = dampingRatio
        self.initialVelocity = initialVelocity
        self.interItemDelay = interItemDelay
    }
}
