//
//  Authorizable.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol Authorizable {
    func checkAuthorizationAndExecute(completion: @escaping SimpleHandler)
    func askAuthorizationAndExecute(completion: @escaping SimpleHandler)
}
