//
//  CacheUserService.swift
//  blagru
//
//  Created by Michael Artuerhof on 19.12.2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol UserDescProtocol {
    var id: String { get }
}

protocol CacheUserService {
    func userExistsWith(id: String) -> Bool
    func getUserWith(id: String) -> User?
    func create(user: User, completion: @escaping (() -> Void))
    func update(user: User, completion: @escaping (() -> Void))
    func deleteUserWith(id: String, completion: @escaping (() -> Void))
}
