//
//  AuthInfo.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct AuthInfo: Codable {
    let type: String
    let scope: String
    let login: String
    let password: String
    let clientId: String
    let clientSecret: String

    enum CodingKeys: String, CodingKey {
        case type = "grant_type"
        case scope
        case login
        case password
        case clientId = "client_id"
        case clientSecret = "client_secret"
    }
}
