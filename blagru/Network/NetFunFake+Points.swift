//
//  NetFunFake+Points.swift
//  blagru
//
//  Created by Michael Artuerhof on 11.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

/// fake service which generates points on map for testing purposes
class NetFunFake {
    private let distance: Double = 1500 // distance between points in meters
    private let count: Int

    init(count: Int) {
        self.count = count
    }

    fileprivate struct const {
        static let eR: Double = 6371e3 // earth's radius in meters
    }
}

extension NetFunFake: NetPointsService {
    private func pointNear(lat1: Double, lon1: Double, brng: Double, d: Double) -> (Double, Double) {
        // copyied from: https://www.movable-type.co.uk/scripts/latlong.html
        let φ1 = lat1.toRadians
        let λ1 = lon1.toRadians
        let φ2 = asin(sin(φ1) * cos(d/const.eR) +
            cos(φ1) * sin(d / const.eR) * cos(brng))
        let λ2 = λ1 + atan2(sin(brng) * sin(d / const.eR) * cos(φ1),
                              cos(d / const.eR) - sin(φ1) * sin(φ2))
        return (φ2.toDegrees, λ2.toDegrees)
    }

    func pointsIn(catId: String,
                  lat0: Double,
                  lon0: Double,
                  lat1: Double,
                  lon1: Double,
                  completion: @escaping (([Ev]?, NSError?) -> Void)) {
        var events = [Ev]()
        for i in 0..<count {
            func genPoint(lat: Double, lon: Double) -> (Double, Double) {
                let d = Double.random(in: 200..<distance)
                let bearing = Double.random(in: -180...180).toRadians
                return pointNear(lat1: lat, lon1: lon, brng: bearing, d: d)
            }
            let pt0 = genPoint(lat: lat0, lon: lon0)
            let pt1 = genPoint(lat: lat1, lon: lon1)
            let cat = EvCategoryDesc(id: "xxTAXIxx")
            let usr = UserDesc(id: "1")
            let kMax = Int.random(in: 2...30)
            let parts = (1...kMax).map { UserDesc(id: "\($0)") }
            let ev = Ev(
                id: UUID().uuidString,
                lat0: pt0.0,
                lon0: pt0.1,
                lat1: pt1.0,
                lon1: pt1.1,
                comment: "Point \(i)",
                category: cat,
                owner: usr,
                participants: parts)
            events.append(ev)
        }
        completion(events, nil)
    }
}
