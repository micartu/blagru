//
//  SingleSectionPresenter.swift
//  blagru
//
//  Created by Michael Artuerhof on 29/07/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol SingleSectionInteractorInput: class {
    func loadModels(with theme: Theme)
}

protocol SingleSectionInteractorOutput: class {
    func fetched(models: [CellAnyModel])
}

open class SingleSectionPresenter:
    CommonViewModel,
    SingleSectionInteractorOutput {

    // data source
    weak var source: SingleSectionInteractorInput?

    // MARK: SingleSectionInteractorOutput

    func fetched(models: [CellAnyModel]) {
        self.models = models
        busy.set(false)
        updateData.set(true)
    }

    // MARK: CommonViewModel

    func getContents() {
        source?.loadModels(with: theme)
    }

    func refreshContents() {
        getContents()
    }

    func selected(indexPath: IndexPath) {
    }

    func model(for indexPath: IndexPath) -> Any {
        return models[indexPath.row]
    }

    func modelsCount(for section: Int) -> Int {
        return models.count
    }

    func title(for section: Int) -> String {
        return ""
    }

    func sectionsCount() -> Int {
        return 1
    }

    func clean() {}

    // MARK: useful functions
    internal func findModelWith(id: String) -> (CellAnyModel?, Int) {
        for i in models.indices {
            let m = models[i]
            if m.id == id {
                return (m, i)
            }
        }
        return (nil, -1)
    }

    // MARK: members of CommonViewModel protocol
    var theme: Theme! {
        didSet {
            getContents()
        }
    }
    var updateItems = Channel<IIndexPathCount?> (value: nil)
    var updateData = Channel<Bool> (value: false)
    var scrollTo = Channel<IMovePath?> (value: nil)
    var removeKeyboard = Channel<Bool> (value: false)
    var busy = Channel<Bool> (value: false)
    var batchUpdate = Channel<Bool?> (value: nil)
    var make = Channel<CellChangeType?> (value: nil)

    // MARK: other members
    internal var models = [CellAnyModel]()
}
