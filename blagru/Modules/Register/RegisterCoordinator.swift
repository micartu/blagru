//
//  RegisterCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class RegisterCoordinator: BaseCoordinator {
    override func start() {
        let vc = RegisterViewController.create()
        let viewModel = RegisterViewModel(net: ServicesAssembler.inject(),
                                          secret: ServicesAssembler.inject(),
                                          cache: ServicesAssembler.inject())
        viewModel.dispatcher = self
        vc.title = "Register".localized
        vc.vm = viewModel
        viewModel.view = vc
        cntr = vc
    }
}

extension RegisterCoordinator: RegisterDispatcherProtocol {
    func authorized() {
        broadcast(result: Access.granted)
    }
}
