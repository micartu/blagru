//
//  CacheTests.swift
//  blagruTests
//
//  Created by Michael Artuerhof on 15.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import XCTest
@testable import blagru

class CacheTests: XCTestCase {
    private let secrets = MockSecrets()
    var cache: Cache!
    let kPhone = "12345"
    let kName = "john"
    let kMail = "john@mail.de"

    override func setUp() {
        cache = Cache(secret: secrets)
    }

    func testUserCreation() {
        let exp = self.expectation(description: "user creation")
        createUser { [unowned self] in
            XCTAssertTrue(self.cache.userExistsWith(id: self.kPhone))
            let u = self.cache.getUserWith(id: self.kPhone)
            XCTAssert(u?.name == self.kName)
            XCTAssert(u?.login == self.kPhone)
            self.cache.deleteUserWith(id: self.kPhone) {
                XCTAssertFalse(self.cache.userExistsWith(id: self.kPhone))
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testUserUpdate() {
        let exp = self.expectation(description: "update user")
        createUser { [unowned self] in
            let kUpdatedName = "test"
            let u = User(
                id: self.kPhone,
                login: self.kPhone,
                name: kUpdatedName,
                email: self.kMail,
                avatar: "",
                password: "")
            self.cache.update(user: u) {
                let u2 = self.cache.getUserWith(id: self.kPhone)
                XCTAssert(u2?.name == kUpdatedName)
                self.cache.deleteUserWith(id: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testCategoryCreation() {
        let exp = self.expectation(description: "create category")
        let cats = (1...10).map { EvCategory(id: "\($0)", name: "\($0)", icon: "") }
        cache.create(categories: cats) {
            let c = 5
            let rcats = self.cache.latestUsedCategories(maxItems: c)
            XCTAssert(rcats.count == c, "but it was: \(rcats.count)")
            XCTAssert(rcats.first?.cat_id == "10", "but it was: \(rcats)")
            self.cache.deleteAllCategories {
                let rcats = self.cache.latestUsedCategories(maxItems: c)
                XCTAssert(rcats.count == 0, "but it was: \(rcats.count)")
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testPointCreation() {
        let exp = self.expectation(description: "create points")
        let pts: [EvPoint] = (1...10).map { EvPoint(
            ind: Int64("\($0)".toInt),
            lon: Double($0),
            lat: Double($0),
            typeA: false,
            state: .hightlighted,
            connectedInd: nil,
            event: nil)
        }
        cache.create(points: pts) {
            let rpts = self.cache.allPoints()
            XCTAssert(rpts.count == pts.count, "but it was: \(rpts)")
            XCTAssertTrue(rpts.first?.state == PointState.hightlighted,
                          "but it was: \(String(describing: rpts.first?.state))")
            XCTAssertTrue(rpts.first?.connectedInd == nil)
            self.cache.deleteAllPoints {
            let rpts = self.cache.allPoints()
                XCTAssert(rpts.count == 0, "but it was: \(rpts)")
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testPointWithEventCreation() {
        let exp = self.expectation(description: "create points and events")
        let pts: [EvPoint] = (1...10).map { EvPoint(
            ind: Int64("\($0)".toInt),
            lon: Double($0),
            lat: Double($0),
            typeA: false,
            state: .hightlighted,
            connectedInd: nil,
            event: nil)
        }
        let cat = EvCategoryDesc(id: "$0")
        let evs = pts.map {
            Ev(id: "ev:\($0.ind)",
               lat0: 0,
               lon0: 0,
               lat1: $0.lat,
               lon1: $0.lon,
               comment: "",
               category: cat,
               owner: UserDesc(id: ""),
               participants: [])
        }
        cache.create(points: pts) {
            var npts = [EvPoint]()
            for (i, p) in pts.enumerated() {
                var m = p
                m.event = EvDesc(evId: evs[i].id)
                npts.append(m)
            }
            self.cache.update(points: npts) {
                self.cache.update(events: evs) {
                    let rpts = self.cache.allPoints()
                    XCTAssert(rpts.count == pts.count, "but it was: \(rpts)")
                    XCTAssertTrue(rpts.first?.state == PointState.hightlighted,
                                  "but it was: \(String(describing: rpts.first?.state))")
                    XCTAssertTrue(rpts.first?.event != nil)
                    XCTAssertTrue(rpts.first?.connectedInd == nil)
                    self.cache.deleteAllPoints {
                        self.cache.deleteAllEvents {
                            let rpts = self.cache.allPoints()
                            XCTAssert(rpts.count == 0, "but it was: \(rpts)")
                            exp.fulfill()
                        }
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testEventsCreation() {
        let exp = self.expectation(description: "create events")
        let kMin = 1
        let kMax = 10
        let cats = (kMin...kMax).map { EvCategoryDesc(id: "\($0)") }
        let owners = (kMin...kMax).map { UserDesc(id: "\($0)") }
        let evs = (kMin...kMax).map { Ev(
            id: "\($0)",
            lat0: 0,
            lon0: 0,
            lat1: Double($0),
            lon1: Double($0),
            comment: "\($0)",
            category: cats[$0 - 1],
            owner: owners[$0 - 1],
            participants: owners)
        }
        cache.create(events: evs) {
            let c = 5
            let rcats = self.cache.latestUsedCategories(maxItems: c)
            XCTAssert(rcats.count == c, "but it was: \(rcats.count)")
            XCTAssert(rcats.first?.cat_id == "10", "but it was: \(rcats)")
            let g = DispatchGroup()
            owners.forEach { u in
                g.enter()
                self.cache.deleteUserWith(id: u.id) {
                    g.leave()
                }
            }
            self.cache.deleteAllCategories {
                self.cache.deleteAllEvents {
                    let rcats = self.cache.latestUsedCategories(maxItems: c)
                    XCTAssert(rcats.count == 0, "but it was: \(rcats.count)")
                    g.notify(queue: .main) {
                        XCTAssertFalse(self.cache.userExistsWith(id: owners.first?.id ?? ""))
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testEventsCreationWithDifferentParticipants() {
        let exp = self.expectation(description: "create events with participants")
        let owner = UserDesc(id: kPhone)
        let cat = EvCategoryDesc(id: "0")
        let part1 = (1...3).map { UserDesc(id: "\($0)") }
        let part2 = (2...10).map { UserDesc(id: "\($0)") }
        let ev1 =  Ev(
            id: "1",
            lat0: 0,
            lon0: 0,
            lat1: 2,
            lon1: 2,
            comment: "1",
            category: cat,
            owner: owner,
            participants: part1)
        var ev2 =  Ev(
            id: "2",
            lat0: 0,
            lon0: 0,
            lat1: 2,
            lon1: 2,
            comment: "2",
            category: cat,
            owner: owner,
            participants: part2)
        cache.create(events: [ev1, ev2]) {
            ev2.participants = part1
            self.cache.update(events: [ev2]) {
                let rev2 = self.cache.EvWith(id: ev2.id)
                XCTAssertTrue(rev2?.participants.count == part1.count)
                let usr = self.cache.getUserWith(id: self.kPhone)
                XCTAssertTrue(usr?.id == self.kPhone)
                // clean up
                let g = DispatchGroup()
                var owners = [owner]
                [part1, part2].forEach {
                    owners.append(contentsOf: $0)
                }
                owners.forEach { u in
                    g.enter()
                    self.cache.deleteUserWith(id: u.id) {
                        g.leave()
                    }
                }
                self.cache.deleteAllCategories {
                    self.cache.deleteAllEvents {
                        let rcats = self.cache.latestUsedCategories(maxItems: 10)
                        XCTAssert(rcats.count == 0, "but it was: \(rcats.count)")
                        g.notify(queue: .main) {
                            for u in owners {
                                XCTAssertFalse(self.cache.userExistsWith(id: u.id))
                            }
                            exp.fulfill()
                        }
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testFavoritesCreation() {
        let exp = self.expectation(description: "create events in favorites")
        let kMin = 1
        let kMax = 10
        let cats = (kMin...kMax).map { EvCategoryDesc(id: "\($0)") }
        let owners = (kMin...kMax).map { UserDesc(id: "\($0)") }
        let evs = (kMin...kMax).map { Ev(
            id: "\($0)",
            lat0: 0,
            lon0: 0,
            lat1: Double($0),
            lon1: Double($0),
            comment: "\($0)",
            category: cats[$0 - 1],
            owner: owners[$0 - 1],
            participants: owners)
        }
        createUser {
            self.cache.addToFavorites(events: evs) {
                let favs = self.cache.userFavorites()
                XCTAssert(favs.count == evs.count, "but it was: \(favs)")
                let g = DispatchGroup()
                [owners, [UserDesc(id: self.kPhone)]].flatMap { $0 }
                    .forEach {
                        g.enter()
                        self.cache.deleteUserWith(id: $0.id) {
                            g.leave()
                        }
                    }
                self.cache.deleteAllCategories {
                    self.cache.deleteAllEvents {
                        let favs = self.cache.userFavorites()
                        XCTAssert(favs.count == 0, "but it was: \(favs)")
                        let rcats = self.cache.latestUsedCategories(maxItems: 5)
                        XCTAssert(rcats.count == 0, "but it was: \(rcats.count)")
                        g.notify(queue: .main) {
                            XCTAssertFalse(self.cache.userExistsWith(id: owners.first?.id ?? ""))
                            exp.fulfill()
                        }
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    private func createUser(completion: @escaping (() -> Void)) {
        let u = User(
            id: kPhone,
            login: kPhone,
            name: kName,
            email: kMail,
            avatar: "",
            password: "")
        cache.create(user: u) {
            self.secrets.userID = self.kPhone
            completion()
        }
    }
}
