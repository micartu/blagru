//
//  SplashViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol SplashDispatcherProtocol: class {
    func loadIsOver()
    func transitionSetup()
}

protocol SplashViewModelProtocol: ViewReadinessProtocol {
    func setupTransition()
}

final class SplashViewModel {
    weak var dispatcher: SplashDispatcherProtocol?
    init(dispatcher: SplashDispatcherProtocol?) {
        self.dispatcher = dispatcher
    }
}

extension SplashViewModel: SplashViewModelProtocol {
    func viewIsReady() {
        delay(1) { [weak self] in // load something later if needed:
            self?.dispatcher?.loadIsOver()
        }
    }

    func setupTransition() {
        dispatcher?.transitionSetup()
    }
}
