//
//  FavSyncer.swift
//  blagru
//
//  Created by Michael Artuerhof on 02.07.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

final class FavSyncer {
    fileprivate let net: NetFavService
    fileprivate let cache: CacheEvAndPointService
    fileprivate let syncer: GeneralSynchronizer
    fileprivate let queue: DispatchQueue

    init(net: NetFavService,
         storage: KeyValueStorageService,
         cache: CacheEvAndPointService) {
        self.net = net
        self.cache = cache
        queue = DispatchQueue(label: "FavSyncer", qos: .utility)
        let d = storage.favDictStatus.value ?? StatusDictType()
        let items: [Ev] = d.count > 0 ? cache.userFavorites() : []
        syncer = GeneralSynchronizer(statusItems: items,
                                     statusDict: d,
                                     deb: ConsoleDebugger(name: "favs")) { (_, dict) in
                                        storage.favDictStatus.value = dict
        }
    }
}

extension FavSyncer: FavSyncerService {
    func syncFavorites(completion: @escaping ((NSError?) -> Void)) {
        net.fetchFavorites { [weak self] (nfavs, error) in
            if let e = error {
                completion(e)
            } else if let netfav = nfavs {
                guard let `self` = self else { completion(nil); return }
                let netg = FavNetSyncer()
                let favs = self.cache.userFavorites()
                let inCloud = ItemsToSync(items: netfav,
                                          interactor: netg)
                let locally = ItemsToSync(items: favs,
                                          interactor: self)
                self.syncer.sync(A: inCloud, B: locally) {
                    self.queue.async {
                        let g = DispatchGroup()
                        let lock = NSRecursiveLock()
                        var e: NSError?
                        if netg.toAdd.count > 0 {
                            for i in netg.toAdd {
                                g.enter()
                                self.net.addToFavorites(eventID: i.itemID) { err in
                                    if err != nil {
                                        lock.lock()
                                        e = err
                                        lock.unlock()
                                    }
                                    g.leave()
                                }
                            }
                        }
                        if netg.toRemove.count > 0 {
                            for i in netg.toRemove {
                                g.enter()
                                self.net.removeFromFavorites(eventID: i.itemID) { err in
                                    if err != nil {
                                        lock.lock()
                                        e = err
                                        lock.unlock()
                                    }
                                    g.leave()
                                }
                            }
                        }
                        g.notify(queue: .main) {
                            completion(e)
                        }
                    }
                }
            }
        }
    }
}

final fileprivate class FavNetSyncer: ItemsToSyncActionsProtocol {
    var toAdd = [SyncItemProtocol]()
    var toRemove = [SyncItemProtocol]()

    func insert(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        toAdd.append(item)
        completion()
    }

    func replace(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        if !toAdd.contains(where: { $0.itemID == item.itemID }) {
            toAdd.append(item)
        }
        completion()
    }

    func remove(_ item: SyncItemProtocol, completion: @escaping (() -> Void)) {
        toRemove.append(item)
        completion()
    }
}

extension FavSyncer: ItemsToSyncActionsProtocol {
    func insert(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        replace(item, at: index, completion: completion)
    }

    func replace(_ item: SyncItemProtocol, at index: Int, completion: @escaping (() -> Void)) {
        if let c = item as? Ev {
            cache.addToFavorites(events: [c], completion: completion)
        } else {
            completion()
        }
    }

    func remove(_ item: SyncItemProtocol, completion: @escaping (() -> Void)) {
        if let c = item as? Ev {
            cache.delete(events: [c], completion: completion)
        } else {
            completion()
        }
    }
}

// MARK: - K-V-Contrainer definitions for needed fields
fileprivate extension KeyValueStorageService {
    var favDictStatus: KeyValueContainer<StatusDictType> {
        makeContainer()
    }
}
