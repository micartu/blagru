//
//  UIImage+Shadow.swift
//  blagru
//
//  Created by Michael Artuerhof on 28.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow(color: UIColor = .lightGray,
                   opacity: Float = 0.5,
                   offset: CGSize = CGSize(width: 5, height: 5),
                   radius: CGFloat = 3.0) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
    }

    func shadow(radius: CGFloat, color: UIColor) {
        self.layer.cornerRadius = radius
        self.layer.shadowRadius = radius
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.masksToBounds = false
        self.layer.drawsAsynchronously = true
    }
}
