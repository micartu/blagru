//
//  NetFun+Event.swift
//  blagru
//
//  Created by Michael Artuerhof on 17.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetEventService {
    func connectedUsersForEvent(id: String,
                                completion: @escaping (([User]?, NSError?) -> Void)) {
    }

    func allCategories(completion: @escaping (([EvCategory]?, NSError?) -> Void)) {
        post(url: "/event_types/get/", data: Data(), success: { (ans: NSDictionary) in
            if let evs = ans["event_types"] as? [NSDictionary] {
                let categories = evs.map { parseCategory(from: $0) }
                completion(categories, nil)
            } else {
                let e = NSError(domain: Const.domain,
                                code: -1,
                                userInfo: [NSLocalizedDescriptionKey: "no 'event_types' field in the answer".localized])
                completion(nil, e)
            }
        }, failure: { e in
            completion(nil, e)
        })
    }

    func create(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(event, nil)
    }

    func update(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(event, nil)
    }

    func join(eventId: String, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(nil, nil)
    }

    func delete(event: Ev, completion: @escaping ((Ev?, NSError?) -> Void)) {
        // TODO: add me
        completion(event, nil)
    }
}
