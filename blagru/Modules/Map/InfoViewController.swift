//
//  InfoViewController.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class InfoViewController: BaseViewController {
    var vm: InfoViewModelProtocol!

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var lblDescr: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnJoin: ScaledImageButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        buttons().forEach {
            $0.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        }
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        let clr = theme.inactiveColor.withAlphaComponent(0.8).cgColor
        container.layer.cornerRadius = 8
        container.layer.borderColor = clr
        container.layer.borderWidth = 1
        container.layer.backgroundColor = clr
        view.backgroundColor = .clear
        lblDescr.font = theme.regularFont.withSize(theme.sz.middle4)
    }

    func buttons() -> [UIControl] {
        return [
            btnJoin,
        ]
    }

    // MARK: - Actions

    @objc fileprivate func buttonAction(_ sender: UIView) {
        let action: (() -> Void)?
        switch sender {
        case btnJoin:
            action = { [weak self] in
                self?.vm.actionJoin()
            }
        default:
            action = nil
        }
        if let a = action {
            applyTouchAnimation(sender, completion: a)
        }
    }
}

// MARK: - InfoViewProtocol
extension InfoViewController: InfoViewProtocol {
    func showInfoMsg(_ msg: String) {
        lblDescr.text = msg
    }

    func makeStackView(hidden: Bool,
                       animated: Bool,
                       completion: (() -> Void)? = nil) {
        func makeIT() {
            let tr: CGAffineTransform
            if hidden {
                tr = CGAffineTransform(translationX: 300, y: 0)
            } else {
                tr = .identity
            }
            stackView.transform = tr
            lblDescr.alpha = hidden ? 1 : 0
        }
        if animated {
            UIView.animate(withDuration: 0.3, delay: 0, animations: {
                makeIT()
            }, completion: { _ in
                completion?()
            })
        } else {
            makeIT()
            completion?()
        }
    }
}
