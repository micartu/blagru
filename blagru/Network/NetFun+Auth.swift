//
//  NetFun+Auth.swift
//  blagru
//
//  Created by Michael Artuerhof on 08.07.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetAuthService {
    func auth(login: String,
              password: String,
              success: ((NSDictionary) -> Void)?,
              failure: ((NSError) -> Void)?) {
        netmanager.auth(login: login,
                        password: password,
                        success: success,
                        failure: failure)
    }

    func socialAuth(type: String,
                    token: String,
                    success: ((NSDictionary) -> Void)?,
                    failure: ((NSError) -> Void)?) {
        netmanager.socialAuth(type: type,
                              token: token,
                              success: success,
                              failure: failure)
    }

    func register(login: String,
                  name: String,
                  email: String,
                  password: String,
                  success: ((NSDictionary) -> Void)?,
                  failure: ((NSError) -> Void)?) {
        netmanager.register(login: login,
                            name: name,
                            email: email,
                            password: password,
                            success: success,
                            failure: failure)
    }

    func logout(success: (() -> Void)?,
                failure: ((NSError) -> Void)?) {
        netmanager.logout(success: success, failure: failure)
    }
}
