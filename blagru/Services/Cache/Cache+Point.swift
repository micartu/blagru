//
//  Cache+Point.swift
//  blagru
//
//  Created by Michael Artuerhof on 16.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CachePointService {
    func pointExistsWith(ind: Int64) -> Bool {
        return existsEntityWithPredicate(getPointPredicate(ind: ind),
                                         EntityType: CEvPoint.self,
                                         in: getContext())
    }

    func create(points: [EvPoint],
                completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.createOrUpdatePoints(points, in: context)
        }, completion: completion)
    }

    func update(points: [EvPoint], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.createOrUpdatePoints(points, in: context)
        }, completion: completion)
    }

    func pointWith(ind: Int64) -> EvPoint? {
        if let ci = getCEvPointWithID(ind,
                                    in: getContext()) {
            return convert(point: ci)
        }
        return nil
    }

    func allPoints() -> [EvPoint] {
        if let pts = CEvPoint.mr_findAllSorted(
            by: "ind",
            ascending: true,
            in: getContext()) as? [CEvPoint] {
            return pts.map { convert(point: $0) }
        }
        return []
    }

    func delete(points: [EvPointDescProtocol],
                completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            for i in points {
                self.deletePointWithID(i.ind, in: context)
            }
        }, completion: completion)
    }

    func deleteAllPoints(completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let cmnts = CEvPoint.mr_findAll(in: context) as? [CEvPoint] {
                    for c in cmnts {
                        self.deleteCEvPoint(c, in: context)
                    }
                }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    internal func getPointPredicate(ind: Int64) -> NSPredicate {
        let idPred = NSPredicate(format: "ind == %ld", ind)
        return NSCompoundPredicate(andPredicateWithSubpredicates: [idPred])
    }

    internal func getCEvPointWithID(_ id: Int64, in context: NSManagedObjectContext) -> CEvPoint? {
        return CEvPoint.mr_findFirst(
            with: getPointPredicate(ind: id),
            in: context)
    }

    @discardableResult
    internal func createCEvPoint(_ c: EvPoint,
                                  in context: NSManagedObjectContext) -> CEvPoint {
        let cc = CEvPoint.mr_createEntity(in: context)!
        updatePointContents(cc, from: c, in: context)
        return cc
    }

    internal func createOrUpdatePoints(_ points: [EvPoint],
                                       in context: NSManagedObjectContext) {
        for m in points {
            let ci: CEvPoint
            if let _ci = getCEvPointWithID(m.ind, in: context) {
                ci = _ci
                updatePointContents(ci, from: m, in: context)
            } else {
                ci = createCEvPoint(m, in: context)
            }
        }
    }

    internal func updatePointContents(_ cm: CEvPoint,
                                      from m: EvPoint,
                                      in context: NSManagedObjectContext) {
        cm.ind = m.ind
        cm.lon = m.lon
        cm.lat = m.lat
        cm.typeA = m.typeA
        cm.state = m.state.rawValue
        cm.connectedInd = m.connectedInd ?? -1
        // check event
        if let e = m.event {
            let ce: CEv
            if let _ce = getCEvWithID(e.evId, in: context) {
                ce = _ce
            } else {
                ce = CEv.mr_createEntity(in: context)!
                ce.ev_id = e.evId
            }
            ce.addToPoints(cm)
            cm.event = ce
        } else {
            cm.event = nil
        }
    }

    internal func convert(point c: CEvPoint) -> EvPoint {
        let ev = (c.event != nil) ?
            EvDesc(evId: c.event?.ev_id ?? "") : nil
        return EvPoint(
            ind: c.ind,
            lon: c.lon,
            lat: c.lat,
            typeA: c.typeA,
            state: PointState(rawValue: c.state ?? "") ?? .idle,
            connectedInd: c.connectedInd >= 0 ? c.connectedInd : nil,
            event: ev)
    }

    internal func deleteCEvPoint(_ c: CEvPoint, in context: NSManagedObjectContext) {
        c.mr_deleteEntity(in: context)
    }

    internal func deletePointWithID(_ id: Int64, in context: NSManagedObjectContext) {
        if let c = getCEvPointWithID(id, in: context) {
            deleteCEvPoint(c, in: context)
        }
    }
}
