//
//  MockSecrets.swift
//  blagruTests
//
//  Created by Michael Artuerhof on 29.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
@testable import blagru

class MockSecrets: SecretService {
    var isUserGenerated: Bool = false
    var isSocialAuth: Bool = false
    var socialAuthType: String = ""
    var socialAuthToken: String = ""
    var socialAuthExtra: [String : String] = [:]
    var socialAuthExpire: Date? = nil
    var restore: String = ""
    var userID: String = ""
    var ulogin: String = ""
    var currentUserId: Int = -1
    var authorized: Bool = false
    var isEncrypted: Bool = false
    var isAuthCreated: Bool = false
    var pwd: String = ""
    var magic: String = ""
    var session: String = ""
    var user: String = ""
    var password: String = ""
    var cardId: String = ""
    var uid: String = ""
    func erase() {
    }
}
