//
//  CacheEvService.swift
//  blagru
//
//  Created by Michael Artuerhof on 15.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol EvDescProtocol {
    var evId: String { get }
}

struct EvDesc: EvDescProtocol {
    let evId: String
}

protocol CacheEvService {
    func EvExistsWith(id: String) -> Bool
    func EvWith(id: String) -> Ev?
    func userFavorites() -> [Ev]
    func create(events: [Ev],
                completion: @escaping (() -> Void))
    func addToFavorites(events: [Ev],
                        completion: @escaping (() -> Void))
    func update(events: [Ev],
                completion: @escaping (() -> Void))
    func eventsForCurrentUser() -> [Ev]
    func delete(events: [EvDescProtocol], completion: @escaping (() -> Void))
    func deleteAllEventsForCurrentUser(completion: @escaping (() -> Void))
    func deleteAllEvents(completion: @escaping (() -> Void))
}
