//
//  TitledViewCell+Combo.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import DropDown

extension TitledViewCell {
    func setupComboBoxFrom(descr: [String:Any], theme: Theme) {
        type = .combo
        let selected: Int
        if let s = descr[const.selected] as? Int {
            selected = s
        } else {
            selected = 0
        }
        // set theme in order to make baseclass work properly:
        self.theme = theme
        imgTopOffset = 0
        labelXOffset = 10
        count = 0
        let title: String
        if let items = descr[const.fields] as? [String], selected < items.count {
            title = items[selected]
            dropDown.dataSource = items
        } else {
            dropDown.dataSource = []
            title = "?"
        }
        curSelection = selected
        initialize()
        add(image: "", width: 0, height: 0, text: title)
        if let last = lastConnected {
            let b = UIButton()
            b.addTarget(self,
                        action: #selector(itemTapped(_:)),
                        for: .touchUpInside)
            last.attachConnectedToBorders(view: b)
            last.addBorders(width: 2, color: theme.secondaryTextColor, cornerRadius: 5)
            last.clipsToBounds = true
            last.layer.borderWidth = 1
            last.layer.borderColor = theme.borderColor.cgColor
            last.layer.backgroundColor = theme.inactiveColor.cgColor
            last.layer.cornerRadius = 5
            for v in last.subviews {
                if let label = v as? UILabel {
                    label.textColor = theme.mainTextColor
                    label.font = theme.regularFont.withSize(theme.sz.middle4)
                }
            }
            let imv = UIImageView(image: UIImage(named: "arrow_down"))
            imv.tintColor = theme.separatorColor
            imv.translatesAutoresizingMaskIntoConstraints = false
            last.addSubview(imv)
            last.addConstraints([imv.trailingAnchor.constraint(equalTo: last.trailingAnchor, constant: -15),
                                 imv.centerYAnchor.constraint(equalTo: last.centerYAnchor)])
            setupDropDown(theme: theme, attachTo: last)
        }
        finishSetup()
    }

    private func setupDropDown(theme: Theme, attachTo: UIView) {
        DropDown.setupDefaultAppearance()
        dropDown.cellNib = UINib(nibName: "SelectionCell", bundle: nil)
        dropDown.customCellConfiguration = { [weak self] (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? SelectionCell else { return }
            guard let `self` = self else { return }
            cell.lblCaption.text = item
            if index == self.curSelection {
                cell.borderView.borderWidth = 1
                cell.borderView.cornerRadius = 4
                cell.borderView.customBorderColor = theme.inactiveColor
                cell.borderView.backgroundColor = theme.inactiveColor
            } else {
                cell.borderView.borderWidth = 0
                cell.borderView.customBorderColor = theme.emptyBackColor
                cell.borderView.backgroundColor = theme.emptyBackColor
            }
        }
        dropDown.anchorView = attachTo
        dropDown.cornerRadius = 8
        dropDown.shadowOffset = CGSize(width: 0, height: 4)
        dropDown.separatorColor = .clear
        dropDown.backgroundColor = theme.emptyBackColor
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.curSelection = index
            self?.radioDelegate?.changed(selected: index, id: self?.id ?? "")
        }
    }
}
