//
//  String+trunc.swift
//  blagru
//
//  Created by Michael Artuerhof on 07.02.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension String {
  func trunc(length: Int, trailing: String = "") -> String {
    return (self.count > length) ? self.prefix(length) + trailing : self
  }
}
