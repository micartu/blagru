//
//  Ev+Sync.swift
//  blagru
//
//  Created by Michael Artuerhof on 02.07.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension Ev: SyncItemProtocol {
    var itemID: String {
        return id
    }

    var tag: String {
        return comment
    }
}
