//
//  ButtonAnimationConfiguration.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

struct ButtonAnimationConfiguration {
    /// Initializes and returns a newly allocated button animation configuration object with the specified style.
    ///
    /// - Parameter style: The animation style.
    ///
    /// - Returns: An initialized button animation configuration object.
    ///
    init(withStyle style: ButtonAnimationStyle) {
        self.style = style
    }

    /// Button animation style
    ///
    enum ButtonAnimationStyle: Int {
        /// Rotate button image to given angle.
        ///
        case rotation

        /// Transition to given image.
        ///
        case transition

        /// user's animation
        ///
        case custom
    }

    /// Button animation style
    let style: ButtonAnimationStyle

    /// The angle in radian the button will rotate to when opening.
    ///
    /// - Remark: Is ignored for style `.rotation`
    ///
    var angle: CGFloat = 0

    /// The image button will transition to when opening.
    ///
    /// - Remark: Is ignored for style `.transition`
    ///
    var image: UIImage?

    /// customized animations for button
    ///
    var customButtonAnimations: ((FloatingActionButtonState) -> Void)?

    /// Animation settings for opening animation.
    /// Default values are:
    ///   - `duration = 0.3`
    ///   - `dampingRatio = 0.55`
    ///   - `initialVelocity = 0.3`
    ///
    var opening = AnimationSettings(duration: 0.3, dampingRatio: 0.55, initialVelocity: 0.3)

    /// Animation settings for closing animation.
    /// Default values are:
    ///   - `duration = 0.3`
    ///   - `dampingRatio = 0.6`
    ///   - `initialVelocity = 0.8`
    ///
    var closing = AnimationSettings(duration: 0.3, dampingRatio: 0.6, initialVelocity: 0.8)
}

extension ButtonAnimationConfiguration {
    /// Returns a button animation configuration that rotates the button image by given angle.
    ///
    /// - Parameter angle: The angle in radian the button will rotate to when opening.
    ///
    /// - Returns: A button animation configuration object.
    ///
    static func rotation(toAngle angle: CGFloat = -.pi / 4) -> ButtonAnimationConfiguration {
        var configuration = ButtonAnimationConfiguration(withStyle: .rotation)
        configuration.angle = angle
        return configuration
    }

    /// Returns a button animation configuration that transitions to a given image.
    ///
    /// - Parameter image: The image button will transition to when opening.
    ///
    /// - Returns: A button animation configuration object.
    ///
    static func transition(toImage image: UIImage) -> ButtonAnimationConfiguration {
        var configuration = ButtonAnimationConfiguration(withStyle: .transition)
        configuration.image = image
        return configuration
    }

    /// Returns a button animation configuration that makes a custom action
    ///
    /// - Parameter handler: sequence of animation actions
    ///
    /// - Returns: A button animation configuration object.
    ///
    static func customAnimation(with handler: @escaping ((FloatingActionButtonState) -> Void)) -> ButtonAnimationConfiguration {
        var configuration = ButtonAnimationConfiguration(withStyle: .custom)
        configuration.customButtonAnimations = handler
        return configuration
    }
}
