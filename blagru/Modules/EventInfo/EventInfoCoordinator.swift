//
//  EventInfoCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 13/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

final class EventInfoCoordinator: AuthorizableBaseCoordinator {
    init(eventId: String, secret: SecretService, parent: BaseCoordinator?) {
        self.eventId = eventId
        super.init(secret: secret, parent: parent)
    }
    
    convenience init(eventId: String, parent: BaseCoordinator?) {
        self.init(eventId: eventId,
                  secret: ServicesAssembler.inject(),
                  parent: parent)
    }

	override func start() {
		let vc = BaseTableViewController()
		vc.manualBuilding = true
		vc.viewBuilder = {
			let tv = UITableView(frame: .zero, style: .grouped)
            tv.separatorStyle = .none
			return (tv, tv, true)
		}
        vc.themeApplier = { (theme, vc) in
            vc.tableview.backgroundColor = theme.mainBackColor
        }
		vc.title = "EventInfo Title".localized
		vc.initializer = { tv in
            let cells = [
                EventCaptionCell.self,
                EventInfoCell.self,
            ]
            for c in cells {
                let identifier = String(describing: c)
                tv.register(UINib(nibName: identifier, bundle: nil),
                            forCellReuseIdentifier: identifier)
            }
		}
        vc.headerViewGen = { (section: Int) -> UIView? in
            let v = UIView()
            v.backgroundColor = .clear
            return v
        }
        vc.heightForHeaderSizeHook = { section in
            return 0
        }
        vc.heightForFooterSizeHook = { section in
            return 5
        }
        let viewModel = EventInfoViewModel(
            eventId: eventId,
            fetchCache: ServicesAssembler.inject(),
            evCache: ServicesAssembler.inject(),
            secret: ServicesAssembler.inject(),
            net: ServicesAssembler.inject(),
            imageLoader: ServicesAssembler.inject())
		viewModel.dispatcher = self
		viewModel.source = viewModel
		vc.viewModel = viewModel
		cntr = vc
	}

    fileprivate let eventId: String
}

// MARK: - EventInfoDispatcherProtocol
extension  EventInfoCoordinator: EventInfoDispatcherProtocol {
}
