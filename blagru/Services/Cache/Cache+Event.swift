//
//  Cache+Event.swift
//  blagru
//
//  Created by Michael Artuerhof on 15.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import MagicalRecord

extension Cache: CacheEvService {
    func EvExistsWith(id: String) -> Bool {
        return existsEntityWithPredicate(getEvPredicate(id: id),
                                         EntityType: CEv.self,
                                         in: getContext())
    }

    func EvWith(id: String) -> Ev? {
        if let ce = getCEvWithID(id, in: getContext()) {
            return convert(event: ce)
        }
        return nil
    }

    func userFavorites() -> [Ev] {
        let pred = getFavoritesPredicate(userID: secret.userID)
        if let evs = CEv.mr_findAllSorted(by: "start_date",
                                          ascending: false,
                                          with: pred,
                                          in: getContext()) as? [CEv] {
            return evs.map { convert(event: $0) }
        }
        return []
    }


    func create(events: [Ev], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.createOrUpdateEvs(events, in: context)
        }, completion: completion)
    }

    func addToFavorites(events: [Ev], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let cu = self.getCurrentCUser(in: context) {
                let cevs = self.createOrUpdateEvs(events,
                                                  returnThem: true,
                                                  in: context)
                cu.addToFavorite_evs(NSSet(array: cevs))
            }
        }, completion: completion)
    }

    func update(events: [Ev], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            self.createOrUpdateEvs(events, in: context)
        }, completion: completion)
    }

    func eventsForCurrentUser() -> [Ev] {
        if let cu = getCurrentCUser(in: getContext()) {
            if let cevs = cu.owning_events as? Set<CEv> {
                return cevs
                    .sorted(by: { ($0.ev_id ?? "") < ($1.ev_id ?? "") })
                    .map { convert(event: $0) }
            }
        }
        return []
    }

    func delete(events: [EvDescProtocol], completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            for e in events {
                if let ce = self.getCEvWithID(e.evId, in: context) {
                    self.deleteCEv(ce, in: context)
                }
            }
        }, completion: completion)
    }

    func deleteAllEventsForCurrentUser(completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let cu = self.getCurrentCUser(in: context) {
                if let cevs = cu.owning_events as? Set<CEv> {
                    for ce in cevs {
                        self.deleteCEv(ce, in: context)
                    }
                    cu.removeFromOwning_events(cevs as NSSet)
                }
            }
        }, completion: completion)
    }

    func deleteAllEvents(completion: @escaping (() -> Void)) {
        modify({ [unowned self] context in
            if let cevs = CEv.mr_findAll(in: context) as? [CEv] {
                for ce in cevs {
                    self.deleteCEv(ce, in: context)
                }
            }
        }, completion: completion)
    }

    // MARK: - Private/Internal

	 internal func getEvPredicate(id: String) -> NSPredicate {
        let idPred = getStrPredicate(key: "ev_id", value: id)
        return NSCompoundPredicate(andPredicateWithSubpredicates: [idPred])
    }

    internal func getFavoritesPredicate(userID: String) -> NSPredicate {
        return getStrPredicate(key: "usrFavorite.id", value: userID)
    }

    internal func getCEvWithID(_ id: String, in context: NSManagedObjectContext) -> CEv? {
        return CEv.mr_findFirst(
            with: getEvPredicate(id: id),
            in: context)
    }

    internal func getCEvWithID(_ id: String) -> CEv? {
        return getCEvWithID(id, in: getContext())
    }

    @discardableResult
    internal func createCEv(_ c: Ev, in context: NSManagedObjectContext) -> CEv {
        let cc = CEv.mr_createEntity(in: context)!
        updateEvContents(cc, from: c, in: context)
        return cc
    }

    @discardableResult
    internal func createOrUpdateEvs(_ events: [Ev],
                                    returnThem: Bool,
                                    in context: NSManagedObjectContext) -> [CEv] {
        var evs = [CEv]()
        for m in events {
            let cev: CEv
            if let ci = getCEvWithID(m.id, in: context) {
                updateEvContents(ci, from: m, in: context)
                cev = ci
            } else {
                cev = createCEv(m, in: context)
            }
            if returnThem {
                evs.append(cev)
            }
        }
        return evs
    }

    internal func createOrUpdateEvs(_ events: [Ev],
                                    in context: NSManagedObjectContext) {
        createOrUpdateEvs(events, returnThem: false, in: context)
    }

    internal func updateEvContents(_ cm: CEv,
                                   from m: Ev,
                                   in context: NSManagedObjectContext) {
        cm.ev_id = m.id
        cm.lat0 = m.lat0
        cm.lon0 = m.lon0
        cm.lat1 = m.lat1
        cm.lon1 = m.lon1
        cm.comment = m.comment

        cm.start_date = Date()

        // check category
        let ccat: CCategory
        let cid = m.category.cat_id
        if !categoryExistsWith(id: cid) {
            let empty = EvCategory(
                id: cid,
                name: "",
                icon: "")
            ccat = createCCategory(empty, in: context)
        } else {
            ccat = getCCategoryWithID(cid, in: context)!
        }
        cm.category = ccat

        // check owner
        if m.owner.id != cm.owner?.id {
            if !existsUserId(m.owner.id, in: context) {
                if let cu = CUser.mr_createEntity(in: context) {
                    cu.id = m.owner.id
                    cm.owner = cu
                    cu.addToOwning_events(cm)
                }
            } else if let cu = getCUserWithID(m.owner.id, in: context) {
                cm.owner = cu
                cu.addToOwning_events(cm)
            }
        }

        // check participants
        let sorters = [NSSortDescriptor(key: "login", ascending: true)]
        let left = (cm.participants?
            .sortedArray(using: sorters) as? [CUser])?
            .reduce("") { $0 + ($1.login ?? "") }
        let right = m.participants
            .sorted(by: { $0.id < $1.id })
            .reduce("") { $0 + $1.id }
        if left != right {
            // first remove all users on the left
            if let users = cm.participants as? Set<CUser> {
                for cu in users {
                    if cu.id != secret.userID && cu.owning_events?.count == 0 {
                        deleteCUser(cu, in: context)
                    }
                }
                cm.removeFromParticipants(users as NSSet)
            }
            // next add all users from the right side:
            for u in m.participants {
                let cu: CUser
                if let _cu = getCUserWithID(u.id, in: context) {
                    cu = _cu
                } else {
                    cu = CUser.mr_createEntity(in: context)!
                    cu.id = u.id
                }
                cm.addToParticipants(cu)
            }
        }
    }

    internal func convert(event e: CEv) -> Ev {
        let cat = EvCategoryDesc(id: e.category?.cat_id ?? "")
        let owner = UserDesc(id: e.owner?.id ?? "")
        let bodies = (e.participants as? Set<CUser>)?
            .map { UserDesc(id: $0.id ?? "") }
            .sorted(by: { $0.id > $1.id })
        return Ev(
            id: e.ev_id!,
            lat0: e.lat0,
            lon0: e.lon0,
            lat1: e.lat1,
            lon1: e.lon1,
            comment: e.comment ?? "",
            category: cat,
            owner: owner,
            participants: bodies ?? []
        )
    }

    internal func deleteCEv(_ c: CEv, in context: NSManagedObjectContext) {
        c.mr_deleteEntity(in: context)
    }

    internal func deleteEvWithID(_ id: String, in context: NSManagedObjectContext) {
        if let c = getCEvWithID(id, in: context) {
            deleteCEv(c, in: context)
        }
    }
}

// part of CacheFetchRequestsService
extension Cache {
    func fetchDataControllerForUserFavorites() -> FetchManagerProtocol {
        let context = NSManagedObjectContext.mr_default()
        let predicate = getFavoritesPredicate(userID: secret.userID)
        let request = CEv.mr_requestAllSorted(by: "start_date",
                                              ascending: false,
                                              with: predicate,
                                              in: context)
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = const.szBatch
        let frc = NSFetchedResultsController(fetchRequest: request,
                                             managedObjectContext: context,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil) as! NSFetchedResultsController<CEv>
        return FetchManager(fetchedResultsController: frc,
                            converter: self)
    }
}
