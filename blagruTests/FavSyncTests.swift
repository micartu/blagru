//
//  FavSyncTests.swift
//  blagruTests
//
//  Created by Michael Artuerhof on 02.07.2020.
//  Copyright © 2020 Michael Artuerhof. All rights reserved.
//

import XCTest
@testable import blagru

class FavSyncTests: XCTestCase {
    private let secrets = MockSecrets()
    var cache: Cache!
    let kPhone = "12345"
    let kName = "john"
    let kMail = "john@mail.de"

    override func setUpWithError() throws {
        cache = Cache(secret: secrets)
    }

    func testFavSyncSimple() throws {
        let exp = self.expectation(description: "fav sync simple test")
        let kMin = 1
        let kMax = 8
        let cats = (kMin...kMax).map { EvCategoryDesc(id: "\($0)") }
        let owners = (kMin...kMax).map { UserDesc(id: "\($0)") }
        let evs = (kMin...kMax).map { Ev(
            id: "\($0)",
            lat0: 0,
            lon0: 0,
            lat1: Double($0),
            lon1: Double($0),
            comment: "\($0)",
            category: cats[$0 - 1],
            owner: owners[$0 - 1],
            participants: owners)
        }
        let net = NetFavFake(
            favs: evs.filter { $0.id.toInt < kMax / 2},
            realFavs: evs
        )
        let sync: FavSyncerService = FavSyncer(
            net: net,
            storage: ServicesAssembler.inject(),
            cache: cache
        )
        createUser {
            let favs = evs.filter { $0.id.toInt >= kMax / 2 }
            self.cache.create(events: favs) {
                self.cache.addToFavorites(events: favs) {
                    let curFavs = self.cache.userFavorites()
                    XCTAssert(curFavs.count == favs.count)
                    sync.syncFavorites { er in
                        XCTAssert(er == nil)
                        let curFavs = self.cache.userFavorites()
                        XCTAssert(curFavs.count == evs.count)
                        XCTAssert(curFavs.count == net.favs.count)
                        self.cache.deleteUserWith(id: self.kPhone) {
                            self.cache.deleteAllEvents {
                                self.cache.deleteAllCategories {
                                    exp.fulfill()
                                }
                            }
                        }
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

   fileprivate func generateEvs(min: Int, max: Int) -> [Ev] {
        let cat = EvCategoryDesc(id: "1")
        let owner = UserDesc(id: "1")
        return (min...max).map { Ev(
            id: "\($0)",
            lat0: 0,
            lon0: 0,
            lat1: Double($0),
            lon1: Double($0),
            comment: "\($0)",
            category: cat,
            owner: owner,
            participants: [] ) }
    }

    func testFavSyncAndDeletionAfterChangesInNet() throws {
        let exp = self.expectation(description: "fav sync & deletion test")
        let favs = self.generateEvs(min: 1, max: 2)
        let net = NetFavFake(
            favs: favs,
            realFavs: favs
        )
        let sync: FavSyncerService = FavSyncer(
            net: net,
            storage: ServicesAssembler.inject(),
            cache: cache
        )
        createUser {
            self.cache.create(events: favs) {
                self.cache.addToFavorites(events: favs) {
                    let curFavs = self.cache.userFavorites()
                    XCTAssert(curFavs.count == favs.count)
                    sync.syncFavorites { er in
                        // sync is over, check correctness
                        XCTAssertTrue(self.cache.EvExistsWith(id: "1"))
                        // imitate changes on network side,
                        // e.g. remove one element:
                        net.favs = self.generateEvs(min: 2, max: 2)
                        sync.syncFavorites { er in
                            // check event 1 deletion:
                            XCTAssertFalse(self.cache.EvExistsWith(id: "1"))
                            self.cache.deleteUserWith(id: self.kPhone) {
                                self.cache.deleteAllEvents {
                                    self.cache.deleteAllCategories {
                                        exp.fulfill()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    private func createUser(completion: @escaping (() -> Void)) {
           let u = User(
               id: kPhone,
               login: kPhone,
               name: kName,
               email: kMail,
               avatar: "",
               password: "")
           cache.create(user: u) {
               self.secrets.userID = self.kPhone
               completion()
           }
       }
}

fileprivate final class NetFavFake: NetFavService {
    var favs: [Ev]
    var realFavs: [Ev]
    var err: NSError?

    init(favs: [Ev], realFavs: [Ev] = []) {
        self.favs = favs
        self.realFavs = realFavs
    }

    func fetchFavorites(completion: @escaping (([Ev]?, NSError?) -> Void)) {
        completion(favs, err)
    }

    func addToFavorites(eventID: String, completion: @escaping ((NSError?) -> Void)) {
        if !favs.contains(where: { $0.evId == eventID }) {
            if let e = realFavs.filter({ $0.id == eventID }).first {
                favs.append(e)
            }
        }
        completion(err)
    }

    func removeFromFavorites(eventID: String, completion: @escaping ((NSError?) -> Void)) {
        favs.removeAll(where: { $0.evId == eventID })
        completion(err)
    }
}
