//
//  CEv+CoreDataProperties.swift
//  blagru
//
//  Created by Michael Artuerhof on 19.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CEv {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CEv> {
        return NSFetchRequest<CEv>(entityName: "CEv")
    }

    @NSManaged public var comment: String?
    @NSManaged public var ev_id: String?
    @NSManaged public var lat0: Double
    @NSManaged public var lat1: Double
    @NSManaged public var lon0: Double
    @NSManaged public var lon1: Double
    @NSManaged public var start_date: Date?
    @NSManaged public var category: CCategory?
    @NSManaged public var owner: CUser?
    @NSManaged public var participants: NSSet?
    @NSManaged public var points: NSSet?
    @NSManaged public var usrFavorite: CUser?

}

// MARK: Generated accessors for participants
extension CEv {

    @objc(addParticipantsObject:)
    @NSManaged public func addToParticipants(_ value: CUser)

    @objc(removeParticipantsObject:)
    @NSManaged public func removeFromParticipants(_ value: CUser)

    @objc(addParticipants:)
    @NSManaged public func addToParticipants(_ values: NSSet)

    @objc(removeParticipants:)
    @NSManaged public func removeFromParticipants(_ values: NSSet)

}

// MARK: Generated accessors for points
extension CEv {

    @objc(addPointsObject:)
    @NSManaged public func addToPoints(_ value: CEvPoint)

    @objc(removePointsObject:)
    @NSManaged public func removeFromPoints(_ value: CEvPoint)

    @objc(addPoints:)
    @NSManaged public func addToPoints(_ values: NSSet)

    @objc(removePoints:)
    @NSManaged public func removeFromPoints(_ values: NSSet)

}
