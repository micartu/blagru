//
//  EventCaptionModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 13/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol EventCaptionCellModel: CommonCell { }

struct EventCaptionModel: EventCaptionCellModel {
    let theme: Theme
    let id: String
    let title: String
    let descr: String
    let joined: Bool
    let image: String
    weak var imgLoader: ImageLoaderService?
	weak var delegate: EventCaptionCellDelegate?

	func setup(cell: EventCaptionCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblTitle.text = title
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblTitle.textColor = theme.mainTextColor

        cell.lblMessage.text = descr
        cell.lblMessage.font = theme.regularFont.withSize(theme.sz.small0)
        cell.lblMessage.textColor = theme.mainTextColor
        
        cell.btnJoin.buttonImageColor = joined ? theme.tintColor : theme.inactiveColor

        if image.count > 0 {
            if let av = imgLoader?.loadOffline(image: image) {
                cell.avatarView.image = av
            } else {
                imgLoader?.load(image: image) { [weak cell] im in
                    if cell?.id == self.id {
                        cell?.avatarView.image = im
                    }
                }
            }
        } else {
            cell.avatarView.image = UIImage(named: "user")
        }
    }
}
