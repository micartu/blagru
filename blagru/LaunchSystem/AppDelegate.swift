//
//  AppDelegate.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var delegates: [AppDelegateProtocol] = [
        CoreDataInit(),
        MapKitDelegate(),
        GoogleSignAppDelegate(),
    ]

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        coordinator = AppCoordinator()
        self.window = window
        for d in delegates {
            if !d.initModule(application: application, options: launchOptions) {
                print("Problem when loading a delegate: \(d.description())")
                return false
            }
        }
        coordinator?.launchFrom(window)
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        for d in delegates {
            d.backgroundMethod?(application)
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        for d in delegates {
            d.foregroundMethod?(application)
        }
    }

    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        for d in delegates {
            d.remoteNotification?(userInfo)
        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        for d in delegates {
            if let can = d.canOpenURL?(app, url, options), can {
                return true
            }
        }
        return false
    }

    func applicationWillTerminate(_ application: UIApplication) {
        for d in delegates {
            d.deinitModule?(application)
        }
    }

    // MARK: - Private

    private var coordinator: AppCoordinator? = nil
}
