//
//  UINavigationController+NavigationProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

extension UINavigationController: NavigationProtocol {
    func push(_ cntr: ControllerProtocol, animated: Bool) {
        if let vc = cntr as? UIViewController {
            pushViewController(vc, animated: animated)
        }
    }

    func pop(animated: Bool) {
        popViewController(animated: animated)
    }
}
