//
//  SplashCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 27.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

final class SplashCoordinator: BaseCoordinator {
    let transitor = SplashTransitionCoordinator()
    override func start() {
        let vc = SplashViewController.create()
        let viewModel = SplashViewModel(dispatcher: self)
        vc.vm = viewModel
        cntr = vc
    }
}

extension SplashCoordinator: SplashDispatcherProtocol {
    func transitionSetup() {
        if let nav = cntr?.vc.navigationController {
            nav.isNavigationBarHidden = true
            nav.delegate = transitor
        }
    }

    func loadIsOver() {
        // remove transition delegates
        // no need to show the same effect on normal views:
        if let nav = cntr?.vc.navigationController {
            nav.isNavigationBarHidden = false
            // wait a little
            delay(1) {
                nav.delegate = nil
            }
        }
        exitWith(result: nil)
    }
}
