//
//  MapViewController.swift
//  blagru
//
//  Created by Michael Artuerhof on 03/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit
import GoogleMaps

final class MapViewController: BaseViewController {
	var vm: MapViewModelProtocol!

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var fab: FloatingActionButton!
    @IBOutlet weak var btnAdd: CircledButton!
    @IBOutlet weak var btnSearch: CircledButton!
    @IBOutlet weak var btnNav: CircledButton!
    @IBOutlet weak var ptChooserView: UIView!
    @IBOutlet weak var ptChooserHeight: NSLayoutConstraint!
    @IBOutlet weak var infoView: UIView!

    /// The view controller that start and end destinations and help info to user
    lazy var chooserVC: PtChooserViewController = {
        return children.lazy.compactMap({ $0 as? PtChooserViewController }).first!
    }()

    /// The info window controller
    lazy var infoVC: InfoViewController = {
        return children.lazy.compactMap({ $0 as? InfoViewController }).first!
    }()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = vm as? GMSMapViewDelegate
        chooserVC.vm = vm as? PointChooserViewModelProtocol
        infoVC.vm = vm as? InfoViewModelProtocol
        // buttons
        fab.itemAnimationConfiguration = .circularSlideIn(withRadius: 120)
        fab.itemAnimationConfiguration.appendMode = .above
        fab.buttonAnimationConfiguration = .customAnimation { [weak self] state in
            let tr: CGAffineTransform
            if state == .opening {
                let cx: CGFloat = 3.0
                tr = CGAffineTransform(scaleX: cx, y: cx)
            } else {
                tr = .identity
            }
            UIView.animate(withDuration: 0.3) {
                self?.fab.circleView.transform = tr
            }
        }
        fab.buttonAnimationConfiguration.opening.duration = 0.8
        fab.buttonAnimationConfiguration.closing.duration = 0.6
        fab.overlayColor = UIColor.lightGray.withAlphaComponent(0.8)
        fab.buttonImageSize = CGSize(width: 22, height: 22)
        for b in circledButtons() {
            b.addTarget(self,
                        action: #selector(buttonAction(_:)),
                        for: .touchUpInside)
        }
        let pt = MapPointer(
            mapView: mapView,
            cache: ServicesAssembler.inject())
        pt.set(theme: theme)
        vm.set(pointer: pt)
        pointer = pt
        vm.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.delegate = self
        vm.viewWillAppear()
    }

    override func apply(theme: Theme) {
        super.apply(theme: theme)
        fab.buttonImageColor = theme.mainTextColor
        let circleColor = theme.inactiveColor.withAlphaComponent(0.5)
        let buttonColor = theme.mainTextColor.withAlphaComponent(0.6)
        fab.circleColor = circleColor
        fab.configureDefaultItem { item in
            item.imageView.contentMode = .scaleAspectFill
            item.buttonColor = buttonColor
            item.buttonImageColor = theme.emptyBackColor
            item.imageSize = CGSize(width: 15, height: 15)
        }
        for b in circledButtons() {
            b.buttonImageColor = buttonColor
            b.circleColor = circleColor
        }
        pointer?.set(theme: theme)
    }

    fileprivate weak var pointer: MapPointer?

    fileprivate func circledButtons() -> [CircledButton] {
        return [
            btnAdd,
            btnSearch,
            btnNav,
        ]
    }
}

// MARK: - Actions
extension MapViewController {
    @objc
    internal func buttonAction(_ sender: UIView) {
        switch sender {
        case btnAdd:
            applyTouchAnimation(sender) { [weak self] in
                self?.vm.addEventAction()
            }
        case btnSearch:
            applyTouchAnimation(sender) { [weak self] in
                self?.vm.searchEventAction()
            }
        case btnNav:
            applyTouchAnimation(sender) { [weak self] in
                self?.vm.curPositionAction()
            }
        default:
            ()
        }
    }
}

// MARK: - UINavigationControllerDelegate
extension MapViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              willShow viewController: UIViewController,
                              animated: Bool) {
        if viewController == self {
            navigationController
                .setNavigationBarHidden(true, animated: animated)
        }
    }
}

// MARK: - MapViewProtocol
extension MapViewController: MapViewProtocol {
    func change(zoom: Float, in camera: GMSCameraPosition) -> Bool {
        mapView?.camera = GMSCameraPosition(target: camera.target,
                                            zoom: zoom,
                                            bearing: camera.bearing,
                                            viewingAngle: camera.viewingAngle)
        if let m = mapView, zoom <= m.maxZoom && zoom >= m.minZoom {
            return true
        }
        return false
    }

    func moveMapTo(point: CLLocationCoordinate2D, zoom: Float) {
        let position = GMSCameraPosition(target: point,
                                         zoom: zoom,
                                         bearing: 0,
                                         viewingAngle: 0)
        mapView?.animate(to: position)
    }

    func userPosition(enabled: Bool) {
        mapView.isMyLocationEnabled = enabled
    }

    func setFAB(actions: [ActionInit]) {
        fab.items.removeAll(keepingCapacity: true)
        for a in actions {
            fab.addItem(image: a.image) { _ in
                a.action()
            }
        }
    }

    func setActiveFAB(icon: UIImage) {
        fab.buttonImage = icon
    }

    func enableUserPositionOnMap() {
        // show user's position:
        mapView.isMyLocationEnabled = true
    }

    func set(button: MapUIButton, active: Bool) {
        let btn: CircledButton
        switch button {
        case .createEvent:
            btn = btnAdd
        case .searchEvent:
            btn = btnSearch
        case .curPosEvent:
            btn = btnNav
        }
        btn.buttonImageColor = active ?
            theme.tintColor : theme.mainTextColor.withAlphaComponent(0.6)
    }

    private func makeWidget(_ w: UIView,
                            hidden: Bool,
                            transform: CGAffineTransform,
                            animated: Bool,
                            completion: @escaping (() -> Void)) {
        func makeIT() {
            let tr: CGAffineTransform
            if hidden {
                tr = transform
            } else {
                tr = .identity
            }
            w.transform = tr
            adjustTopZoom()
        }
        if animated {
            UIView.animate(withDuration: 0.3, delay: 0, animations: {
                makeIT()
            }, completion: { _ in
                completion()
            })
        } else {
            makeIT()
            completion()
        }
    }

    private var makeSureItsHidden: CGFloat {
        let b = UIScreen.main.bounds
        return max(b.width, b.height)
    }

    func makeChooserPanel(hidden: Bool,
                          animated: Bool,
                          completion: @escaping (() -> Void)) {
        makeWidget(ptChooserView,
                   hidden: hidden,
                   transform: CGAffineTransform(translationX: 0, y: -300),
                   animated: animated,
                   completion: completion)
    }

    func makeInfoPanel(hidden: Bool,
                       animated: Bool,
                       completion: @escaping (() -> Void)) {
        makeWidget(infoView,
                   hidden: hidden,
                   transform: CGAffineTransform(translationX: -makeSureItsHidden, y: 0),
                   animated: animated,
                   completion: completion)
    }


    func make(btn: MapUIButton,
              hidden: Bool,
              animated: Bool,
              completion: @escaping (() -> Void)) {
        func makeIT() {
            func neededButton() -> UIView {
                let b: UIView
                switch btn {
                case .searchEvent:
                    b = btnSearch
                case .createEvent:
                    b = btnAdd
                case .curPosEvent:
                    b = btnNav
                }
                return b
            }
            if hidden {
                let tr = CGAffineTransform(translationX: makeSureItsHidden, y: 0)
                neededButton().transform = tr
            } else {
                neededButton().transform = .identity
            }
        }
        if animated {
            UIView.animate(withDuration: 0.3, delay: 0, animations: {
                makeIT()
            }, completion: { _ in
                completion()
            })
        } else {
            makeIT()
            completion()
        }
    }

    func makeChooserView(shrink: Bool) {
        ptChooserHeight.constant = shrink ? 30 : 60
        adjustTopZoom()
    }

    private func adjustTopZoom() {
        let top: CGFloat
        // chooser is shown and it has normal height:
        if ptChooserView.transform == .identity && ptChooserHeight.constant == 60 {
            top = 100
        // chooser is shown and it has small height:
        } else  if ptChooserView.transform == .identity && ptChooserHeight.constant == 30 {
            top = 230
        } else { // other cases
            top = 20
        }
        pointer?.set(top: top)
    }
}

// MARK: - PtChooserViewProtocol
extension MapViewController: PtChooserViewProtocol {
    func blink(button: dstButtonType) {
        chooserVC.blink(button: button)
    }

    func showMsg(_ msg: String, timeout: TimeInterval) {
        chooserVC.showMsg(msg, timeout: timeout)
    }

    func makeStackView(hidden: Bool,
                       animated: Bool,
                       completion: (() -> Void)?) {
        chooserVC.makeStackView(hidden: hidden,
                                animated: animated,
                                completion: completion)
    }

    func make(button: OptionalButtonType, hidden: Bool) {
        chooserVC.make(button: button, hidden: hidden)
    }

    func make(button: OptionalButtonType, active: Bool) {
        chooserVC.make(button: button, active: active)
    }

    func makeButtons(hide: [OptionalButtonType], appear: [OptionalButtonType]) {
        chooserVC.makeButtons(hide: hide, appear: appear)
    }
}


// MARK: - PtChooserViewProtocol
extension MapViewController: InfoViewProtocol {
    func showInfoMsg(_ msg: String) {
        infoVC.showInfoMsg(msg)
    }
}

// MARK: - ViewControllerable
extension MapViewController: ViewControllerable {
	static var storyboardName: String {
		return "Map"
	}
}
