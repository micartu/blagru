//
//  ButtonCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 08.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class ButtonCell: UITableViewCell {
    @IBOutlet weak var button: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        button.addTarget(self,
                         action: #selector(cellTapped),
                         for: .touchUpInside)
    }

    @objc private func cellTapped () {
        delegate?.cellTouched(id: id)
    }

    var id = ""
    weak var delegate: TouchableCell?
}
