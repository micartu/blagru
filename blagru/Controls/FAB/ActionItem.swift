//
//  ActionItem.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

/// The item view representing an action.
///
import UIKit

@IBDesignable open class ActionItem: UIControl {
    /// The action that is executed when the item is tapped.
    /// Default is `nil`.
    ///
    /// - Parameter item: The action item that has been tapped.
    ///
    var action: ((ActionItem) -> Void)?

    /// Calls the action on the action item.
    ///
    func callAction() {
        action?(self)
    }

    /// The color of action item circle view.
    /// Default is `UIColor.black`.
    ///
    /// - SeeAlso: `circleView`
    ///
    @IBInspectable var buttonColor: UIColor {
        get {
            return circleView.color
        }
        set {
            circleView.color = newValue
        }
    }

    /// The color of action item circle view with highlighted state.
    /// Default is `nil`.
    ///
    /// - SeeAlso: `circleView`
    ///
    @IBInspectable var highlightedButtonColor: UIColor? {
        get {
            return circleView.highlightedColor
        }
        set {
            circleView.highlightedColor = newValue
        }
    }

    /// The image displayed by the item.
    /// Default is `nil`.
    ///
    /// - SeeAlso: `imageView`
    ///
    @IBInspectable var buttonImage: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
        }
    }

    /// The tint color of the image view.
    /// By default the color of the floating action button is used.
    ///
    /// - Warning: Only template images are colored.
    ///
    /// - SeeAlso: `imageView`
    ///
    @IBInspectable var buttonImageColor: UIColor {
        get {
            return imageView.tintColor
        }
        set {
            imageView.tintColor = newValue
        }
    }

    /// The image view of the item. Can be configured as needed.
    /// Read only.
    ///
    /// - SeeAlso: `buttonImage`
    /// - SeeAlso: `buttonImageColor`
    ///
    fileprivate(set) lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        return imageView
    }()

    /// The background circle of the item. Can be configured as needed.
    /// Read only.
    ///
    /// - SeeAlso: `buttonColor`
    /// - SeeAlso: `highlightedButtonColor`
    ///
    fileprivate(set) lazy var circleView: CircleView = {
        let view = CircleView()
        view.color = .black
        view.isUserInteractionEnabled = false
        return view
    }()

    /// The size of the image view. Default is `(0, 0)`.
    /// When imageSize is `.zero` the image is shrunken until it fits
    /// completely into the circle view. If it already does the actual
    /// size of the image is used.
    ///
    var imageSize: CGSize = .zero {
        didSet {
            setNeedsUpdateConstraints()
        }
    }

    internal override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    /// Returns an object initialized from data in a given unarchiver.
    ///
    /// - Parameter aDecoder: An unarchiver object.
    ///
    /// - Returns: `self`, initialized using the data in decoder.
    ///
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    fileprivate var dynamicConstraints: [NSLayoutConstraint] = []
}

// MARK: - UIView

extension ActionItem {
    /// Tells the view that its superview changed.
    ///
    open override func didMoveToSuperview() {
        // reset tintAdjustmentMode
        imageView.tintColorDidChange()
    }

    /// Updates constraints for the view.
    ///
    open override func updateConstraints() {
        updateDynamicConstraints()
        super.updateConstraints()
    }
}

// MARK: - UIControl

extension ActionItem {
    /// A Boolean value indicating whether the action item draws a highlight.
    ///
    open override var isHighlighted: Bool {
        set {
            super.isHighlighted = newValue
            circleView.isHighlighted = newValue
        }
        get {
            return super.isHighlighted
        }
    }
}

// MARK: - Private Methods

fileprivate extension ActionItem {
    func setup() {
        backgroundColor = .clear
        isUserInteractionEnabled = true

        addSubview(circleView)
        circleView.addSubview(imageView)

        circleView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false

        createStaticConstraints()
        updateDynamicConstraints()
    }

    func updateDynamicConstraints() {
        NSLayoutConstraint.deactivate(dynamicConstraints)
        dynamicConstraints.removeAll()
        createDynamicConstraints()
        NSLayoutConstraint.activate(dynamicConstraints)
        setNeedsLayout()
    }

    func createStaticConstraints() {
        setContentHuggingPriority(.required, for: .horizontal)
        setContentHuggingPriority(.required, for: .vertical)

        var constraints: [NSLayoutConstraint] = []
        var constraint: NSLayoutConstraint

        constraints.append(imageView.centerXAnchor.constraint(equalTo: circleView.centerXAnchor))
        constraints.append(imageView.centerYAnchor.constraint(equalTo: circleView.centerYAnchor))

        constraints.append(circleView.widthAnchor.constraint(equalTo: circleView.heightAnchor))

        constraints.append(circleView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor))
        constraints.append(circleView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor))
        constraints.append(circleView.topAnchor.constraint(greaterThanOrEqualTo: topAnchor))
        constraints.append(circleView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor))

        constraint = circleView.leadingAnchor.constraint(equalTo: leadingAnchor)
        constraint.priority = .defaultLow
        constraints.append(constraint)
        constraint = circleView.trailingAnchor.constraint(equalTo: trailingAnchor)
        constraint.priority = .defaultLow
        constraints.append(constraint)
        constraint = circleView.topAnchor.constraint(equalTo: topAnchor)
        constraint.priority = .defaultLow
        constraints.append(constraint)
        constraint = circleView.bottomAnchor.constraint(equalTo: bottomAnchor)
        constraint.priority = .defaultLow
        constraints.append(constraint)

        NSLayoutConstraint.activate(constraints)

        imageView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        imageView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .vertical)
        circleView.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        circleView.setContentHuggingPriority(.fittingSizeLevel, for: .vertical)
    }

    func createDynamicConstraints() {
        dynamicConstraints.append(contentsOf: imageSizeConstraints)
    }

    var imageSizeConstraints: [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []
        if imageSize == .zero {
            let multiplier = CGFloat(1 / sqrt(2))
            constraints.append(imageView.widthAnchor.constraint(lessThanOrEqualTo: circleView.widthAnchor, multiplier: multiplier))
            constraints.append(imageView.heightAnchor.constraint(lessThanOrEqualTo: circleView.heightAnchor, multiplier: multiplier))
        } else {
            constraints.append(imageView.widthAnchor.constraint(equalToConstant: imageSize.width))
            constraints.append(imageView.heightAnchor.constraint(equalToConstant: imageSize.height))
        }
        return constraints
    }
}
