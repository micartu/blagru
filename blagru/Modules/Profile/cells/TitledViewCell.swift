//
//  TitledViewCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 10.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit
import DropDown

enum TitledViewCellType {
    case radio
    case combo
}

protocol RadioItemsCellDelegate: class {
    func changed(selected: Int, id: String)
}

final class TitledViewCell: ExtendableTableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    var type: TitledViewCellType = .radio

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @objc internal func itemTapped(_ btn: UIButton) {
        if type == .radio {
            for i in 1...count {
                if let b = viewWithTag(i) as? UIButton,
                    let v = b.superview {
                    let a = (btn.tag == b.tag)
                    if a {
                        radioDelegate?.changed(selected: i - 1, id: id)
                    }
                    setRadioIn(view: v, active: a)
                }
            }
        } else if type == .combo {
            // open up combo box:
            dropDown.show()
        }
    }

    struct const {
        static let fields = ":fields:"
        static let selected = ":selected:"
        static let sx:CGFloat = 21
        static let rpict0 = "combo"
        static let rpict1 = "combo_selected"
    }
    internal var curSelection = -1
    internal var items: [String]!
    internal let dropDown = DropDown()
    weak var radioDelegate: RadioItemsCellDelegate? = nil
}
