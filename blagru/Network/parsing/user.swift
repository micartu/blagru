//
//  user.swift
//  blagru
//
//  Created by Michael Artuerhof on 30.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

func parseUser(from d: NSDictionary) -> User {
    let iid = parseInt(d, key: "id")
    let id = "\(iid)"
    let name = parseString(d, for: "name")
    let login = parseString(d, for: "login")
    let email = parseString(d, for: "email")
    let avatar = id
    return User(id: id,
                login: login,
                name: name,
                email: email,
                avatar: avatar,
                password: "")
}
