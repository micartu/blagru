//
//  CoordinatorProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol CoordinatorProtocol {
    var cntr: ControllerProtocol? { get }
    func start()
    func stop()
    func presented(on vc: ControllerProtocol, controller: ControllerProtocol?)
}

public protocol ParentCoordinatorProtocol: class {
    func exitFrom(coordinator: CoordinatorProtocol, with result: Any?)
}

public protocol BaseDispatcherProtocol: class {
    func exitWith(result: Any?)
}

protocol CommonCoordinatorProtocol:
    ParentCoordinatorProtocol,
    BaseDispatcherProtocol,
    AlertProtocol { }
