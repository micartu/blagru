//
//  MapPointerService.swift
//  blagru
//
//  Created by Michael Artuerhof on 09.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import CoreLocation

enum PointType {
    case A
    case B
}

struct MapPointDescr {
    let type: PointType
    let coord: CLLocationCoordinate2D
    let state: PointState
}

struct MapAttachDescr {
    let ind0: Int64
    let ind1: Int64
    let ev: Ev?
}

protocol MapPointerService {
    func add(point: CLLocationCoordinate2D, type: PointType, state: PointState) -> Int64
    func add(points: [MapPointDescr], completion: @escaping (([Int64]) -> Void))
    func changePointWith(ind: Int64, state: PointState)
    func connect(ind0: Int64, ind1: Int64)
    func sameArc(ind0: Int64?, ind1: Int64?) -> Bool
    func attachTo(ind: Int64, event: Ev)
    func attachInBatch(pts: [MapAttachDescr], completion: @escaping (() -> Void))
    func event(ind: Int64) -> Ev?
    func drawArcPolyline(ind0: Int64, ind1: Int64)
    func connectedLineIndicies() -> (Int64, Int64)?
    func coordinatesOf(ind: Int64) -> CLLocationCoordinate2D?
    func findConnectionTo(point: Int64) -> Int64?
    func restorePoints()
    func removeArcPolyline()
    func remove(indicies: [Int64], completion: @escaping (() -> Void))
    func removeAllPoints(completion: @escaping (() -> Void))
}
