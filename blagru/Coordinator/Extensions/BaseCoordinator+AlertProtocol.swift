//
//  BaseCoordinator+AlertProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

extension BaseCoordinator: AlertProtocol {
    private var vc: UIViewController? {
        return cntr?.vc
    }

    func handle(error: NSError) {
        vc?.handle(error: error)
    }

    func chooseFrom(titles: [String], title: String, okAction: ((String, Int) -> Void)?) {
        vc?.chooseFrom(titles: titles, title: title, okAction: okAction)
    }

    func show(buttons: [String],
              title: String,
              message: String,
              okAction: ((String, Int) -> Void)?) {
        vc?.show(buttons: buttons,
                 title: title,
                 message: message,
                 okAction: okAction)
    }

    func show(image: UIImage,
              titles: [String],
              title: String,
              message: String,
              style: UIAlertController.Style,
              needCancel: Bool,
              okAction: ((String, Int) -> Void)?) {
        vc?.show(image: image,
                 titles: titles,
                 title: title,
                 message: message,
                 style: style,
                 needCancel: needCancel,
                 okAction: okAction)
    }

    func show(title: String, error: String, action: ((UIAlertAction) -> Void)?) {
        show(title: title, message: error, okTitle: Const.text.kOk, action: action)
    }

    func show(title: String, error: String) {
        show(title: title, error: error, action: nil)
    }

    func show(error: String) {
        show(title: "Error".localized, error: error, action: nil)
    }

    func show(title: String, message: String) {
        show(title: title, message: message, okTitle: Const.text.kClose, action: nil)
    }

    func show(title: String, message: String, okTitle: String, action: ((UIAlertAction) -> Void)?) {
        vc?.show(title: title,
                 message: message,
                 okTitle: okTitle,
                 action: action)
    }

    func showYesNO(title: String,
                   message: String,
                   actionYes: ((UIAlertAction) -> Void)?,
                   actionNo: ((UIAlertAction) -> Void)?) {
        vc?.showYesNO(title: title,
                      message: message,
                      actionYes: actionYes,
                      actionNo: actionNo)
    }

    func showInputDialog(title: String,
                         message: String,
                         okAction:((String) -> Void)?) {
        vc?.showInputDialog(title: title,
                            message: message,
                            okAction: okAction)
    }

    func showBusyIndicator() {
        busy.mutate { $0 = true }
        delay(0.2) { [weak self] in
            if self?.busy.value == false { return }
            self?.vc?.showBusyIndicator()
        }
    }

    func hideBusyIndicator() {
        busy.mutate { $0 = false }
        vc?.hideBusyIndicator()
    }
}
