//
//  FloatingActionButtonState.swift
//  blagru
//
//  Created by Michael Artuerhof on 03.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

enum FloatingActionButtonState: Int {
    /// No items are visible
    ///
    case closed

    /// Items are fully visible
    ///
    case open

    /// During opening animation
    ///
    case opening

    /// During closing animation
    ///
    case closing
}
