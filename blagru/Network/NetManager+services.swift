//
//  NetManager+services.swift
//  blagru
//
//  Created by Michael Artuerhof on 11.05.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetManager: ServiceLocatorModulProtocol {
    func registerServices(serviceLocator s: ServicesAssembler) {
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetAuthService
        })
        /*
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetPointsService
        })
 */
        s.register(factory: { (named: String) in
            NetFunFake(count: 10) as NetPointsService
        })
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetFavService
        })
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetMapService
        })
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetEventService
        })
        s.register(factory: { (named: String) in
            NetFun(netmanager: self, named: named) as NetProfileService
        })
    }
}
