//
//  UIView+ToBorders.swift
//  blagru
//
//  Created by Michael Artuerhof on 18.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

extension UIView {
    func attachConnectedToBorders(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        addConstraints([view.topAnchor.constraint(equalTo: topAnchor),
                        view.leadingAnchor.constraint(equalTo: leadingAnchor),
                        view.trailingAnchor.constraint(equalTo: trailingAnchor),
                        view.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
}
