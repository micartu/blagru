//
//  CCategory+CoreDataProperties.swift
//  blagru
//
//  Created by Michael Artuerhof on 15.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//
//

import Foundation
import CoreData


extension CCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CCategory> {
        return NSFetchRequest<CCategory>(entityName: "CCategory")
    }

    @NSManaged public var cat_id: String?
    @NSManaged public var name: String?
    @NSManaged public var icon: String?
    @NSManaged public var events: NSSet?
    @NSManaged public var used_date: Date?

}

// MARK: Generated accessors for events
extension CCategory {

    @objc(addEventsObject:)
    @NSManaged public func addToEvents(_ value: CEv)

    @objc(removeEventsObject:)
    @NSManaged public func removeFromEvents(_ value: CEv)

    @objc(addEvents:)
    @NSManaged public func addToEvents(_ values: NSSet)

    @objc(removeEvents:)
    @NSManaged public func removeFromEvents(_ values: NSSet)

}
