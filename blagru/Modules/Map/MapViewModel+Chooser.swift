//
//  MapViewModel+Chooser.swift
//  blagru
//
//  Created by Michael Artuerhof on 04.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation
import CoreLocation

extension MapViewModel: PointChooserViewModelProtocol {
    func swipeUp() {
        view?.makeChooserPanel(hidden: true, animated: true) { [weak self] in
            self?.selectorShown = false
        }
    }

    func swipeRight() {
        view?.makeStackView(hidden: false, animated: true)
    }

    func swipeLeft() {
    }

    private func buttonPressedOf(type: PointType) {
        let ind: Int64?
        switch type {
        case .A:
            ind = pt0Ind
            pt0Ind = nil
            selectAButton()
            view?.blink(button: .source)
        case .B:
            ind = pt1Ind
            pt1Ind = nil
            selectBButton()
            view?.blink(button: .dest)
        }
        if let ind = ind {
            ptIndices.append(ind)
        }
        if mode == .search {
            view?.makeChooserView(shrink: false)
            view?.makeInfoPanel(hidden: true, animated: true)
        }
        spt0Ind = nil
        spt1Ind = nil
        pointer?.removeArcPolyline()
        removeAllIrrelevantPoints { [weak self] in
            self?.switchModeIfNeeded(networkCall: false)
        }
    }

    func actionA() {
        buttonPressedOf(type: .A)
    }

    func actionB() {
        buttonPressedOf(type: .B)
    }

    func actionFavorites() {
        if let id = selectedPointInFavoritesID() {
            dispatcher?.showYesNO(
                title: "Are you sure?".localized,
                message: "Do you want to delete point from favorites?".localized,
                actionYes: { [weak self] _ in
                    self?.cache.delete(events: [EvDesc(evId: id)]) {
                        self?.checkOpMode()
                    }
                },
                actionNo: nil)

        } else {
            tryToAddToFavorites()
        }
    }

    fileprivate func tryToAddToFavorites() {
        if let p0 = pt0Ind,
            let p1 = pt1Ind,
            let c0 = pointer?.coordinatesOf(ind: p0),
            let c1 = pointer?.coordinatesOf(ind: p1) {
            dispatcher?.askAuthorizationAndExecute { [weak self] in
                self?.dispatcher?.showInputDialog(
                    title: "Comment to point".localized,
                    message: "Enter comment to favorites".localized
                ) { comment in
                    if comment.count > 0 {
                        let cat = EvCategoryDesc(id: self?.category?.cat_id ?? "")
                        let ev = Ev(
                            id: UUID().uuidString,
                            lat0: c0.latitude,
                            lon0: c0.longitude,
                            lat1: c1.latitude,
                            lon1: c1.longitude,
                            comment: comment,
                            category: cat,
                            owner: UserDesc(id: self?.secret.userID ?? ""),
                            participants: [])
                        self?.cache.addToFavorites(events: [ev]) {
                            self?.checkOpMode()
                        }
                    } else {
                        self?.dispatcher?.show(error: "Enter something as a comment!".localized)
                    }
                }
            }
        } else {
            dispatcher?.show(error: "Please choose start and end points!".localized)
        }
    }

    func actionSavedList() {
        dispatcher?.askAuthorizationAndExecute { [weak self] in
            self?.dispatcher?.showFavorites()
        }
    }

    internal func resetPoints() {
        pointer?.removeArcPolyline()
        removeAllIrrelevantPoints {}
    }

    internal func pointSelector(show: Bool) {
        if selectorShown != show {
            view?.makeChooserPanel(hidden: !show, animated: true) { [weak self] in
                self?.selectorShown = show
            }
        }
    }

    func actionDelete() {
        dispatcher?.showYesNO(
            title: "Are you sure?".localized,
            message: "Active Route will be deleted!".localized,
            actionYes: { [weak self] _ in
                self?.resetToIdleMode()
            },
            actionNo: nil)
    }

    func actionDone() {
        if let i0 = pt0Ind,
            let i1 = pt1Ind,
            let p0 = pointer?.coordinatesOf(ind: i0),
            let p1 = pointer?.coordinatesOf(ind: i1) {
            dispatcher?.askAuthorizationAndExecute { [weak self] in
                self?.dispatcher?.showInputDialog(
                    title: "Comment it".localized,
                    message: "Enter comment for other users".localized
                ) {  comment in
                    if comment.count > 0 {
                        let cat = EvCategoryDesc(id: self?.category?.cat_id ?? "")
                        let ev = Ev(
                            id: "",
                            lat0: p0.latitude,
                            lon0: p0.longitude,
                            lat1: p1.latitude,
                            lon1: p1.longitude,
                            comment: comment,
                            category: cat,
                            owner: UserDesc(id: self?.secret.userID ?? ""),
                            participants: [])
                        self?.net.create(event: ev) { (event, err) in
                            if let e = err {
                                self?.dispatcher?.show(error: e.localizedDescription)
                            } else {
                                self?.usrEvent = event
                            }
                        }
                    } else {
                        self?.dispatcher?.show(
                            error: "Enter something as a comment!".localized
                        )
                    }
                }
            }
        }
    }
}
