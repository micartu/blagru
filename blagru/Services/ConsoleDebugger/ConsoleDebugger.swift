//
//  ConsoleDebugger.swift
//  blagru
//
//  Created by Michael Artuerhof on 28.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

final class ConsoleDebugger: ConsoleDebuggerService {
    init(name: String = "") {
        dname = name
    }

    public func set(name: String) {
        self.dname = name
    }

    public func pr(_ messages: String...) {
        #if DEBUG
        var out = [String]()
        out.append(contentsOf: messages)
        let title = "[\(dname)]: "
        out.insert(title, at: 0)
        print(out.joined())
        #endif
    }

    // MARK: - Private

    private var dname: String
    private var enabled = false
}
