//
//  MobileNetTransportService.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import Alamofire

protocol MobileNetTransportService: NetTransportService {
    var sessionManager: SessionManager { get }
    func commonParsingResponse(_ response: DataResponse<Any>,
                               success: ([NSDictionary]) -> Void,
                               failure: ((NSError) -> Void)?)
}
