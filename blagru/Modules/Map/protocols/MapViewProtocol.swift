//
//  MapViewProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 04.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

struct ActionInit {
    let image: UIImage
    let action: (() -> Void)
}

enum MapUIButton {
    case createEvent
    case searchEvent
    case curPosEvent
}

protocol MapViewProtocol: class,
    PtChooserViewProtocol,
    InfoViewProtocol {
    func moveMapTo(point: CLLocationCoordinate2D, zoom: Float)
    func userPosition(enabled: Bool)
    func change(zoom: Float, in camera: GMSCameraPosition) -> Bool
    func setFAB(actions: [ActionInit])
    func setActiveFAB(icon: UIImage)
    func enableUserPositionOnMap()
    func set(button: MapUIButton, active: Bool)
    func makeChooserPanel(hidden: Bool,
                          animated: Bool,
                          completion: @escaping (() -> Void))
    func makeInfoPanel(hidden: Bool,
                       animated: Bool,
                       completion: @escaping (() -> Void))
    func make(btn: MapUIButton,
              hidden: Bool,
              animated: Bool,
              completion: @escaping (() -> Void))
    func makeChooserView(shrink: Bool)
}

extension MapViewProtocol {
    func makeChooserPanel(hidden: Bool,
                          animated: Bool) {
        makeChooserPanel(hidden: hidden, animated: animated, completion: {})
    }

    func makeInfoPanel(hidden: Bool,
                       animated: Bool) {
        makeInfoPanel(hidden: hidden, animated: animated, completion: {})
    }

    func make(btn: MapUIButton,
              hidden: Bool,
              animated: Bool) {
        make(btn: btn, hidden: hidden, animated: animated, completion: {})
    }
}
