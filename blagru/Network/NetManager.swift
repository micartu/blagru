//
//  NetManager.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.06.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation
import Alamofire

fileprivate struct NamedOp {
    let name: String
    let op: (() -> Void)
}

final class NetManager: NetManagerService {

    private let transport: MobileNetTransport
    private let cfg: NetworkConfig
    private let authHandler: AuthHandler
    private let sessionManager: SessionManager

    init(cfg: NetworkConfig,
         secret: SecretService,
         transport: MobileNetTransport) {
        self.cfg = cfg
        self.transport = transport
        self.sessionManager = transport.sessionManager
        self.authHandler = AuthHandler(transport: transport,
                                       secret: secret,
                                       cfg: cfg)
        base = cfg.baseUrl + cfg.prefix
        sessionManager.adapter = authHandler
        sessionManager.retrier = authHandler
    }

    func auth(login: String,
              password: String,
              success: ((NSDictionary) -> Void)?,
              failure: ((NSError) -> Void)?) {
        authHandler.auth(login: login, password: password) { (ok, err) in
            if let e = err {
                failure?(e)
            } else {
                success?(ok ?? NSDictionary())
            }
        }
    }

    func socialAuth(type: String,
                    token: String,
                    success: ((NSDictionary) -> Void)?,
                    failure: ((NSError) -> Void)?) {
        authHandler.socialAuth(type: type, token: token) { (ok, err) in
            if let e = err {
                failure?(e)
            } else {
                success?(ok ?? NSDictionary())
            }
        }
    }

    func register(user: User, completion: @escaping ((NSDictionary?, NSError?) -> Void)) {
        authHandler.register(user: user, completion: completion)
    }

    func register(login: String,
                  name: String,
                  email: String,
                  password: String,
                  success: ((NSDictionary) -> Void)?,
                  failure: ((NSError) -> Void)?) {
        let u = User(
            id: "",
            login: login,
            name: name,
            email: email,
            avatar: "",
            password: password)
        register(user: u) { ans, err in
            if let e = err {
                failure?(e)
            } else {
                success?(ans ?? NSDictionary())
            }
        }
    }

    func logout(success: (() -> Void)?, failure: ((NSError) -> Void)?) {
        // TODO: add me
    }

    func getQuery<T>(url: String,
                     params: [String : Any]?,
                     success: ((T) -> Void)?,
                     failure: ((NSError) -> Void)?) {
        put { [weak self] in
            guard let `self` = self else { return }
            self.transport.getQuery(
                url: self.base + url,
                params: params,
                success: { (ans: T) in
                    self.s(ans, success)
                    self.executeNext()
                },
                failure: { err in
                    self.f(err, failure)
                    self.executeNext()
                }
            )
        }
    }

    func getCustomURLQuery<T>(url: String,
                              params: [String : Any]?,
                              success: ((T) -> Void)?,
                              failure: ((NSError) -> Void)?) {
        put { [weak self] in
            guard let `self` = self else { return }
            self.transport.getCustomURLQuery(
                url: url,
                params: params,
                success: { (ans: T) in
                    self.s(ans, success)
                    self.executeNext()
                },
                failure: { err in
                    self.f(err, failure)
                    self.executeNext()
                }
            )
        }
    }

    func getRawQuery(url: String, success: ((Data) -> Void)?, failure: ((NSError) -> Void)?) {
        put { [weak self] in
            guard let `self` = self else { return }
            self.transport.getRawQuery(
                url: self.cfg.baseUrl + url,
                success: { (ans: Data) in
                    self.s(ans, success)
                    self.executeNext()
                },
                failure: { err in
                    self.f(err, failure)
                    self.executeNext()
                }
            )
        }
    }

    func postQuery<T>(url: String, data: Data, success: ((T) -> Void)?, failure: ((NSError) -> Void)?) {
        put { [weak self] in
            guard let `self` = self else { return }
            self.transport.postQuery(
                url: self.base + url,
                data: data,
                success: { (ans: T) in
                    self.s(ans, success)
                    self.executeNext()
                },
                failure: { err in
                    self.f(err, failure)
                    self.executeNext()
                }
            )
        }
    }

    func xgetQuery<T>(url: String, success: ((T) -> Void)?, failure: ((NSError) -> Void)?) {
        put { [weak self] in
            guard let `self` = self else { return }
            self.transport.xgetQuery(
                url: self.base + url,
                success: { (ans: T) in
                    self.s(ans, success)
                    self.executeNext()
                },
                failure: { err in
                    self.f(err, failure)
                    self.executeNext()
                }
            )
        }
    }

    func xpostQuery<T>(url: String,
                       parameters: [String : Any],
                       success: ((T) -> Void)?,
                       failure: ((NSError) -> Void)?) {
        put { [weak self] in
            guard let `self` = self else { return }
            self.transport.xpostQuery(
                url: self.base + url,
                parameters: parameters,
                success: { (ans: T) in
                    self.s(ans, success)
                    self.executeNext()
                },
                failure: { err in
                    self.f(err, failure)
                    self.executeNext()
                }
            )
        }
    }

    // MARK: - Private

    private func put(_ op: @escaping (() -> Void)) {
        let q = NamedOp(name: usedBy, op: op)
        lock.lock()
        operations.append(q)
        lock.unlock()
        executeNext()
    }

    private func s<T>(_ ans: T, _ success: ((T) -> Void)?) {
        DispatchQueue.main.async {
            success?(ans)
        }
    }

    private func f(_ err: NSError, _ failure: ((NSError) -> Void)?) {
        DispatchQueue.main.async {
            failure?(err)
        }
    }

    internal func errHandler(_ failure: ((NSError) -> Void)?) -> ((NSError) -> Void) {
        return { e in
            DispatchQueue.main.async {
                failure?(e)
            }
        }
    }

    private func executeNext() {
        queue.addOperation { [weak self] in
            let opp: NamedOp?
            self?.lock.lock()
            opp = self?.operations.popLast()
            self?.lock.unlock()
            if let p = opp {
                p.op()
            }
        }
    }

    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "NetManager Queue"
        queue.qualityOfService = .userInitiated
        return queue
    }()
    private lazy var operations = [NamedOp]()
    internal var usedBy = ""
    private let lock = NSRecursiveLock()
    private let base: String
}

extension NetManager: NetQueueService {
    func used(by: String) {
        usedBy = by
    }

    func removeAllOperations() {
        lock.lock()
        operations = operations.filter({ $0.name != usedBy })
        lock.unlock()
    }
}
