//
//  EvCategory+sync.swift
//  blagru
//
//  Created by Michael Artuerhof on 22.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension EvCategory: SyncItemProtocol {
    var itemID: String {
        return id
    }

    var tag: String {
        return name + icon
    }
}
