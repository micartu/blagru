//
//  CommonEditableModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol CommonEditableModel: CommonCell {
    var focused: Bool { get set }
    var text: String { get set }
}
