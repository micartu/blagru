//
//  PointState.swift
//  blagru
//
//  Created by Michael Artuerhof on 12.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

enum PointState : String {
    case idle
    case hightlighted
    case pt

    func image(type: PointType, themed: Theme) -> UIImage? {
        let t = (type == .A) ? "a" : "b"
        let c = (type == .A) ? themed.pointAColor : themed.pointBColor
        switch self {
        case .idle:
            return tinted(image: "point_" + t, with: c)
        case .hightlighted:
            return tinted(image: "placemark_" + t, with: c)
        case .pt:
            let c = UIColor.lightGray
            return tinted(image: "pselected_" + t,
                          with: c,
                          size: CGSize(width: 120, height: 120))
        }
    }

    private func tinted(image: String, with color: UIColor, size: CGSize? = nil) -> UIImage? {
        if let orig = UIImage(named: image)?.withRenderingMode(.alwaysTemplate) {
            let sz: CGSize
            if let s = size {
                sz = s
            } else {
                sz = orig.size
            }
            let r = UIGraphicsImageRenderer(size: sz)
            let im = r.image { _ in
                color.set()
                orig.draw(
                    in: CGRect(origin: CGPoint.zero,
                               size: sz)
                )
            }
            return im
        }
        return nil
    }
}
