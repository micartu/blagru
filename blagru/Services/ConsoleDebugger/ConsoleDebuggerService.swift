//
//  ConsoleDebuggerService.swift
//  blagru
//
//  Created by Michael Artuerhof on 28.03.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

public protocol ConsoleDebuggerService {
    func set(name: String)
    func pr(_ messages: String...)
}
