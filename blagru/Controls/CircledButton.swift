//
//  CircledButton.swift
//  blagru
//
//  Created by Michael Artuerhof on 04.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

@IBDesignable
class CircledButton: UIControl {
    /// Sets touch handler closure
    ///
    /// - Parameter action: The action handler of the touch up event
    ///
    func set(action: (() -> Void)?) {
        touchHandler = action
    }

    /// The shadow color of the floating action button and its children
    ///
    /// Default is `nil`, means there would be no shadow
    ///
    @IBInspectable var shadowColor: UIColor? {
        didSet {
            setupShadow()
        }
    }

    /// The image displayed on the button by default.
    /// When only one `ActionItem` is added and `handleSingleActionDirectly` is enabled,
    /// the image from the item is shown instead.
    /// When set to `nil` an image of a plus sign is used.
    /// Default is `nil`.
    ///
    /// - SeeAlso: `imageView`
    ///
    @IBInspectable var buttonImage: UIImage? {
        didSet {
            configureButtonImage()
        }
    }

    /// The size of the image view.
    /// Default is `CGSize.zero`.
    /// If set to `.zero` the actual size of the image is used.
    ///
    /// - SeeAlso: `imageView`
    ///
    @IBInspectable var buttonImageSize: CGSize = .zero {
        didSet {
            setNeedsUpdateConstraints()
        }
    }

    /// The tint color of the image view.
    /// Default is `UIColor.white`.
    ///
    /// - Warning: Only template images are colored.
    ///
    /// - SeeAlso: `imageView`
    ///
    @IBInspectable var buttonImageColor: UIColor {
        get {
            return imageView.tintColor
        }
        set {
            imageView.tintColor = newValue
        }
    }

    /// The default diameter of the floating action button.
    /// This is ignored if the size is defined by auto-layout.
    /// Default is `56`.
    ///
    @IBInspectable var buttonDiameter: CGFloat = 56 {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    /// The color of circle around the button
    /// Default is `UIColor.clear`
    ///
    @IBInspectable var circleColor: UIColor = .clear {
        didSet {
            circleView.color = circleColor
        }
    }

    // content properties

    /// The round background view of the floating action button.
    /// Read only.
    /// could be modified directly only for animation purposes!
    ///
    /// - SeeAlso: `buttonColor`
    /// - SeeAlso: `highlightedButtonColor`
    ///
    lazy var circleView: CircleView = {
        let view = CircleView()
        view.isUserInteractionEnabled = false
        view.color = circleColor
        return view
    }()

    /// The image view of the floating action button.
    /// Read only.
    /// could be modified directly only for animation purposes!
    ///
    /// - Warning: Setting the image of the `imageView` directly will not work.
    ///            Use `buttonImage` instead.
    ///
    /// - SeeAlso: `buttonImage`
    /// - SeeAlso: `buttonImageColor`
    ///
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = false
        imageView.backgroundColor = .clear
        imageView.tintColor = .blue
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    // MARK: init stuff
    /// Initializes and returns a newly allocated floating action button object with the specified frame rectangle.
    ///
    /// - Parameter frame: The frame rectangle for the floating action button, measured in points.
    ///                    The origin of the frame is relative to the superview in which you plan to add it.
    ///                    This method uses the frame rectangle to set the center and bounds properties accordingly.
    ///
    /// - Returns: An initialized floating action button object.
    ///
    /// - SeeAlso: init?(coder: NSCoder)
    ///
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    /// Returns an object initialized from data in a given unarchiver.
    ///
    /// - Parameter aDecoder: An unarchiver object.
    ///
    /// - Returns: `self`, initialized using the data in decoder.
    ///
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    /// Initializes and returns a newly allocated floating action button object with the specified image and action.
    ///
    /// - Parameter image: The image of the action item. Default is `nil`.
    /// - Parameter action: The action handler of the action item. Default is `nil`.
    ///
    /// - Returns: An initialized floating action button object.
    ///
    /// - SeeAlso: init(frame: CGRect)
    ///
    convenience init(image: UIImage, action: (() -> Void)? = nil) {
        self.init()
        set(action: action)
    }

    fileprivate var touchHandler: (() -> Void)?
    fileprivate var dynamicConstraints: [NSLayoutConstraint] = []
}

// MARK: - Actions
extension CircledButton {
    @objc func buttonWasTapped() {
        touchHandler?()
    }
}

// MARK: - UIView

extension CircledButton {
    /// The natural size for the floating action button.
    ///
    open override var intrinsicContentSize: CGSize {
        return CGSize(width: buttonDiameter, height: buttonDiameter)
    }

    /// Updates constraints for the view.
    ///
    open override func updateConstraints() {
        updateDynamicConstraints()
        super.updateConstraints()
    }
}

// MARK: - Setup
extension CircledButton {
    func setup() {
        backgroundColor = .clear
        clipsToBounds = false
        isUserInteractionEnabled = true
        addTarget(self, action: #selector(buttonWasTapped), for: .touchUpInside)

        setupShadow()

        circleView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false

        addSubview(circleView)
        circleView.addSubview(imageView)

        createStaticConstraints()

        configureButtonImage()
    }

    func setupShadow() {
        if let color = shadowColor {
            layer.shadowColor = color.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 1)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = 2
        } else {
            layer.shadowOffset = CGSize.zero
            layer.shadowOpacity = 0
            layer.shadowRadius = 0
            layer.shadowColor = UIColor.clear.cgColor
        }
    }

    func createStaticConstraints() {
        circleView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        circleView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        circleView.widthAnchor.constraint(equalTo: circleView.heightAnchor).isActive = true
        circleView.widthAnchor.constraint(lessThanOrEqualTo: widthAnchor).isActive = true
        circleView.heightAnchor.constraint(lessThanOrEqualTo: heightAnchor).isActive = true
        let widthConstraint = circleView.widthAnchor.constraint(equalTo: widthAnchor)
        widthConstraint.priority = .defaultHigh
        widthConstraint.isActive = true
        let heightConstraint = circleView.heightAnchor.constraint(equalTo: heightAnchor)
        heightConstraint.priority = .defaultHigh
        heightConstraint.isActive = true

        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: circleView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: circleView.centerYAnchor).isActive = true

        imageView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        imageView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .vertical)
        circleView.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        circleView.setContentHuggingPriority(.fittingSizeLevel, for: .vertical)
    }

    func configureButtonImage() {
        imageView.image = buttonImage
    }

    func updateDynamicConstraints() {
        NSLayoutConstraint.deactivate(dynamicConstraints)
        dynamicConstraints.removeAll()
        createDynamicConstraints()
        NSLayoutConstraint.activate(dynamicConstraints)
        setNeedsLayout()
    }

    func createDynamicConstraints() {
        dynamicConstraints.append(contentsOf: imageSizeConstraints)
    }

    var imageSizeConstraints: [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []
        if buttonImageSize == .zero {
            let multiplier = CGFloat(1 / sqrt(2))
            constraints.append(imageView.widthAnchor.constraint(lessThanOrEqualTo: circleView.widthAnchor, multiplier: multiplier))
            constraints.append(imageView.heightAnchor.constraint(lessThanOrEqualTo: circleView.heightAnchor, multiplier: multiplier))
        } else {
            constraints.append(imageView.widthAnchor.constraint(equalToConstant: buttonImageSize.width))
            constraints.append(imageView.heightAnchor.constraint(equalToConstant: buttonImageSize.height))
        }
        return constraints
    }
}
