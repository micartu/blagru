//
//  NetFun+Queue.swift
//  blagru
//
//  Created by Michael Artuerhof on 01/05/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

extension NetFun: NetQueueService {
    func removeAllOperations() {
        netmanager.used(by: named)
        netmanager.removeAllOperations()
    }
}
