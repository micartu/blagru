//
//  NetFun+Profile.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.07.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

extension NetFun: NetProfileService {
    private func scaleDown(image: UIImage, kMax: CGFloat = 1000) -> UIImage {
        // scale image down if it's too big:
        let scaled: UIImage
        let kw = image.scale * image.size.width
        let kh = image.scale * image.size.height
        if kw > kMax || kh > kMax {
            if kw > kh {
                let h = heightFrom(width: kMax, proportional: image.size)
                scaled = imageWith(image: image,
                                   scaledToSize: CGSize(width: kMax, height: h),
                                   scale: 1)
            } else {
                let w = widthFrom(height: kMax, proportional: image.size)
                scaled = imageWith(image: image,
                                   scaledToSize: CGSize(width: w, height: kMax),
                                   scale: 1)
            }
        } else {
            scaled = image
        }
        return scaled
    }

    func uploadProfile(avatar: UIImage,
                       success: (() -> Void)?,
                       failure: ((NSError) -> Void)?) {
        let s = ["data": scaleDown(image: avatar), "type": "image/jpeg"] as NSDictionary
        let params = [
            "img": s,
        ]
        xpost(url: "/user/upload_avatar/", parameters: params, success: { (ans: NSDictionary) in
            success?()
        }, failure: { e in
            failure?(e)
        })
    }

    func save(user: NSDictionary,
              completion: @escaping (() -> Void),
              failure: @escaping ((NSError) -> Void)) {
        let d = user.toJSONString()?.data(using: .utf8) ?? Data()
        post(url: "/user/edit_info/", data: d, success: { (ans: NSDictionary) in
            completion()
        }, failure: { e in
            failure(e)
        })
    }
}
