//
//  CoreDataInit.swift
//  blagru
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//


import UIKit
import MagicalRecord

final class CoreDataInit {
    private var dbStore : String {
        get {
            return "blagru" // TODO: replace me
        }
    }

    func setupCoreDataStack() {
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: dbStore)
    }

    func cleanUp() {
        MagicalRecord.cleanUp()

        var removeError: NSError?
        let deleteSuccess: Bool
        do {
            guard let url = NSPersistentStore.mr_url(forStoreName: dbStore) else {
                return
            }
            let p = url.path
            let walUrl = URL(string: p + "-wal") ?? url
            let shmUrl = URL(string: p + "-shm") ?? url

            try FileManager.default.removeItem(at: url)
            try? FileManager.default.removeItem(at: walUrl)
            try? FileManager.default.removeItem(at: shmUrl)

            deleteSuccess = true
        } catch let error as NSError {
            removeError = error
            deleteSuccess = false
        }

        if deleteSuccess {
            print(">>> db: '\(dbStore)' successfully deleted, create and initialize a new one...")
        } else {
            print(">>> An error has occured while deleting '\(dbStore)'")
            print(">>> Error description: \(removeError.debugDescription)")
        }
        setupCoreDataStack()
    }
}

extension CoreDataInit: AppDelegateProtocol {
    func description() -> String {
        return "CoreDataInit"
    }

    func initModule(application: UIApplication,
                    options: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupCoreDataStack()
        return true
    }

    var deinitModule: ((UIApplication) -> Void)? {
        return { [weak self] app in
            self?.saveContext()
        }
    }

    var backgroundMethod: ((UIApplication) -> Void)? {
        return { [weak self] a in
            self?.saveContext()
        }
    }

    private func saveContext() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (success, err) in
            if let e = err {
                print("error while saving core data: \(e.localizedDescription)")
            }
        })
    }
}
