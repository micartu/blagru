//
//  InfoViewModelProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 13.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol InfoViewModelProtocol {
    func actionJoin()
}
