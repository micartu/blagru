//
//  SplashTransitionCoordinator.swift
//  blagru
//
//  Created by Michael Artuerhof on 26/08/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

final class SplashTransitionCoordinator: NSObject, UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationController.Operation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DissolveTransition()
    }
}
