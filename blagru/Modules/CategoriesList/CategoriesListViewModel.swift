//
//  CategoriesListViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol CategoriesListDispatcherProtocol: class {
    func chosen(category: EvCategory)
}

protocol CategoriesListViewModelProtocol {
}

final class CategoriesListViewModel: FetchedDataPresenter {
	weak var dispatcher: (AlertProtocol & CategoriesListDispatcherProtocol)?
    fileprivate let cache: CacheCategoryService
    fileprivate var cellIDs = [Int:String]() // cell-row <-> ID dict

    init(fetchCache: CacheFetchRequestsService,
         cache: CacheCategoryService) {
        self.cache = cache
        super.init(manager: fetchCache.fetchDataControllerForCategories())
	}
}

// MARK: - FetchedDataInteractorInput
extension CategoriesListViewModel: FetchedDataInteractorInput {
    func wrap(entity: Any,
              indexPath: IndexPath,
              with theme: Theme) -> CellAnyModel {
        if let e = entity as? EvCategory {
            cellIDs[indexPath.row] = e.id
            return CategoryModel(
                theme: theme,
                id: e.id,
                title: e.name,
                descr: "",
                delegate: self)
        }
        fatalError("CategoriesListViewModel: Asked about an unknown entity: \(entity)")
    }
}

// MARK: - CategoriesListViewModelProtocol
extension CategoriesListViewModel: CategoryCellDelegate {
    func cellTouched(id: String) {
        if let c = cache.categoryWith(id: id) {
            dispatcher?.chosen(category: c)
        }
    }
}

// MARK: - CategoriesListViewModelProtocol
extension CategoriesListViewModel: CategoriesListViewModelProtocol {
}
