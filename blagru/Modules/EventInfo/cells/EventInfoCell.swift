//
//  EventInfoCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 13/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol EventInfoCellDelegate: TouchableCell {
}

final class EventInfoCell: UITableViewCell {
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(cellTapped))
        addGestureRecognizer(tap)
    }

    @objc private func cellTapped () {
        delegate?.cellTouched(id: id)
    }

    var id = ""
    weak var delegate: EventInfoCellDelegate? = nil
}
