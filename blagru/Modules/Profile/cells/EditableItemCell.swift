//
//  EditableItemCell.swift
//  blagru
//
//  Created by Michael Artuerhof on 08.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol EditableItemCellDelegate: class {
    func editReturned(id: String, text: String)
    func editEnded(id: String, text: String)
}

final class EditableItemCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var editItem: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        editItem.delegate = self
    }

    var id = ""
    weak var delegate: EditableItemCellDelegate? = nil
}

// MARK: - UITextFieldDelegate
extension EditableItemCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate?.editReturned(id: id, text: editItem.text ?? "")
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.editEnded(id: id, text: editItem.text ?? "")
    }
}
