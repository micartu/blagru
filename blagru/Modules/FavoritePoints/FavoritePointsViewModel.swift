//
//  FavoritePointsViewModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 17/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

protocol FavoritePointsDispatcherProtocol: class {
    func chosen(ev: Ev)
}

protocol FavoritePointsViewModelProtocol {
}

final class FavoritePointsViewModel: FetchedDataPresenter {
	weak var dispatcher: (AlertProtocol & FavoritePointsDispatcherProtocol)?
    fileprivate let cache: CacheEvService
    fileprivate var favIDs = [Int:String]() // cell-row <-> favoriteID dict

    init(fetchCache: CacheFetchRequestsService,
         cache: CacheEvService) {
        self.cache = cache
        super.init(manager: fetchCache.fetchDataControllerForUserFavorites())
	}
}

// MARK: - FetchedDataInteractorInput
extension FavoritePointsViewModel: FetchedDataInteractorInput {
    func wrap(entity: Any,
              indexPath: IndexPath,
              with theme: Theme) -> CellAnyModel {
        if let e = entity as? Ev {
            favIDs[indexPath.row] = e.id
            return FavoriteModel(
                theme: theme,
                id: e.id,
                title: e.comment,
                descr: "",
                delegate: self)
        }
        fatalError("FavoritePointsViewModel: Asked about an unknown entity: \(entity)")
    }
}

// MARK: - CellOperationProtocol
extension FavoritePointsViewModel: CellOperationProtocol {
    func cellOperation(for indexPath: IndexPath) -> [CellOperation] {
        return [.delete]
    }

    func deleteCell(at indexPath: IndexPath) {
        if let id = favIDs[indexPath.row] {
            delete(id: id)
        }
    }

    fileprivate func delete(id: String) {
        cache.delete(events: [EvDesc(evId: id)]) {
        }
    }
}

// MARK: - FavoritePointsViewModelProtocol
extension FavoritePointsViewModel: FavoriteCellDelegate {
    func cellTouched(id: String) {
        if let ev = cache.EvWith(id: id) {
            dispatcher?.chosen(ev: ev)
        }
    }

    func starTouchedOn(id: String) {
        dispatcher?.showYesNO(
            title: "Are you sure?".localized,
            message: "Do you want to delete point from favorites?".localized,
            actionYes: { [weak self] _ in
                self?.delete(id: id)
            },
            actionNo: nil)
    }
}

// MARK: - FavoritePointsViewModelProtocol
extension FavoritePointsViewModel: FavoritePointsViewModelProtocol {
}
