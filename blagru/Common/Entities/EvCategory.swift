//
//  EvCategory.swift
//  blagru
//
//  Created by Michael Artuerhof on 04.06.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct EvCategory: Codable {
    let id: String
    let name: String
    let icon: String
}

struct EvCategoryDesc: Codable {
    let id: String
}

extension EvCategoryDesc: CategoryDescProtocol {
    var cat_id: String {
        return id
    }
}

extension EvCategory: CategoryDescProtocol {
    var cat_id: String {
        return id
    }
}
