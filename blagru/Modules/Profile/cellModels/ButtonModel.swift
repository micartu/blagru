//
//  ButtonModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 08.10.19.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import UIKit

protocol ButtonCellModel: CommonCell { }

struct ButtonModel: ButtonCellModel {
    let theme: Theme
    let id: String
    let title: String
    let active: Bool
    let delegate: TouchableCell?

    func setup(cell: ButtonCell) {
        cell.id = id
        cell.delegate = delegate

        cell.button.titleLabel?.font = theme.semiboldFont.withSize(theme.sz.middle4)
        cell.button.setTitleColor(theme.emptyBackColor, for: .normal)
        cell.button.setTitle(title, for: .normal)
        cell.button.layer.cornerRadius = 5
        if active {
            cell.button.backgroundColor = theme.tintColor
        } else {
            cell.button.backgroundColor = theme.inactiveColor
        }
        cell.button.isEnabled = active
    }
}
