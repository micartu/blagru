//
//  EventInfoModel.swift
//  blagru
//
//  Created by Michael Artuerhof on 13/06/2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import UIKit

protocol EventInfoCellModel: CommonCell { }

struct EventInfoModel: EventInfoCellModel {
    let theme: Theme
    let id: String
    let name: String
    let image: String
    weak var imgLoader: ImageLoaderService?
	weak var delegate: EventInfoCellDelegate?

	func setup(cell: EventInfoCell) {
        cell.id = id
        cell.delegate = delegate

        cell.lblTitle.text = name
        cell.lblTitle.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblTitle.textColor = theme.mainTextColor

        cell.lblMessage.text = ""
        cell.lblMessage.font = theme.boldFont.withSize(theme.sz.middle1)
        cell.lblMessage.textColor = theme.mainTextColor

        if image.count > 0 {
            if let av = imgLoader?.loadOffline(image: image) {
                cell.avatarView.image = av
            } else {
                imgLoader?.load(image: image) { [weak cell] im in
                    if cell?.id == self.id {
                        cell?.avatarView.image = im
                    }
                }
            }
        } else {
            cell.avatarView.image = UIImage(named: "user")
        }
    }
}
