//
//  IIndexPathCount.swift
//  blagru
//
//  Created by Michael Artuerhof on 06/08/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

/// represents group of elements in table or collection views
/// starting from path and ending path.row + count elements
struct IIndexPathCount {
    let path: IndexPath
    let count: Int
    let animated: Bool
}
