//
//  User.swift
//  blagru
//
//  Created by Michael Artuerhof on 29.01.2020.
//  Copyright © 2020 BearMonti. All rights reserved.
//

import Foundation

struct User: Codable {
    let id: String
    let login: String
    let name: String
    var email: String
    let avatar: String
    var password: String // for auth funcs, should be empty otherwise
}

struct UserDesc: Codable {
    let id: String
}

extension UserDesc: UserDescProtocol { }

extension User: UserDescProtocol { }
