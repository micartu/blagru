//
//  CacheFetchRequestsService.swift
//  blagru
//
//  Created by Michael Artuerhof on 22/10/2019.
//  Copyright © 2019 BearMonti. All rights reserved.
//

import Foundation

protocol CacheFetchRequestsService {
    func fetchDataControllerForParticipantsOfEvent(evId: String) -> FetchManagerProtocol
    func fetchDataControllerForUserFavorites() -> FetchManagerProtocol
    func fetchDataControllerForCategories() -> FetchManagerProtocol
}
