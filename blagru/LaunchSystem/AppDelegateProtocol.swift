//
//  AppDelegateProtocol.swift
//  blagru
//
//  Created by Michael Artuerhof on 24.11.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

import UIKit

protocol AppDelegateProtocol {
    func initModule(application: UIApplication, options: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    var deinitModule: ((UIApplication) -> Void)? { get }
    var backgroundMethod: ((UIApplication) -> Void)? { get }
    var foregroundMethod: ((UIApplication) -> Void)? { get }
    var remoteNotification: (([AnyHashable : Any]) -> Void)? { get }
    var canOpenURL: ((UIApplication, URL, [UIApplication.OpenURLOptionsKey : Any]) -> Bool)? { get }
    func description() -> String
}

// do not return methods by default
extension AppDelegateProtocol {
    var backgroundMethod: ((UIApplication) -> Void)? {
        return nil
    }

    var foregroundMethod: ((UIApplication) -> Void)? {
        return nil
    }

    var remoteNotification: (([AnyHashable : Any]) -> Void)? {
        return nil
    }

    var canOpenURL: ((UIApplication, URL, [UIApplication.OpenURLOptionsKey : Any]) -> Bool)? {
        return nil
    }

    var deinitModule: ((UIApplication) -> Void)? {
        return nil
    }
}
